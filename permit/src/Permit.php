<?php

namespace PackageHalcyon\Permit;

use App\Models\Permit as Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Permit.
 */
class Permit
{
    public function toPermit()
    {
        $list = DB::table(config('package.permit.auth.role.table'))->select(['roles.id', 'roles.name', DB::raw('("role") as type')]);
        $list = DB::table(config('package.permit.auth.user.table'))->select(['users.id', DB::raw('CONCAT(users.first_name, " " , users.last_name) as name'), DB::raw('("user") as type')])->union($list);
        return $list;
    }

    public function permitted($model, $user = null)
    {
        if (empty($user)) {
            $user = auth()->user();
        }
        if (empty($user) && auth()->guest()) {
            return false;
        }
        if ($user->roles()->where('id', 1)->count()) {
            return true;
        }
        $roles = $user->roles()->pluck('id');
        return $model->permitable()->whereIn('role_id', $roles)->count();
    }


    /**
     * @param Request $request
     * @param Model  $model
     *
     * @return static
     */
    public function store($request, $model)
    {
        if (! isset($request->permits)) {
            return ;
        }
        // dd($request->all());
        DB::beginTransaction();
        try {
            if (array_has($request->permits, 'all') && $request->permits['all'] == 1) {
                $model->permitable()->delete();
                DB::commit();
                return $model;
            }
            $role = app(config('package.permit.auth.role.model'));
            $user = app(config('package.permit.auth.user.model'));
            // $existingIDs = Model::where(function ($q) use($request) {
            //     $q->orWhere(function ($q) use ($request) {
            //         $q->where('accessable_type', )
            //     })
            // })->get();



            dd($request->permits['allowed']);


            $data = $this->generateStub($request, $model, $existingIDs);
            $model->permitable()->whereNotIn('role_id', $existingIDs)->delete();
            $model->permitable()->insert($data);
            DB::commit();
            return $model;
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function generateStub($request, $model, $existingIDs)
    {
        $newIDs = $request->roles['id'];
        if (count($newIDs)) {
            foreach ($newIDs as $index => $nID) {
                foreach ($existingIDs as $key => $eID) {
                    if ($nID == $eID) {
                        array_forget($newIDs, $index);
                    }
                }
            }
        }
        $ids = [];
        foreach ($newIDs as $key => $id) {
            $ids[$key]['role_id'] = $id;
            $ids[$key]['permitable_type'] = get_class($model);
            $ids[$key]['permitable_id'  ] = $model->id;
        }
        return $ids;
    }
}
