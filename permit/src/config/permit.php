<?php

return [
    'auth' => [
        'role' => [
            'model' => \App\Models\Auth\Role::class,
            'table' => 'roles'
        ],
        'user' => [
            'model' => \App\Models\Auth\Role::class,
            'table' => 'users'
        ]
    ]
];
