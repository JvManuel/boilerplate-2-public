@php
    $permits = Permit::toPermit();
@endphp
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header" role="tab" id="permit-header">
                <h5 class="mb-0 clearfix">
                    Permit View
                    <div class="pull-right">
                        Allow All
                        <label class="switch switch-3d switch-primary">
                            {!! Form::checkbox('permits[all]', true, 
                                true, 
                                ['class' => 'switch-input permit_all' 
                            ]) !!}
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                </h5>
            </div>
            <div id="permit-container" class="collapse show relative" role="tabpanel" aria-labelledby="permit-header">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="card">
                                <div class="card-header clearfix">Not Permitted Users/Roles <a class="btn btn-info pull-right" name="btn_disallow_clear_selected"><i class="fa fa-refresh"></i></a></div>
                                <ul id="disallowed" class="card-permit-list media-list">
                                    @foreach($permits->take(5)->get() as $p => $permit)
                                        <li class="media card"><a class="btn btn-link">
                                            {!! Form::hidden('permits[disallowed]['.$permit->type.']['.$permit->id.']', true) !!} {{ ucfirst($permit->name) }} 
                                            <span class="badge badge-{{ $permit->type == 'role' ? 'info' : 'primary' }}">{{ ucfirst($permit->type) }}</span>
                                        </a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="btn-group-justified col">
                                <a class="btn btn-success btn-block" name="btn_allow_all_list">>>> All</a>
                                <a class="btn btn-success btn-block" name="btn_allow_list">>>></a>
                                <a class="btn btn-danger btn-block" name="btn_disallow_list"><<<</a>
                                <a class="btn btn-danger btn-block" name="btn_disallow_all_list"><<< All</a>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="card">
                                <div class="card-header clearfix">Not Permitted Users/Roles <a class="btn btn-info pull-right" name="btn_allow_clear_selected"><i class="fa fa-refresh"></i></a></div>
                                <ul id="allowed" class="card-permit-list media-list">
                                    @foreach($permits->skip(5)->take(5)->get() as $p => $permit)
                                        <li class="media card"><a class="btn btn-link">
                                            {!! Form::hidden('permits[allowed]['.$permit->type.']['.$permit->id.']', true) !!} {{ ucfirst($permit->name) }} 
                                            <span class="badge badge-{{ $permit->type == 'role' ? 'info' : 'primary' }}">{{ ucfirst($permit->type) }}</span>
                                        </a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@push('after-styles')
    <style type="text/css">
        .card-permit-list {
            max-height: 250px;
            min-height: 100px;
            height: auto;
            overflow-y: auto;
            overflow-x: hidden;
            width: 100%; 
            padding: 0;
            margin: 0;
        }
        .card-permit-list li.media.card {
            margin: 0;
        }
    </style>
@endpush
@push('after-scripts')
    <script type="text/javascript">
        let permitOverlay = `<div class="overlay">
                <div class="loader-container text-center">
                    <div class="text-center"><i class="fa fa-eye fa-3x"></i></div>
                    <div class="text-center">This will allow guest to view this content.</div>
                </div>
            </div>
        `
        $(document).ready(function () {
            // $('#permit-container').prepend(permitOverlay)
            $('#permit-header .permit_all').on('change', function () {
                $('#permit-container .role').attr('disabled', $(this).is(':checked'))
                if ($(this).is(':checked')) {
                    $('#permit-container').prepend(permitOverlay)
                } else {
                    $('#permit-container').find('.overlay').remove()
                }
            })

            $('#disallowed').on('click', 'li.media.card', function () {
                if ($(this).hasClass('bg-primary')) {
                    $(this).removeClass('bg-primary')
                } else {
                    $(this).addClass('bg-primary')
                }
            })
            
            $('#allowed').on('click', 'li.media.card', function () {
                if ($(this).hasClass('bg-primary')) {
                    $(this).removeClass('bg-primary')
                } else {
                    $(this).addClass('bg-primary')
                }
            })

            $('[name=btn_disallow_clear_selected]').click(function () {
                $('#disallowed').find('li.media.card.bg-primary').each(function() {
                    $(this).removeClass('bg-primary')
                })
            })

            $('[name=btn_allow_clear_selected]').click(function () {
                $('#allowed').find('li.media.card.bg-primary').each(function() {
                    $(this).removeClass('bg-primary')
                })
            })

            // BTN ACTION MOVING

            $('[name=btn_allow_list]').click(function () {
                $('#disallowed').find('li.media.card.bg-primary').each(function() {
                    $('#allowed').prepend($(this).removeClass('bg-primary'))
                })
            })
            $('[name=btn_disallow_list]').click(function () {
                $('#allowed').find('li.media.card.bg-primary').each(function() {
                    console.log($(this).attr('name'))
                    $('#disallowed').prepend($(this).removeClass('bg-primary'))
                })
            })


            $('[name=btn_disallow_all_list]').click(function () {
                $('#allowed').find('li.media.card').each(function() {
                    $('#disallowed').prepend($(this).removeClass('bg-primary'))
                })
            })


            $('[name=btn_allow_all_list]').click(function () {
                $('#disallowed').find('li.media.card').each(function() {
                    $('#allowed').prepend($(this).removeClass('bg-primary'))
                })
            })
        })
    </script>
@endpush