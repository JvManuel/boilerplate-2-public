<?php
namespace PackageHalcyon\Permit\Traits;

use PackageHalcyon\Permit\Models\Permit;

trait Permitable
{
    public function permitable()
    {
        return $this->morphMany(Permit::class, 'permitable');
    }

    public function permitAll()
    {
        return $this->permitable()->where('all', 1);
    }
}
