<?php
namespace PackageHalcyon\Permit\Traits;

use PackageHalcyon\Permit\Models\Permit;

trait AccessableScopes
{
    public function permit()
    {
        return $this->morphMany(Permit::class, 'accessable');
    }
}
