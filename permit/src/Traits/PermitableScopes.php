<?php
namespace PackageHalcyon\Permit\Traits;

use PackageHalcyon\Permit\Models\Permit;

trait PermitableScopes
{
    public function hasPermitted()
    {
        return $this->morphMany(Permit::class, 'permitable');
    }
}
