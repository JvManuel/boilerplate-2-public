<?php
namespace PackageHalcyon\Permit;

use Illuminate\Support\ServiceProvider;
use PackageHalcyon\Permit\Permit;
use PackageHalcyon\Permit\Middleware\PermitMiddleware;

class PermitServiceProvider extends ServiceProvider
{
    public function boot(\Illuminate\Routing\Router $router)
    {
        $router->aliasMiddleware('permit-middleware', PermitMiddleware::class);
        $this->publishes([__DIR__ . '/config/permit.php' => config_path('package/permit.php')]);
    }

    public function register()
    {
        $this->app->bind('permit', function ($app) {
            return new Permit($app);
        });
        $this->app->alias('permit', \PackageHalcyon\Permit\Facades\Permit::class);
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        
        $this->loadViewsFrom(__DIR__.'/resources/views', 'permit');

        $this->mergeConfigFrom(__DIR__ . '/config/permit.php', 'package.permit');
    }
}
