<?php

namespace PackageHalcyon\Permit\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Permit.
 */
class Permit extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'permit';
    }
}
