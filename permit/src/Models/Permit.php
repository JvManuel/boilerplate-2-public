<?php 

namespace PackageHalcyon\Permit\Models;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    protected $fillable = [
        'permitable_id', 'permitable_type', 'accessable_id', 'acccessable_type'
    ];

    public function permitable()
    {
        return $this->morphTo();
    }

    public function accessable()
    {
        return $this->morphTo();
    }
}
