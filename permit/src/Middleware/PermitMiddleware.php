<?php

namespace PackageHalcyon\Permit\Middleware;

use Closure;
use PackageHalcyon\Permit\Facades\Permit;
use Spatie\Permission\Exceptions\UnauthorizedException;

/**
 * Class PermitMiddleware.
 */
class PermitMiddleware
{
    /**
     * @param         $request
     * @param Closure $next
     *
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle($request, Closure $next, $permitable_type)
    {
        $route = $request->route();
        $key = $route->parameterNames()[count($request->route()->parameterNames()) - 1];
        $slug = $route->parameters($key);
        $model = app($permitable_type);
        $model = $model->with(['permitable'])->where($model->getRouteKeyName(), $slug)->firstOrFail();
        if (! Permit::permitted($model)) {
            throw UnauthorizedException::forRoles([]);
        }
        return $next($request);
    }
}
