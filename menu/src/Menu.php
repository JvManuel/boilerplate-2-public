<?php

namespace PackageHalcyon\Menu;

use PackageHalcyon\Menu\Models\Menu as Model;
use PackageHalcyon\Menu\Models\Node;
use Illuminate\Support\Facades\DB;

/**
 * Class Menu.
 */
class Menu
{
    protected $model;
    protected $nodes;

    protected $modelView;
    protected $nodesView;

    public function menus()
    {
        // if (! is_array($fields)) { $fields = [Model::getRouteKeyName() => $fields]; }
        // $model = Model::where($fields)->firstOrFail();
        $menus = Model::where('status', 'active')->get();

        if (count($menus)) {
            foreach ($menus as $m => $menu) {
                $menu->hierarchy = $this->hierarchyViews($menu);
            }
        }

        return $menus;
    }

    /**
     * Returns the hierarchy structure of a menu
     * @return array
     */
    public function hierarchyViews($menu, $parentID = null) : array
    {
        $this->modelView = $menu->nodes()->with(['menuable']);
        if (! $this->nodesView) {
            $this->nodesView = $this->modelView->orderBy('order', 'ASC')->get();
        }

        $nodes = $this->nodesView->where('parent_id', $parentID);
        $result = [];

        foreach ($nodes as $n => $node) {
            if ($node->menuable) {
                $node->type = $node->menuable->base('menu_type');
            }
            $node->children = $this->hierarchyViews($menu, $node->id);
            $result[] = $node;

            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                // dd();
                $this->nodesView = null;
                $this->modelView = null;
            }
        }
        return $result;
    }


    /**
     * Returns the hierarchy structure of a menu
     * @return array
     */
    public function hierarchy($menu, $parentID = null) : array
    {
        $this->model = $menu->nodes()->with(['menuable']);
        if (! $this->nodes) {
            $this->nodes = $this->model->orderBy('order', 'ASC')->get();
        }

        $nodes = $this->nodes->where('parent_id', $parentID);
        $result = [];

        foreach ($nodes as $n => $node) {
            if ($node->menuable) {
                $node->type = $node->menuable->base('menu_type');
            }
            $node->children = $this->hierarchy($menu, $node->id);
            $result[] = $node;

            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                // dd();
                $this->nodes = null;
                $this->model = null;
            }
        }
        return $result;
    }


    /**
     * Returns the hierarchy structure of a taxonomy in an array
     * @return array
     */
    public function hierarchiesInForm($model = null)
    {
        $results = [];
        $menus = Model::where('status', 'active')->get();
        if (count($menus)) {
            foreach ($menus as $m => $menu) {
                $results[$menu->id] = $menu->name;
                $this->hierarchyInForm($menu, $results, $label = "-", $parentID = null, $model);
            }
        }
        return $results;
    }

    /**
     * Returns the hierarchy structure of a taxonomy in an array
     * @return array
     */
    public function hierarchyInForm($menu, &$result, $label = "", $parentID = null, $model = null)
    {
        $this->model = $menu->nodes();
        if (! $this->nodes || ! count($this->nodes)) {
            $this->nodes = $menu->nodes()->orderBy('order', 'ASC')->get();
        }
        $nodes = $this->nodes->where('parent_id', $parentID);
        foreach ($nodes as $n => $node) {
            if (($model && $model->id != $node->id) || ! $model) {
                $result[$menu->id .'_'. $node->id] = $label . $node->name;
            }
            $this->hierarchyInForm($menu, $result, $label . '-', $node->id, $model);
            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                $this->nodes = null;
                $this->model = null;
            }
        }
        return $result;
    }


    /**
     * Returns the hierarchy structure of a taxonomy in an array
     * @param Model $menu
     * @param array appended $result
     * @param int $parentID
     * @return array $result
     */
    public function hierarchyToArray($menu, &$result, $parentID = null)
    {
        $this->model = $menu->nodes();
        if (! $this->nodes) {
            $this->nodes = $this->model->orderBy('order', 'ASC')->get();
        }
        $nodes = $this->nodes->where('parent_id', $parentID);
        foreach ($nodes as $n => $node) {
            $result[] = $node;
            $this->hierarchyToArray($class, $result, $node->id);
            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                $this->nodes = null;
                $this->model = null;
            }
        }
        return $result;
    }


    /**
     * Returns the hierarchy route
     * @param Menu $model
     * @param Appended Collection &$list
     * @return Collection $list
     */
    public function getRoots($model, &$list, $include_model = true)
    {
        if (! $list instanceof Illuminate\Database\Eloquent\Collection) {
            $list = collect($list);
        }
        if ($include_model) {
            $list->prepend($model);
        }
        if ($model->parent_id != null) {
            $this->getRoots($model->parent, $list);
        }
        return $list;
    }

    /**
     * Returns the hierarchy route
     * @param Menu $model
     * @param Appended String &$route
     * @return String $route
     */
    public function getRouteRoots($model, &$route, $include_model = true) : String
    {
        $slug = $include_model ? $model->{$model->getRouteKeyName()} : "";
        $route = $slug . ($route != "" ? '/' : '') . $route ;
        if ($model->parent_id != null) {
            $this->getRouteRoots($model->parent, $route);
        }
        return $route;
    }

    /**
     * Update
     *
     **/
    /**
     * Update Hierarchy
     * @param Object $request
     * @param Menu $menu
     * @return mixed
     */
    public function updateHierarchy($request, $menu)
    {
        $this->model = $menu->nodes();
        DB::beginTransaction();
        try {
            // Delete
            $nodes = $request->deleted;
            if ($request->deleted && !is_array($nodes)) {
                $nodes = json_decode($nodes ?? []);
            }
            $this->deleteHierarchyNode($nodes);
            // Structure
            $hierarchy = $request->hierarchy;
            if (!is_array($hierarchy)) {
                $hierarchy = json_decode($hierarchy, true);
            }

            $list = $this->model->orderBy('order', 'ASC')->get();
            $nodes = collect();
            foreach ($hierarchy as $order => $node) {
                $nodes = $nodes->push($this->updateHierarchyNode($list, $node, $order));
            }

            DB::commit();
            return $this->hierarchy($class);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Update Node by Hierarchy
     * @param Collection $list
     * @param Model $node
     * @param int $order
     * @param int $parentID
     * @return mixed
     */
    protected function updateHierarchyNode($list, $node, $order, $parentID=null)
    {
        DB::beginTransaction();
        try {
            $data = ['order' => $order, 'parent_id' => $parentID];
            if (array_key_exists('id', $node) && $node['id']) {
                $result = $list->where('id', $node['id'])->first();
                $result->update($data);
            }

            $children = array_get($node, 'children', []);
            if (count($children)) {
                foreach ($children as $key => $child) {
                    $this->updateHierarchyNode($list, $child, $key, $node['id']);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Delete items by id
     * @param EloquentBase|int|array|null $id
     * @return mixed
     */
    protected function deleteHierarchyNode($id = null)
    {
        $nodes = null;
        if (is_array($id)) {
            $nodes = $this->model->whereIn('id', $id);
        } elseif ($id instanceof Model) {
            $nodes = $id;
        } else {
            $nodes = $this->model->where('id', '=', $id);
        }
        try {
            $nodes->delete();
            return true;
        } catch (\Exception $e) {
            dd($e);
        }
    }














    // Widget

    public function store($request, $model)
    {
        if ($request->menu['add']
            && $request->menu['add']['menu_id']
            && $request->menu['add']['menu_id'] != "null"
            && $request->menu['add']['name']
        ) {
            $id = explode('_', $request->menu['add']['menu_id']);
            $data = [];
            $data['name'] = $request->menu['add']['name'] ?? $model->name;
            $data['menu_id'] = $id[0];
            $data['order'] = 0;
            $data['options'] = json_encode([]);
            if (count($id) > 1) {
                $data['parent_id'] = $id[1];
            }
            $model = $model->menuable()->create($data);
        }
    }

    public function update($request, $model)
    {
        if (array_key_exists('nodes', $request->menu) && $request->menu['nodes']) {
            $menuables = $model->menuable()->get();
            foreach ($request->menu['nodes'] as $n => $node) {
                $menuable = $menuables->where('id', $n)->first();
                $id = explode('_', $node['menu_id']);
                $data['name'] = $node['name'] ?? $model->name;
                $data['menu_id'] = $id[0];
                if (count($id) > 1) {
                    $data['parent_id'] = $id[1];
                }
                if ($model->menu_id != $id[0]
                    || (count($id) > 1 && $model->parent_id && $model->parent_id != $id[1])
                ) {
                    $data['order'] = 0;
                }
                $menuable->update($data);
            }
            $model->menuable()->whereNotIn('id', array_keys($request->menu['nodes']))->delete();
        }
    }



    // Render


    public function renderMenu($menu)
    {
        if (! $menu) {
            return "";
        }
        return view('menu::frontend.template.' . $menu->template, compact('menu'))->render();
    }
}
