<?php

use PackageHalcyon\Menu\Models\Menu;
use Illuminate\Database\Seeder;
use App\Models\Auth\User;
use App\Models\Auth\Role;
use App\Models\Auth\Permission;

/**
 * Class MenuPermissionTableSeeder.
 */
class MenuPermissionTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $menu = Role::create(['name' => 'menu manager']);
        
        // Menu
        Permission::create(['name' => 'menu list']);
        Permission::create(['name' => 'menu activity']);
        Permission::create(['name' => 'menu inactive']);
        if (config('package.menu.can_create')) {
            Permission::create(['name' => 'menu create']);
        }
        Permission::create(['name' => 'menu update']);
        if (config('package.menu.can_delete')) {
            Permission::create(['name' => 'menu delete']);
        }
        Permission::create(['name' => 'menu show']);
        
        Permission::create(['name' => 'menu change status']);

        $permissions = [
            'menu list', 'menu inactive', 'menu activity',
            'menu create', 'menu update', 'menu delete', 'menu show',
            'menu change status',
        ];
        if (! config('package.menu.can_create')) {
            array_forget($permissions, 3);
        }
        if (! config('package.menu.can_delete')) {
            array_forget($permissions, 5);
        }
        // dd($permissions);

        $menu->givePermissionTo($permissions);

        User::find(1)->assignRole('menu manager');
        User::find(2)->assignRole('menu manager');
        
        $this->enableForeignKeys();
    }
}
