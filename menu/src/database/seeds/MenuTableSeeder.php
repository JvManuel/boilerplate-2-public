<?php

use Illuminate\Database\Seeder;
use PackageHalcyon\Menu\Models\Menu;

/**
 * Class MenuTableSeeder.
 */
class MenuTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        Menu::create([
            'name' => 'Main Menu', 'slug' => str_slug('Main Menu'),
            'description' => 'This is the main navigation of the website.',
            'status' => 'active', 'template' => 'navbar', 'depth' => 3
        ]);

        Menu::create([
            'name' => 'Footer Menu', 'slug' => str_slug('Footer Menu'),
            'description' => 'This is the footer navigation of the website.',
            'status' => 'active', 'template' => 'default', 'depth' => 3
        ]);

        $this->enableForeignKeys();
    }
}
