<?php
/**
 * All route names are prefixed with 'admin.menu'.
 */
Route::group([
    'namespace'  => 'PackageHalcyon\Menu\Controllers',
], function () {
    Route::group([
    'namespace'  => 'Backend',
    'middleware' => ['web', 'admin'], 'as' => 'admin.', 'prefix' => 'admin/'
    ], function () {
        Route::post('menu/table', 'MenuTableController@index')->name('menu.table');
        Route::get('menu/activities', 'MenuActivityController')->name('menu.activity');
    
        Route::get('menu/inactive', 'MenuStatusController@inactive')->name('menu.inactive');
        Route::patch('menu/{menu}/mark', 'MenuStatusController@mark')->name('menu.mark');
        
        
        Route::post('menu/{menu}/hierarchy', 'MenuHierarchyController@store')->name('menu.hierarchy.store');
        Route::patch('menu/{menu}/hierarchy', 'MenuHierarchyController@update')->name('menu.hierarchy.update');
        Route::post('menu/{menu}/hierarchy/table', 'MenuHierarchyController@table')->name('menu.hierarchy.table');
        


        Route::resource('menu', 'MenusController', ['except' => [''. (config('package.menu.can_create') ?: 'create') .'', ''. (config('package.menu.can_delete') ?: 'delete') .'']]);
    });
});
