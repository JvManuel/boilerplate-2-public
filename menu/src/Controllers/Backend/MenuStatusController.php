<?php

namespace PackageHalcyon\Menu\Controllers\Backend;

use PackageHalcyon\Base\Controllers\StatusController as Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Menu\Repositories\MenuRepository as Repository;

use PackageHalcyon\Menu\Models\Menu as Model;

/**
 * Class MenuStatusController.
 */
class MenuStatusController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
        $this->route    = 'admin.menu';
        $this->view    	= 'menu::backend';
        $this->middleware('permission:menu inactive', ['only' => ['inactive']]);
        $this->middleware('permission:menu change status', ['only' => ['mark']]);
    }
}
