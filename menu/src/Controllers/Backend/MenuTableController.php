<?php

namespace PackageHalcyon\Menu\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Menu\Repositories\MenuRepository as Repository;

use PackageHalcyon\Menu\Models\Menu as Model;
use DataTables;

/**
 * Class MenuTableController.
 */
class MenuTableController extends Controller
{
    /**
     * @var BlockRepository
     */
    protected $repo;

    /**
     * @param BlockRepository $repo
     */
    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        return DataTables::of($this->repo->table($request))
            ->escapeColumns(['id'])
            ->editColumn('status', function ($model) use ($user) {
                return [
                    'type' => $model->status == "active" ? 'success' : 'danger',
                    'label' => ucfirst($model->status),
                    'value' => $model->status,
                    'link' => route('admin.menu.mark', $model),
                    'can' => $user->can('menu change status')
                ];
            })
            ->editColumn('updated_at', function ($model) {
                return $model->updated_at->format('d M, Y h:m A');
            })
            ->addColumn('actions', function ($model) {
                return $model->actions();
            })
            ->make(true);
    }
}
