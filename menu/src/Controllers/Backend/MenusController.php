<?php

namespace PackageHalcyon\Menu\Controllers\Backend;

use PackageHalcyon\Base\Controllers\CRUDController as Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Menu\Repositories\MenuRepository as Repository;

use PackageHalcyon\Menu\Models\Menu as Model;

/**
 * Class MenusController.
 */
class MenusController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
        $this->config   = (object)[
            'view' => 'menu::backend',
            'route' => 'admin.menu',
            'isSlugged' => true,
            'hasSoftDelete' => false
        ];

        $this->middleware('permission:menu list', ['only' => ['index']]);
        if (config('package.menu.can_create')) {
            $this->middleware('permission:menu create', ['only' => ['create', 'store']]);
        }
        $this->middleware('permission:menu update', ['only' => ['update', 'edit']]);
        if (config('package.menu.can_delete')) {
            $this->middleware('permission:menu delete', ['only' => ['delete']]);
        }
    }

    public function storeRules()
    {
        return [
            'name' => 'required|max:255|unique:menus',
            'description' => 'max:255',
            'depth' => 'required|integer|min:1|max:5',
            'template' => 'required|in:' . implode(',', array_keys(config('package.menu.templates'))),
        ];
    }

    public function updateRules($model)
    {
        return [
            'name' => 'required|max:255|unique:menus,id,'. $model->id,
            'description' => 'max:255',
            'depth' => 'required|integer|min:1|max:5',
            'template' => 'required|in:' . implode(',', array_keys(config('package.menu.templates'))),
        ];
    }
}
