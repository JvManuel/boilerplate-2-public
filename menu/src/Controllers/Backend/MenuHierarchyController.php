<?php

namespace PackageHalcyon\Menu\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Menu\Repositories\HierarchyRepository as Repository;

use PackageHalcyon\Menu\Models\Menu as Model;
use DataTables;
use PackageHalcyon\Menu\Facades\Menu;

/**
 * Class MenuHierarchyController.
 */
class MenuHierarchyController extends Controller
{
    /**
     * @var BlockRepository
     */
    protected $repo;

    /**
     * @param BlockRepository $repo
     */
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo = $repo;
        $this->model = $model;
    }
        
    public function table(Request $request, $slug)
    {
        $model = $this->model->whereSlug($slug)->firstOrFail();
        $hierarchy =  Menu::hierarchy($model);
        return response()->json(['data' => $hierarchy]);
    }

    public function store(Request $request, $slug)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'url' => 'required|url',
            'parent_id' => 'nullable|exists:menu_nodes,id',
            'options.class' => 'nullable|max:100',
            'options.icon' => 'nullable|max:100',
            'options.target' => 'nullable|max:100',
        ]);
        $model = $this->model->whereSlug($slug)->firstOrFail();
        $this->repo->store($request, $model);
        return redirect()->route('admin.menu.show', $model);
    }

    public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'hierarchy.*.name' => 'required|max:100',
            'hierarchy.*.url' => 'sometimes|url',
            'hierarchy.*.parent_id' => 'nullable|exists:menu_nodes,id',
            'hierarchy.*.options.class' => 'nullable|max:100',
            'hierarchy.*.options.icon' => 'nullable|max:100',
            'hierarchy.*.options.target' => 'nullable|max:100',

        ]);
        $model = $this->model->whereSlug($slug)->firstOrFail();
        $hierarchy = $this->repo->update($request, $model);
        return response()->json(['data' => $hierarchy]);
    }
}
