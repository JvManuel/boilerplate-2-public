<?php

namespace PackageHalcyon\Menu\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class MenuActivityController.
 */
class MenuActivityController extends Controller
{

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return view('menu::backend.activity');
    }
}
