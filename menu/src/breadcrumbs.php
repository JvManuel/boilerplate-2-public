<?php

Breadcrumbs::register('admin.menu.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Menu', route('admin.menu.index'));
});

Breadcrumbs::register('admin.menu.activity', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.menu.index');
    $breadcrumbs->push('Activities', route('admin.menu.activity'));
});


Breadcrumbs::register('admin.menu.inactive', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.menu.index');
    $breadcrumbs->push('Inactive List', route('admin.menu.inactive'));
});

if (config('package.menu.can_create')) {
    Breadcrumbs::register('admin.menu.create', function ($breadcrumbs) {
        $breadcrumbs->parent('admin.menu.index');
        $breadcrumbs->push('Create', route('admin.menu.create'));
    });
}

Breadcrumbs::register('admin.menu.show', function ($breadcrumbs, $model) {
    $breadcrumbs->parent('admin.menu.index');
    $breadcrumbs->push('Show', route('admin.menu.show', $model));
});

Breadcrumbs::register('admin.menu.edit', function ($breadcrumbs, $model) {
    $breadcrumbs->parent('admin.menu.show', $model);
    $breadcrumbs->push('Edit', route('admin.menu.edit', $model));
});
