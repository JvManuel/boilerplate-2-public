<?php

namespace PackageHalcyon\Menu\Traits;

use PackageHalcyon\Menu\Models\Node as Model;

trait Menuable
{
    public function menuable()
    {
        return $this->morphMany(Model::class, 'menuable');
    }
}
