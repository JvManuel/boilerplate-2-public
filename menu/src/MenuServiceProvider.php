<?php
namespace PackageHalcyon\Menu;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

class MenuServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([__DIR__ . '/database/seeds/MenuPermissionTableSeeder.php' => database_path('seeds/Auth/ModulePermission/MenuPermissionTableSeeder.php')]);
        $this->publishes([__DIR__ . '/database/seeds/MenuTableSeeder.php' => database_path('seeds/Module/MenuTableSeeder.php')]);
        $this->publishes([__DIR__.'/resources/assets' => public_path('vendor/menu') ], 'public');
        $this->publishes([__DIR__ . '/config/menu.php' => config_path('package/menu.php')]);
        $this->publishes([ __DIR__.'/resources/views/frontend/template' => resource_path('views/vendor/menu/frontend/template')]);

        require 'breadcrumbs.php';
        View::composer(config('package.menu.views'), function ($view) {
            $menus = collect();
            try {
                $menus = \PackageHalcyon\Menu\Facades\Menu::menus();
            } catch (\Exception $e) {
            }
            $view->with(compact('menus'));
        });
    }

    public function register()
    {
        $this->app->bind('menu', function ($app) {
            return new Menu($app);
        });
        $this->app->alias('menu', \PackageHalcyon\Menu\Facades\Menu::class);
        
        // // register our controller
        $this->app->make('PackageHalcyon\Menu\Controllers\Backend\MenusController');
        $this->app->make('PackageHalcyon\Menu\Controllers\Backend\MenuStatusController');
        $this->app->make('PackageHalcyon\Menu\Controllers\Backend\MenuTableController');
        

        // // Migration
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        
        //       // Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'menu');
        $this->mergeConfigFrom(__DIR__.'/config/menu.php', 'package.menu');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }
}
