@if(count($menus))
	@foreach($menus as $m => $menu)
		@include('menu::frontend.template.' . config('package.menu.template')[$menu->template], ['menu' => $menu])
	@endforeach
@endif