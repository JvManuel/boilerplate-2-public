@if(! isset($nodes))
    <ul class="nav navbar-nav navbar-right @can('menu update') qlink @endcan">
        @can('menu update') 
            <a href="{{ $menu->action($frontend = false, 'edit') }}" class="btn btn-sm btn-info qlink-button">
                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit Menu"></i>
            </a>
        @endcan
        @include('menu::frontend.template.navbar', ['nodes' => $menu->hierarchy])
    </ul>
@else
	@if(count($nodes))
		@foreach($nodes as $n => $node)
            <li class="{{ (isset($sub) && $sub) ? 'dropdown-submenu' : '' }}">
                @if(count($node->children))
                    <a href="{{ $node->link }}" class=" dropdown-toggle" aria-haspopup="true" aria-expanded="false"> {{ $node->name }}  <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        @include('menu::frontend.template.navbar', ['nodes' => $node->children, 'sub' => true])
                    </ul>
                @else
                    <a href="{{ $node->link }}" class="" > {{ $node->name }} </a>
                @endif
            </li>
        @endforeach
	@endif
@endif