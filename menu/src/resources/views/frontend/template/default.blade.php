@if(! isset($nodes))
	<div class="menu">
		<ul>
			@include('menu::frontend.template.default', ['nodes' => $menu->hierarchy])
		</ul>
	</div>
@else
	@if(count($nodes))
		@foreach($nodes as $n => $node)
			<li>
				<a href="{{ $node->link }}">{{ $node->name ?? $node->menuable->name }}</a>
				@if(count($node->children))
					<ul>
						@include('menu::frontend.template.default', ['nodes' => $node->children])
					</ul>
				@endif
			</li>
		@endforeach
	@endif
@endif