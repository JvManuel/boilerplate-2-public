@extends('backend.layouts.app')

@section ('title', 'Menu Management | Show Menu')

@section('breadcrumb-links')
    @include('menu::backend.includes.breadcrumb-links')
@endsection
@include('base::partials.fancybox')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Menu Management
                    <small class="text-muted">Show Menu</small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
                <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                    @can('menu hierarchy list')
                        <a href="{{ route('admin.menu.hierarchy.index', $model) }}" class="btn btn-primary ml-1" data-toggle="tooltip" title="View Menu Hierarchy"><i class="fa fa-list"></i></a>
                    @endcan
                    @can('menu update')
                        <a href="{{ route('admin.menu.edit', $model) }}" class="btn btn-info ml-1" data-toggle="tooltip" title="Edit Menu"><i class="fa fa-pencil"></i></a>
                    @endcan
                    @if(config('package.menu.can_delete'))
                        @can('menu delete')
                            <a href="{{ route('admin.menu.destroy', $model) }}" data-redirect="{{ route('admin.menu.index') }}" class="btn btn-danger ml-1" id="btn_delete" data-toggle="tooltip" title="Delete Content"><i class="fa fa-trash"></i></a>
                        @endcan
                    @endif
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ ! request()->query() ? 'active' : '' }}" data-toggle="tab" href="#hierarchy-tab" role="tab" aria-controls="hierarchy-tab" aria-expanded="true"><i class="fa fa-list"></i> Hierarchy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->query('overview') ? 'active' : '' }}" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fa fa-user"></i> Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->query('history') ? 'active' : '' }}" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-expanded="true"><i class="fa fa-list"></i> Activity</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane {{ ! request()->query() ? 'active' : '' }}" id="hierarchy-tab" role="tabpanel" aria-expanded="true">
                       @include('menu::backend.includes.hierarchy.hierarchy')
                    </div>
                    <div class="tab-pane {{ request()->query('overview') ? 'active' : '' }}" id="overview" role="tabpanel" aria-expanded="true">
                       @include('menu::backend.includes.overview')
                    </div>
                    <div class="tab-pane {{ request()->query('history') ? 'active' : '' }}" id="history" role="tabpanel" aria-expanded="true">
                        {!! History::renderEntity($model) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>Created At: </strong> {{ $model->updated_at->timezone(get_user_timezone()) }} ({{ $model->created_at->diffForHumans() }}),
                    <strong>Updated At:</strong> {{ $model->created_at->timezone(get_user_timezone()) }} ({{ $model->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { previewActions() })
    </script>
@endpush
@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/timeline.css') }}">
@endpush