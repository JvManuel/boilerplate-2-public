@extends('backend.layouts.app')

@section ('title', 'Menu Management')

@section('breadcrumb-links')
    @include('menu::backend.includes.breadcrumb-links')
@endsection
@include('base::partials.datatable')
@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-table">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title mb-0">
                                Menu Management <small class="text-muted">Active Menu</small>
                            </h4>
                        </div>
                        <div class="col-sm-7">
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                @if(config('package.menu.can_create'))
                                    @can('menu create')
                                        <a href="{{ route('admin.menu.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="Create Menu">
                                            <i class="fa fa-plus-circle"></i>
                                        </a>
                                    @endcan
                                @endif
                                <a name="btn_refresh" class="btn btn-primary ml-1" data-toggle="tooltip" title="Refresh List"> <i class="fa fa-refresh"></i> </a>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="table-responsive">
                            <table id="content-table" class="table table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Last Modified</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Last Modified</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            table = $('#content-table').DataTable({
                processing: false,
                serverSide: true,
                scrollY: '45vh',
                scrollCollapse: false,
                ajax: {
                    url: '{{ route("admin.menu.table") }}',
                    type: 'post',
                    data: { status: 1, trashed: false },
                    beforeSend: function(){ $('.card.card-table').prepend(overlay); },
                    dataSrc: function(response){
                        setTimeout(function () { $('.card.card-table').find('.overlay').remove(); }, 500);
                        return response.data;
                    }
                },
                columns: [
                    {data: 'name',      },
                    {data: 'slug',      },
                    {
                        data: 'status',  
                        render: function(data){  return data.can == true ? renderChangeStatus(data) : renderLabel(data);  },
                        searchable: false, 
                        sortable: false
                    },
                    {data: 'updated_at',},
                    {
                        data: 'actions', 
                        render: function(data){ return renderActions(data); },
                        searchable: false, 
                        sortable: false
                    }
                ],
                order: [[4, "desc"]],
                searchDelay: 500,
                fnInitComplete:function(){
                    $('.card.card-table').find('.overlay').remove();
                    $('[name=btn_refresh]').click(function(){ table.ajax.reload(); });
                    dataTableActions(table);
                },
            });
        });
    </script>
@endpush
