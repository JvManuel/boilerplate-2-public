@extends('backend.layouts.app')

@section ('title', 'Menu Management | Activities')

@section('breadcrumb-links')
    @include('menu::backend.includes.breadcrumb-links')
@endsection
@section('content')
	<div class="row">
		<div class="col">
			<div class="card">
			    <div class="card-body">
                    {!! history()->renderType(\PackageHalcyon\Menu\Models\Menu::class) !!}
			    </div>
			</div>
		</div>
	</div>
@endsection
@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/timeline.css') }}">
@endpush