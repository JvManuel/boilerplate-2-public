@extends('backend.layouts.app')

@section ('title', 'Menu Management | Create Menu')

@section('breadcrumb-links')
    @include('menu::backend.includes.breadcrumb-links')
@endsection

@section('content')
    {!! Form::open(['url' => route('admin.menu.store'), 'file' => 'mutlipart/enctype', 'files' => true, 'id' => 'content-form', 'class' => 'form-horizontal' ]) !!}
        <div class="card card-form">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Menu Management
                            <small class="text-muted">Create Menu</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                @include('menu::backend.includes.fields')
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <a href="{{ route('admin.menu.index') }}" class="btn btn-danger btn-sm">Cancel</a>
                    </div><!--col-->

                    <div class="col text-right">
                        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-sm btn-submit']) }}
                    </div><!--row-->
                </div><!--row-->
            </div><!--card-footer-->
        </div>
    {!! Form::close() !!}
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { formSubmit($('#content-form')) })
    </script>
@endpush


