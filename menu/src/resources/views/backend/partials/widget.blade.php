@php    
    $menus = Menu::hierarchiesInForm();
@endphp
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header" role="tab" id="menuNode">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#menuNodeBody" aria-expanded="true" aria-controls="menuNodeBody">
                      Menu Information
                    </a>
                </h5>
            </div>
            <div id="menuNodeBody" class="collapse show" role="tabpanel" aria-labelledby="menuNode" data-parent="#accordion">
                <div class="card-header">
                    <h5 class="mb-0"> Add to Menu </h5>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Menu Link Title</label>
                        {!! Form::text('menu[add][name]', isset($model) ? $model->name : null, ['class' => 'form-control', 'placeholder' => 'Enter Menu Name']) !!}
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group">
                        <label>Parent Menu </label>
                        {!! Form::select('menu[add][menu_id]', $menus, null, ['class' => 'form-control', 'placeholder' => 'Enter Parent Menu']) !!}
                        <span class="help-block"></span>
                    </div>
				</div>	
                <div class="card-footer"></div>

                @if(isset($model) && !is_null($model->menuable))
                    @foreach($model->menuable as $m => $menuable)
                        <div class="card-container">
                            <div class="card-header ">
                                <h6 class="mb-0  clearfix"> 
                                    Menu : {{ $menuable->menu->name }} 
                                    <a class="btn btn-danger pull-right" name="btn_menu_node_remove"><i class="fa fa-trash"></i></a>
                                </h6>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Menu Link Title</label>
                                    {!! Form::text('menu[nodes]['. $menuable->id .'][name]', $menuable->name , ['class' => 'form-control', 'placeholder' => 'Enter Menu Name']) !!}
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Parent Menu </label>
                                    {!! Form::select('menu[nodes]['.$menuable->id.'][menu_id]', $menus, 
                                        $menuable->menu_id . ($menuable->parent_id ? '_' . $menuable->parent_id : null ), 
                                        ['class' => 'form-control', 'placeholder' => 'Enter Parent Menu']) !!}
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="card-footer"></div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

    </div>
</div>
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('[name=btn_menu_node_remove]').click(function () {
                $(this).closest('.card-container').remove();
            })
        })
    </script>
@endpush