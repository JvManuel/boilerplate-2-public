@if(! isset($nodes))
 	@if(count($menus))
 		@foreach($menus as $m => $menu)
		 	<option value="{{ $menu->id }}" @if($menuable && ! $menuable->parent_id && $menuable->menu_id == $menu->id) selected="true" @endif><b>{{ $menu->name }}</b></option>
		 	@if(count($menu->children))
		 		@include('menu::backend.partials.optionForm', ['menu' => $menu, 'nodes' => $menu->children, 'label' => '-'])
		 	@endif
	 	@endforeach
 	@endif
@else
	@foreach($nodes as $n => $node)
	 	<option value="{{ $menu->id }}_{{ $node->id }}" @if($menuable && $menuable->parent_id && $menuable->parent_id == $node->id) selected="true" @endif>{{ $label }} {{ $node->name }}</option>
	 	@if(count($node->children))
	 		@include('menu::backend.partials.optionForm', ['menu' => $menu, 'nodes' => $node->children, 'label' => $label . '-'])
	 	@endif
	@endforeach
@endif