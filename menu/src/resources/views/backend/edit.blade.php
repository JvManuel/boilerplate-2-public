@extends('backend.layouts.app')

@section ('title', 'Menu Management | Edit Menu')

@section('breadcrumb-links')
    @include('menu::backend.includes.breadcrumb-links')
@endsection

@section('content')
    {!! Form::model($model, ['url' => route('admin.menu.update', $model), 'file' => 'mutlipart/enctype', 'files' => true, 'id' => 'content-form', 'class' => 'form-horizontal', 'method' => 'PATCH' ]) !!}
        <div class="card card-form">
           
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Menu Management
                            <small class="text-muted">Edit Menu</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                @include('menu::backend.includes.fields')
                <hr/> @include('menu::backend.partials.widget')
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <a href="{{ route('admin.menu.show', $model) }}" class="btn btn-danger btn-sm">Cancel</a>
                    </div><!--col-->
                    <div class="col text-right">
                        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-sm btn-submit']) }}
                    </div><!--row-->
                </div><!--row-->
            </div><!--card-footer-->
        </div>
    {!! Form::close() !!}
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { formSubmit($('#content-form')) })
    </script>
@endpush

