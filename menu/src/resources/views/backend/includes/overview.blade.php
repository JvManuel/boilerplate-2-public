<div class="col">
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>Name</th>
                <td>{{ $model->name }}</td>
            </tr>
            <tr>
                <th>Depth</th>
                <td>{{ $model->depth }}</td>
            </tr>
            <tr>
                <th>Template</th>
                <td>{{ config('package.menu.templates')[$model->template] }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td class="pre-line">{{ $model->description }}</td>
            </tr>

            <tr>
                <th>Status</th>
                <td>{!! $model->status == 'active' ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">In Active</span>' !!}</td>
            </tr>
        </table>
    </div>
</div>