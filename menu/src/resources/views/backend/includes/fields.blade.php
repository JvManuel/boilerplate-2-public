<div class="row mt-4 mb-4">
    <div class="col">
        @if(isset($model))
            <div class="form-group row">
                <label class="col-md-2 form-control-label">Name</label>
                <div class="col-md-10">
                    <div class="form-control" disabled>{{ $model->name }}</div>
                </div>
            </div>
        @else
            <div class="form-group row">
                <label class="col-md-2 form-control-label">Name</label>
                <div class="col-md-10">
                    {!! Form::text('name', old('name'), ['class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : '' ), 
                        'placeholder' => 'Enter name', 'autofocus' => true]) !!}
                </div>
            </div>
        @endif
        <div class="form-group row">
            <label class="col-md-2 form-control-label">Depth</label>
            <div class="col-md-10">
                {!! Form::number('depth', old('depth'), ['class' => 'form-control ' . ($errors->has('depth') ? 'is-invalid' : '' ), 
                    'placeholder' => 'Enter depth', 'max' => 5, 'min' => 1, 'step' => 1]) !!}
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label">Template</label>
            <div class="col-md-10">
                {!! Form::select('template', config('package.menu.templates'), old('template'), ['class' => 'form-control ' . ($errors->has('template') ? 'is-invalid' : '' ), 
                    'placeholder' => 'Enter template', 'autofocus' => true]) !!}
            </div>
        </div>

        @if(isset($model))
            <div class="form-group row">
                <label class="col-md-2 form-control-label">Description</label>
                <div class="col-md-10">
                    <div class="form-control" disabled>{!! $model->description !!}</div>
                </div>
            </div>
        @else
            <div class="form-group row">
                <label class="col-md-2 form-control-label">Description</label>
                <div class="col-md-10">
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control ' . ($errors->has('description') ? 'is-invalid' : '' ), 
                        'placeholder' => 'Enter description', 'rows' => 3]) !!}
                </div>
            </div>
        @endif
        <div class="form-group row">
            <label class="col-md-2 form-control-label">Status<br/><small>This menu will not be visible on the system if turned off</small></label>
            <div class="col-md-10">
                <label class="switch switch-3d switch-primary">
                    {!! Form::checkbox('status', 1, (isset($model) && $model->status == 'active' || ! isset($model)), ['class' => 'switch-input']) !!}
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->
    </div>
</div>