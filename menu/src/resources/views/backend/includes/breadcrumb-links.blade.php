<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if (Active::checkUriPattern('admin/menu'))
                    Active Menu
                @elseif (Active::checkUriPattern('admin/menu/create') && config('package.menu.can_create'))
                    Create Menu
                @elseif (Active::checkUriPattern('admin/menu/inactive'))
                    Inactive List
                @elseif (Active::checkUriPattern('admin/menu/activity'))
                    Activities
                @else 
                    Menu
                @endif
            </a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                @can('menu list') <a class="dropdown-item" href="{{ route('admin.menu.index') }}">Active Menu</a> @endcan
                @can('menu inactive') <a class="dropdown-item" href="{{ route('admin.menu.inactive') }}">Inactive List</a> @endcan
                @can('menu activity') <a class="dropdown-item" href="{{ route('admin.page.activity') }}">Activities</a> @endcan
                @if(config('package.menu.can_create'))
                    @can('menu create') <a class="dropdown-item" href="{{ route('admin.menu.create') }}">Create Menu</a> @endcan
                @endif
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>