<script type="text/x-custom-template" id="tpl_list_group">
    <ol class="dd-list"></ol>
</script>

<script type="text/x-custom-template" id="tpl_list_item">
    <li class="dd-item dd3-item" data-type="__type__" data-id="__id__" data-node_id="__node_id__">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
            <span class="text pull-left">__name__</span>
            <span class="text pull-right">__type__</span>
            <a href="javascript:;" title="Toggle item details" class="show-item-details">
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="clearfix"></div>
        </div>
        <div class="item-details">
            __details__
            <div class="text-right">
                <a href="#" title="" class="btn-danger btn-flat  btn-remove btn-sm"  data-type="__type__">Remove</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </li>
</script>
<script type="text/x-custom-template" id="tpl_node_fields">
    <div class="fields">
        <div class="form-group">
             <label class="control-label">
                <b>Name</b>
            </label>
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'autocomplete' => 'off', 'data-field' => 'name', 'placeholder' => '' ]) !!}
        </div>
        __url_field__
       {{--  <div class="form-group">
             <label class="control-label">
                <b>CSS Class</b>
            </label>
            {!! Form::text('options_class', old('class'), ['class' => 'form-control', 'autocomplete' => 'off', 'data-field' => 'class', 'placeholder' => '' ]) !!}
        </div>
         <div class="form-group">
             <label class="control-label">
                <b>Icon </b>
            </label>
            {!! Form::text('options_icon', old('icon'), ['class' => 'form-control', 'autocomplete' => 'off', 'data-field' => 'icon', 'placeholder' => '' ]) !!}
        </div>
        <div class="form-group">
             <label class="control-label">
                <b>Target</b>
            </label>
            {!! Form::select('options_target', 
                ['Not set', 'Self', 'Blank', 'Parent', 'Top'], old('target'), 
                ['class' => 'form-control', 'data-field' => 'target', 'autocomplete' => 'off', 'data-field' => 'target' ]) !!}
        </div> --}}
    </div>
</script>

<script type="text/x-custom-template" id="tpl_node_url">
    <div class="form-group">
         <label class="control-label">
            <b>Url</b>
        </label>
        {!! Form::text('url', old('url'), ['class' => 'form-control', 'autocomplete' => 'off', 'data-field' => 'url', 'placeholder' => '' ]) !!}
    </div>
</script>