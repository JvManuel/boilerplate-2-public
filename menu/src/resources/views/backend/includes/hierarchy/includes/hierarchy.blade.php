<script type="text/javascript">
     $.fn.serializeObject=function(){"use strict";var a={},b=function(b,c){var d=a[c.name];"undefined"!=typeof d&&d!==null?$.isArray(d)?d.push(c.value):a[c.name]=[d,c.value]:a[c.name]=c.value};return $.each(this.serializeArray(),b),a};

let MenuHierarchyUI = function(){
    "use strict";

    let $BOX        = $('#menu_hierarchy_form');
    let $STRUCTURE  = $('#menu_hierarchy');

    let NODES   = [];
    let DELETED = [];
    let REQUEST = null;
    
    let _TEMPLATES = {
        // Main Template
        overlay : `
            <div class="overlay">
                <div class="loader-container text-center">
                    <div class="text-center"><i class="fa fa-spin fa-spinner fa-3x"></i></div>
                    <div class="text-center">Loading...</div>
                </div>
            </div>
        `,
        list: $('#tpl_list_group').html(),
        item: $('#tpl_list_item').html(),
        fields : $('#tpl_node_fields').html(),
        url : $('#tpl_node_url').html(),
    };

    let _SETTINGS = {
        nodeLink  : '',
        nodeStore : '',
        depth: 5
    };

    let _RENDER = {
        structure : function (list) {
            $STRUCTURE.append(_RENDER.list(list, 'active'));
        },

        list : function (list, type) {
            var listGroup = $(_TEMPLATES.list);
            list.forEach(function (node, index) {
                listGroup.append(_RENDER.node(node, type));
            });

            if(list.length <= 0){
               $STRUCTURE.append('<div class="dd-empty"></div>');
            }


            return listGroup;
        },
        node: function (node, type) {
            let listItem = _TEMPLATES.item;
            let name = array_get(node, 'name', 'Name');
            let id   = type == 'inactive' ? null : array_get(node, 'id',   null);
            let children = array_get(node, 'children', []);

            listItem = listItem.replace(/__name__/gi, name);
            listItem = listItem.replace(/__type__/gi, array_get(node, 'type', ''));
            listItem = listItem.replace(/__id__/gi, id);
            listItem = listItem.replace(/__details__/gi,  _TEMPLATES.fields);
            listItem = listItem.replace(/__url_field__/gi, array_get(node, 'type', 'custom') == 'custom' ? _TEMPLATES.url : "");
            let $listItem = $(listItem);
            let options = json_decode(node.options);

            $listItem.find('input[name=name]').val(array_get(node, 'name', 'Name'));
            $listItem.find('input[name=url]').val(array_get(node, 'url', '#'));
            $listItem.find('select[name=options_target]').val(array_get(options, 'target', ''));
            $listItem.find('input[name=options_id]').val(array_get(options, 'id', ''));
            $listItem.find('input[name=options_class]').val(array_get(options, 'class', ''));
            $listItem.find('input[name=options_icon]').val(array_get(options, 'icon', ''));
            if(typeof children !== 'undefined'){
                if (children.length > 0) {
                    $listItem.append(_RENDER.list(array_get(node, 'children', []), type));
                }
            }
            return $listItem;
        },
    };

    let _ACTIONS = {
        abort: function(){ if(REQUEST){ REQUEST.abort(); } },
        details : function (){
            /**
             * Toggle item details
             */
            $('body').on('click', '#menu_hierarchy .dd-item .dd3-content a.show-item-details', function (event) {
                event.preventDefault();
                $(this).toggleClass('active');
                $(this).closest('.dd-item').toggleClass('active');
            });

            /**
             * Change details value
             */
            $('body').on('change keyup', '#menu_hierarchy .dd-item .item-details .fields input[type=text], .dd-item .item-details .fields select', function (event) {
                event.preventDefault();
                var $current = $(this);
                var $label = $current.closest('label'),
                    $currentItem = $current.closest('.dd-item');
                $currentItem.data($label.attr('data-field'), $current.val());
            });
        },
        refresh : function (){
            $('body').on('click', '#menu_hierarchy_form [name=btn_refresh]', function(e){
                e.preventDefault();
                _ACTIONS.abort();
                _INIT.initNodes('refresh');
            });
        },
        removeNode: function (){
            /**
             * Remove node
             */
            $('body').on('click', '#menu_hierarchy .dd-item .item-details .btn-remove', function (event) {
                event.preventDefault();
                var $parent = $(this).closest('.dd-item');
                var $childs = $parent.find('> .dd-list > .dd-item');
                if (array_length($childs)) {
                    $parent.after($childs);
                }
                DELETED.push($parent.data('id'));
                $parent.remove();
            });

            $("body").on("click", "#menu_hierarchy .dd-item a[name=btn_delete]", function(e) {
                e.preventDefault();
                let linkURL = $(this).attr("href"),
                    linkRedirect = $(this).attr("data-redirect");
                deleteSwal(linkURL, linkRedirect)
            });

        },
        save: function() {
            let structure = _ACTIONS.serialize();
            $BOX.prepend(_TEMPLATES.overlay);
            swalLoader();
            _ACTIONS.abort();
            REQUEST = $.ajax({
                url: _SETTINGS.nodeStore,
                type: 'PATCH',
                dataType: 'json',
                data: { hierarchy: structure, deleted: DELETED},
                success: function(response){
                    $STRUCTURE.empty();
                    _INIT.instance(response.data);
                    swal.close();
                    $BOX.find('.overlay').remove();
                }, 
                error: function(response){
                    // swalError('An error occured while processing your request.', function(){
                        _INIT.initNodes();
                        $BOX.find('.overlay').remove();
                    // });
                }
            });
        },
        serialize : function () {
            // let fields = $STRUCTURE.nestable('serialise');
            // return fields;

            var data,
                depth = 0,
                list  = $STRUCTURE,
                step  = function(level, depth)
                {
                    var array = [ ],
                        items = level.children('li');
                    items.each(function()
                    {
                        var li   = $(this),
                            fields = li.find('.fields:first'),
                            sub  = li.children('ol');
                        let item = li.data();
                            fields.find(':input, select, textarea').each(function(){
                                item[$(this).attr('name')] = $(this).attr('type') == 'checkbox' ? $(this).is(':checked') : $(this).val();
                            });

                        if (sub.length) {
                            item.children = step(sub, depth + 1);
                        }
                        array.push(item);
                    });
                    return array;
                };
            data = step(list.find('ol').first(), depth);
            return data;

        },
    };  

    let _INIT = {
        initNodes : function(process) {
            $BOX.prepend(_TEMPLATES.overlay);
            _ACTIONS.abort();
            REQUEST = $.ajax({
                url: _SETTINGS.nodeLink,
                type: 'POST',
                dataType: 'json',
                data: { }, 
                success: function(response){
                    $BOX.find('.overlay').remove();
                    _INIT.instance(response.data);
                    // _INIT.trigger();
                },
                error: function(response){
                    console.log(process);
                    if(process == 'refresh'){

                    }else{
                        // swalError('An error occured while loading the fields.', function(){
                            $BOX.find('.overlay').remove();
                        // });
                    }
                }
            });
        },
        instance : function(data) {
            _INIT.loadFields(data);
            _INIT.initNestables();
            
        },

        initNestables : function() {
            $STRUCTURE.nestable({
                group: 1,
                expandBtnHTML: '',
                collapseBtnHTML: '',
                maxDepth        : _SETTINGS.depth,
            });

        },
        loadFields : function (items) {
            $STRUCTURE.empty();
            _RENDER.structure(items);
        },
    };

    return {
        constuctor: function(){
            return {
                init: function(settings){
                    _SETTINGS.nodeLink = settings.nodeLink
                    _SETTINGS.nodeStore = settings.nodeStore
                    _SETTINGS.depth = settings.depth


                    _ACTIONS.refresh();
                    _INIT.initNodes();
                    _ACTIONS.details();
                    _ACTIONS.removeNode();
                    _ACTIONS.refresh();
                    $('[name=btn_hierarchy_update]').click(function() { 
                       _ACTIONS.save(); 
                    });
                }, 
                refresh : function (){
                    _ACTIONS.refresh();
                    _INIT.initNodes();
                }
            }
        }
        
    };

}();

</script>