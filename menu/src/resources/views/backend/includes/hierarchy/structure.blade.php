<div class="card" id="menu_hierarchy_form">
	<div class="card-header">
		{{ $model->name }} Hierarchy
	</div>
	<div class="card-body">
        <div id="menu_hierarchy" class="dd"></div>
	</div>
	<div class="card-footer clearfix">
		<a class="btn btn-success pull-right" name="btn_hierarchy_update"><i class="fa fa-save"></i> Update Hierarchy</a>
	</div>
</div>

@push('after-styles')
	@include('menu::backend.includes.hierarchy.includes.css')
@endpush
@push('after-scripts')

		@include('menu::backend.includes.hierarchy.includes.template')
		@include('menu::backend.includes.hierarchy.includes.js')
		@include('menu::backend.includes.hierarchy.includes.hierarchy')
		
	    <script type="text/javascript">
            $(document).ready(function () {
                var menuHierarchyUI = new MenuHierarchyUI.constuctor();
                    menuHierarchyUI.init({
                        nodeLink  : '{{ route('admin.menu.hierarchy.table', $model) }}',
                        nodeStore : '{{ route('admin.menu.hierarchy.update', $model) }}',
                        depth: {{ $model->depth ?? 5 }}
                    });
            })
	    </script>
@endpush