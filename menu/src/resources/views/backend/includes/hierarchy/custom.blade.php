{!! Form::open(['class' => 'card card-form', 'id' => 'custom-form', 'url' => route('admin.menu.hierarchy.store', $model)]) !!}
	<div class="card-header"> Custom Link </div>
	<div class="card-body">
		<div class="form-group">
			<label>Name</label>
			{!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			<label>Url</label>
			{!! Form::text('url', old('url'), ['class' => 'form-control']) !!}
		</div>
	</div>
	<div class="card-footer clearfix">
		{!! Form::submit('Add Link', ['class' => 'btn btn-sm btn-success pull-right']) !!}
	</div>
{!! Form::close() !!}

@push('after-scripts')
	<script type="text/javascript">
		$('#custom-form').submit(function (event) {
	        var formId = this.id,
	            form = this;
	        event.preventDefault();
	        $(this).prepend(`
	           <div class="overlay">
	                <div class="loader-container text-center">
	                    <div class="text-center"><i class="fa fa-spin fa-spinner fa-3x"></i></div>
	                    <div class="text-center">Submitting request...</div>
	                </div>
	            </div>
	        `)
	        setTimeout( function () {
	            form.submit();
	        }, 1000);
	    })
	</script>
@endpush