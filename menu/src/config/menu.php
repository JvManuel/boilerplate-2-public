<?php

return [

    'can_create' => false,

    'can_delete' => false,

    'views' => ['frontend.layouts.app'],
    'templates' => [
        'default' => 'Default',
        'navbar' => 'Navigation'

    ]

];
