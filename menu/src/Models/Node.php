<?php 

namespace PackageHalcyon\Menu\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use PackageHalcyon\History\Traits\Historable;

class Node extends Model
{
    use Sluggable, SluggableScopeHelpers, Historable;

    protected $table = "menu_nodes";

    protected $fillable = [
        'name', 'slug', 'url', 'order', 'options', 'type',
        'menu_id', 'parent_id', 'menuable_id', 'menuable_type'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function option($key)
    {
        $options = json_decode($this->options);
        return array_key_exists($key, $options) ? $options[$key] : null;
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }
    
    public function parent()
    {
        return $this->belongsTo(Node::class, 'parent_id');
    }

    public function nodes()
    {
        return $this->hasMany(Node::class, 'parent_id');
    }

    public function menuable()
    {
        return $this->morphTo();
    }

    public function getLinkAttribute()
    {
        if ($this->type == 'custom') {
            return $this->url;
        }
        // if (method_exists($this->menuable(), 'bootSoftDeletes') && ! $this->menuable) {
        // }
        if ($this->menuable) {
            return $this->menuable->action($frontend = true, 'show');
        }
        return null;
    }
}
