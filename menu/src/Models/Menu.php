<?php 

namespace PackageHalcyon\Menu\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use PackageHalcyon\Base\Traits\Baseable;
use PackageHalcyon\History\Traits\Historable;

class Menu extends Model
{
    use Sluggable, SluggableScopeHelpers, Baseable, Historable;

    protected $fillable = [
        'name', 'slug', 'description', 'depth', 'status', 'template'
    ];

    /**
     * Return the baseable configuration array of this model
     **/
    public function baseable()
    {
        return [
            'source' => 'name'
        ];
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function nodes()
    {
        return $this->hasMany(Node::class);
    }

    public function actions($frontend = false)
    {
        $user = auth()->user();
        $actions = [];

        if ($frontend) {
            return $actions;
        }

        if ($user->can('menu show')) {
            $actions['show'] = ['type' => 'show',      'link' => route('admin.menu.show', $this)];
        }
        if ($user->can('menu update')) {
            $actions['edit'] = ['type' => 'edit',      'link' => route('admin.menu.edit', $this)];
        }
        
        if ($user->can('menu delete') && config('package.menu.can_delete')) {
            $actions['delete'] = ['type' => 'delete',    'link' => route('admin.menu.destroy', $this) , 'redirect' => route('admin.menu.index')];
        }

        return $actions;
    }
}
