<?php

namespace PackageHalcyon\Menu\Repositories;

use PackageHalcyon\Menu\Models\Node as Model;
use Illuminate\Support\Facades\DB;
use Uploader;
use Meta;
use PackageHalcyon\Base\BaseRepository;
use PackageHalcyon\History\Facades\History;
use PackageHalcyon\Menu\Facades\Menu;

/**
 * Class HierarchyRepository.
 */
class HierarchyRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Model::class;

    protected $nodes;
    /**
     * @var Node Model
     */
    protected $model;
    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model  = $model;
    }

    //

    public function store($request, $menu)
    {
        DB::beginTransaction();
        try {
            $data = $request->except(['_token', '_method']);
            $data['type'] = 'custom';
            $data['order'           ] = $menu->nodes()->whereNull('parent_id')->count();
            $data['options'         ] = json_encode($request->options ?? []);
            $display = $menu->nodes()->create($data);
           
            DB::commit();
            return $menu;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function update($request, $menu)
    {
        DB::beginTransaction();
        try {
            // Delete
            $nodes = $request->deleted;
            if ($request->deleted && !is_array($nodes)) {
                $nodes = json_decode($nodes ?? []);
            }
            $this->nodeDelete($menu, $nodes);
            // Structure
            $hierarchy = $request->hierarchy ?? [];
            if (!is_array($hierarchy)) {
                $hierarchy = json_decode($hierarchy, true);
            }

            $list = $menu->nodes()->orderBy('order', 'ASC')->get();
            $nodes = collect();
            foreach ($hierarchy as $order => $node) {
                $nodes = $nodes->push($this->updateNode($menu, $list, $node, $order));
            }

            DB::commit();
            return Menu::hierarchy($menu);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }


    public function updateNode($menu, $list, $node, $order, $parentID=null)
    {
        DB::beginTransaction();
        try {
            $options = [];
            if (count($node)) {
                foreach ($node as $n => $field) {
                    if (str_contains($n, ['options_'])) {
                        $options[str_replace('options_', '', $n)] = $field;
                    }
                }
            }
            $data = ['name' => $node['name'], 'url' => $node['type'] == 'custom' ? $node['url'] : null, 'order' => $order, 'parent_id' => $parentID, 'options' => json_encode($options)];
            if (array_key_exists('id', $node) && $node['id']) {
                $result = $list->where('id', $node['id'])->first();
                $result->update($data);
            }

            $children = array_get($node, 'children', []);
            if (count($children)) {
                foreach ($children as $key => $child) {
                    $this->updateNode($menu, $list, $child, $key, $node['id']);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            $this->exceptions($e->getMessage());
        }
    }

    /**
     * Delete items by id
     * @param EloquentBase|int|array|null $id
     * @return mixed
     */
    public function nodeDelete($menu, $id = null)
    {
        $nodes = null;
        if (is_array($id)) {
            $nodes = $this->model->whereIn('id', $id);
        } elseif ($id instanceof Model) {
            $nodes = $id;
        } else {
            $nodes = $this->model->where('id', '=', $id);
        }
        try {
            $nodes->delete();
            return true;
        } catch (\Exception $e) {
            $this->exceptions($e->getMessage());
        }
    }
}
