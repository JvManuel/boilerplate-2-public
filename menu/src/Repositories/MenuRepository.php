<?php

namespace PackageHalcyon\Menu\Repositories;

use PackageHalcyon\Menu\Models\Menu as Model;
use Illuminate\Support\Facades\DB;
use Uploader;
use Meta;
use PackageHalcyon\Base\BaseRepository;
use PackageHalcyon\History\Facades\History;

/**
 * Class MenuRepository.
 */
class MenuRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Model::class;

    /**
     * @var Slider Model
     */
    protected $model;
    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model  = $model;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Model::class;
    }

    public function table($request)
    {
        $query = $this->model->query()->select([ 'id', 'name', 'status', 'slug', 'updated_at' ]);
        if ($request->status == '1' || $request->status == '0') {
            $query->where('status', $request->status == '1' ? 'active' : 'inactive');
        }
        return $query;
    }

    /**
     * @param Request $request
     * @param Model  $model
     *
     * @return static
     */
    public function store($request, $model = null)
    {
        $data = $this->generateStub($request, $model);
        DB::beginTransaction();
        try {
            if ($model) {
                $model->update($data);
                History::log($model, ['action' => 'updated', 'icon' => 'fa fa-pencil', 'color' => 'bg-primary']);
            } else {
                if (config('package.menu.can_create')) {
                    $model = $this->model->create($data);
                    History::log($model, ['action' => 'created', 'icon' => 'fa fa-plus', 'color' => 'bg-success']);
                }
            }

            DB::commit();
            return $model;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Create stub in storing Inquiries
     * @param Request $request
     * @param Model $model
     * @return array $data
     */
    public function generateStub($request, $model=null)
    {
        $data = $request->except(['_token', '_method', 'status', '']);
        $data['status'] = $request->status ? 'active' : 'inactive';
        $data['slug'] = str_slug($data['name']);
        return $data;
    }
}
