<?php

namespace PackageHalcyon\Minifier\Middleware;

use Closure;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (\App::environment('production')) {
//        if ($response instanceof Illuminate\Http\Response)
//        {
            $buffer = $response->getContent();
            if (substr($buffer, 0, 15) != '<!DOCTYPE html>') {
                // do nothing
                $replace = [];
            } elseif (strpos($buffer, '<pre>') !== false) {
                $replace = array(
                    '/<!--[^\[](.*?)[^\]]-->/s' => '',
                    "/<\?php/" => '<?php ',
                    "/\r/" => '',
                    "/>\n</" => '><',
                    "/>\s+\n</" => '><',
                    "/>\n\s+</" => '><',
                );
            } else {
                $replace = array(
                    '/<!--[^\[](.*?)[^\]]-->/s' => '',
                    "/<\?php/" => '<?php ',
                    "/\n([\S])/" => '$1',
                    "/\r/" => '',
                    "/\n/" => '',
                    "/\t/" => '',
                    "/ +/" => ' ',
                    '#^\s*//.+$#m' => '', // remove single comment in JS
                );
            }
            $buffer = preg_replace(array_keys($replace), array_values($replace), $buffer);
            $response->setContent($buffer);
//        }
        }

        return $response;
    }
}
