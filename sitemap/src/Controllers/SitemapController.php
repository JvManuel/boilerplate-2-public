<?php

namespace PackageHalcyon\Sitemap\Controllers;

use App\Http\Controllers\Controller;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Support\Facades\Route;

/**
 * Class SitemapController.
 */
class SitemapController extends Controller
{
    public function index()
    {
        $html = new \Htmldom(public_path('sitemap.xml'));
        return view('sitemap::index', compact('html'));
    }
    public function generate()
    {
        SitemapGenerator::create(url('/'))
        ->hasCrawled(function (Url $url) {
            if ($url->segment(1) === 'admin') {
                return;
            }
            return $url;
        })
        ->writeToFile(public_path('sitemap.xml'));
        return redirect()->route('frontend.sitemap.index');
    }
}
