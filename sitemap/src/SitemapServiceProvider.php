<?php
namespace PackageHalcyon\Sitemap;

use Illuminate\Support\ServiceProvider;

class SitemapServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    public function register()
    {
        // register our controller
        $this->app->make('PackageHalcyon\Sitemap\Controllers\SitemapController');
        $this->loadViewsFrom(__DIR__.'/views', 'sitemap');
    }
}
