<!DOCTYPE html>
<html>
<head>
	<title>Sitemap | {{ app_name() }}</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend.css') }}">
</head>
<body>
	<div class="container">
		<h4>Sitemap file: <a href="{{ url('/sitemap.xml') }}">{{ url('/sitemap.xml') }}</a></h4>
		<p>Number of URLs in this sitemap: {{ count($html->find('url')) }}</p>
		<br>
		<div class="col-md-12">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>URL Location</th>
						<th>Last Modification Date</th>
						<th>Change Frequency</th>
						<th>Priority</th>
					</tr>
				</thead>
				<tbody>
					@foreach($html->find('url') as $url)
					<tr>
						<td><a href="{{ $url->find('loc', 0)->plaintext }}">{{ $url->find('loc', 0)->plaintext }}</a></td>
						<td>{{ $url->find('lastmod', 0)->plaintext }}</td>
						<td>{{ $url->find('changefreq', 0)->plaintext }}</td>
						<td>{{ $url->find('priority', 0)->plaintext }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>