<?php
/**
 * All route names are prefixed with 'admin.content'.
 */
Route::group([
    'namespace'  => 'PackageHalcyon\Sitemap\Controllers',
    'middleware' => ['web'], 'as' => 'frontend.'
], function () {
    Route::get('sitemap', 'SitemapController@index')->name('sitemap.index');
    Route::get('sitemap/generate', 'SitemapController@generate')->name('sitemap.generate');
});
