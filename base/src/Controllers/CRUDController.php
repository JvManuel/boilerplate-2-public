<?php

namespace PackageHalcyon\Base\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use PackageHalcyon\Base\Controllers\Traits\CRUDTraits;

/**
 * Class CRUDController.
 */
class CRUDController extends Controller
{
    use CRUDTraits;

    public $config;

    public function index()
    {
        return view($this->config->view . '.index');
    }

    public function create()
    {
        return view($this->config->view . '.create');
    }

    public function edit($slug)
    {
        $model = $this->getModel($slug);
        return view($this->config->view . '.edit', compact('model'));
    }

    public function show($slug)
    {
        $model = $this->getModel($slug);
        return view($this->config->view . '.show', compact('model'));
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules());
        $model = $this->repo->store($request);
        return $this->response('store', $request, $model);
    }

    public function update(Request $request, $slug)
    {
        $model = $this->getModel($slug);
        $this->validate($request, $this->updateRules($model));
        $model = $this->repo->store($request, $model);
        return $this->response('update', $request, $model);
    }

    public function destroy(Request $request, $slug)
    {
        $model = $this->getModel($slug);
        $this->repo->destroy($model);
        return $this->response('delete', $request, $model);
    }
}
