@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/dataTables.bootstrap4.min.css') }}">
@endpush
@push('after-scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap4.min.js') }}"></script>
@endpush