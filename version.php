<?php

/*
 * Halcyon Packages version name
 */
if (! function_exists('versionNameHalcyonBoilerPlatePackages')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function versionNameHalcyonBoilerPlatePackages()
    {
        return 'v4.3.6';
    }
}
