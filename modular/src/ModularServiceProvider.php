<?php

namespace PackageHalcyon\Modular;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class ModularServiceProvider extends ServiceProvider
{
    protected $files;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->files = new Filesystem;
        $this->registerMakeCommand();
    }

    /**
     * Register the "make:module" console command.
     *
     * @return Console\ModuleMakeCommand
     */
    protected function registerMakeCommand()
    {
        $this->commands('modules.make');
        $this->commands('modules.delete');
        $this->commands('modules.status');

        $bind_method = method_exists($this->app, 'bindShared') ? 'bindShared' : 'singleton';
        $this->app->{$bind_method}('modules.make', function ($app) {
            return new Console\ModuleMakeCommand($this->files);
        });
        $this->app->{$bind_method}('modules.delete', function ($app) {
            return new Console\ModuleDeleteCommand($this->files);
        });
        $this->app->{$bind_method}('modules.status', function ($app) {
            return new Console\ModuleStatusCommand($this->files);
        });
    }
}
