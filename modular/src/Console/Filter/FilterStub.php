<?php

namespace PackageHalcyon\Modular\Console\Filter;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubFilterGeneratorInterface;

class FilterStub implements StubFilterGeneratorInterface
{

    /**
     *
     */
    const tagMigrationField       = '<<<migrationFields>>>';
    const tagFactoryField         = '<<<factoryFields>>>';
    const tagSlugNameTableColumn  = '<<<slugNameTableColumn>>>';
    const tagGetRouteKeyName      = '<<<RouteKeyNameTableName>>>';
    const tagModelFillable        = '<<<modelFillable>>>';
    const tagIndexViewFieldTop    = '<<<indexViewFieldTop>>>';
    const tagIndexViewFieldBottom = '<<<indexViewFieldBottom>>>';
    const tagCrudControllerStore  = '<<<crudControllerStore>>>';
    const tagCrudControllerUpdate = '<<<crudControllerUpdate>>>';
    const tagShowViewField        = '<<<showViewField>>>';
    const tagEditViewField        = '<<<editViewField>>>';

    /**
     *
     */
    const startSoftDeleteTag = '<<<softdelete';
    const endSoftDeleteTag   = 'softdelete>>>';

    /**
     *
     */
    const startSluggableTag = '<<<sluggable';
    const endSluggableTag   = 'sluggable>>>';

    /**
     *
     */
    const startStatusTag = '<<<status';
    const endStatusTag   = 'status>>>';

    /**
     *
     */
    const startActivityTag = '<<<activity';
    const endActivityTag   = 'activity>>>';

    /**
     *
     */
    const startHasFrontendTag = '<<<hasfrontend';
    const endHasFrontendTag   = 'hasfrontend>>>';

    /**
     *
     */
    const startDefaultTableSeeder        = '<<<defaultTableSeeder';
    const endDefaultTableSeeder        = 'defaultTableSeeder>>>';

    private static function _filterStub($value, $startTag, $endTag)
    {
        $return = '';
        $strlen = strlen($value);

        $start = false;
        for ($i = 0; $i <= $strlen; $i++) {
            $tag = substr($value, $i, strlen($startTag));
            if ($startTag == $tag) {
                $start = true;
            }

            if ($start && $endTag == $tag) {
                $start = false;
            }

            if ($start) {
                // skip char
                continue;
            }

            $return .= substr($value, $i, 1);
        }

        return $return;
    }

    public static function generate($value, $options) :string
    {
        $value = self::_generateSoftDelete($value, $options->hasSoftDelete);
        $value = self::_generateActivity($value, $options->hasActivity);
        $value = self::_generateFrontend($value, $options->hasFrontend);
        $value = self::_generateStatus($value, $options->hasStatus);
        $value = self::_generateSluggable($value, $options);

        return $value;
    }

    private static function _generateSoftDelete($value, $hasSoftDelete)
    {
        if (!$hasSoftDelete) {
            $value = self::_filterStub($value, self::startSoftDeleteTag, self::endSoftDeleteTag);
        }

        // Remove tags
        $value = str_replace(self::startSoftDeleteTag, '', $value);
        return str_replace(self::endSoftDeleteTag, '', $value);
    }

    private static function _generateActivity($value, $hasActivity)
    {
        if (!$hasActivity) {
            $value = self::_filterStub($value, self::startActivityTag, self::endActivityTag);
        }

        // Remove tags
        $value = str_replace(self::startActivityTag, '', $value);
        return str_replace(self::endActivityTag, '', $value);
    }
    public static function replaceTableSeeder($value, bool $isDefault)
    {
        if (!$isDefault) {
            $value = self::_filterStub($value, self::startDefaultTableSeeder, self::endDefaultTableSeeder);
        }

        // Remove tags
        $value = str_replace(self::startDefaultTableSeeder, '', $value);
        return str_replace(self::endDefaultTableSeeder, '', $value);
    }

    private static function _generateFrontend($value, $hasFrontend)
    {
        if (!$hasFrontend) {
            $value = self::_filterStub($value, self::startHasFrontendTag, self::endHasFrontendTag);
        }

        // Remove tags
        $value = str_replace(self::startHasFrontendTag, '', $value);
        return str_replace(self::endHasFrontendTag, '', $value);
    }

    private static function _generateStatus($value, $hasStatus)
    {
        if (!$hasStatus) {
            $value = self::_filterStub($value, self::startStatusTag, self::endStatusTag);
        }

        // Remove tags
        $value = str_replace(self::startStatusTag, '', $value);
        return str_replace(self::endStatusTag, '', $value);
    }

    private static function _generateSluggable($value, $options)
    {
        if (!$options->hasSluggable) {
            $value = self::_filterStub($value, self::startSluggableTag, self::endSluggableTag);
        }

        // Remove tags
        $value = str_replace(self::startSluggableTag, '', $value);
        $value = str_replace(self::endSluggableTag, '', $value);

        if ($options->hasSluggable) {
            // for sluggable() coulm name from table
            $slugableFieldName = ($options->renderFieldCount === 0) ? 'name' : $options->sluggableFieldName;
            $value             = str_replace(self::tagSlugNameTableColumn, $slugableFieldName, $value);
        }

        // Column name for getRouteKeyName()
        $column = $options->hasSluggable ? 'slug' : 'id';
        return str_replace(self::tagGetRouteKeyName, $column, $value);
    }

    public static function generateCrudControllerValidation($value, $fieldsString)
    {
        $value = (new self)->_replaceTag($value, self::tagCrudControllerStore, $fieldsString['store']);
        return (new self)->_replaceTag($value, self::tagCrudControllerUpdate, $fieldsString['update']);
    }

    public static function generateMigrationFields($value, $fieldsString)
    {
        return (new self)->_replaceTag($value, self::tagMigrationField, $fieldsString);
    }

    public static function generatefactoryFields($value, $fieldsString)
    {
        return (new self)->_replaceTag($value, self::tagFactoryField, $fieldsString);
    }

    public static function replaceFillable($value, $fieldsString)
    {
        return (new self)->_replaceTag($value, self::tagModelFillable, $fieldsString);
    }

    public static function replaceView($value, $fieldsString)
    {
        $value = (new self)->_replaceTag($value, self::tagIndexViewFieldTop, $fieldsString['top']);
        return (new self)->_replaceTag($value, self::tagIndexViewFieldBottom, $fieldsString['bottom']);
    }

    public static function replaceViewShow($value, $fieldsString)
    {
        return (new self)->_replaceTag($value, self::tagShowViewField, $fieldsString);
    }

    public static function replaceViewEdit($value, $fieldsString)
    {
        return (new self)->_replaceTag($value, self::tagEditViewField, $fieldsString);
    }

    private function _replaceTag($value, $tag, $newString)
    {
        return str_replace($tag, $newString, $value);
    }
}
