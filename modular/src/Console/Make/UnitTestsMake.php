<?php

namespace PackageHalcyon\Modular\Console\Make;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class UnitTestsMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

 
    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }
    public function structures()
    {
        $return = [];
        $return[]=[
            'generate'=>['tests\\Module\\Backend\\', '/stub/test/backend.stub', 'BackendTest', null, false, false],
            'create'=>[false, null],
        ];
        

        if ($this->_options->hasFrontend) {
            $return[]=[
                'generate'=>['tests\\Module\\Frontend\\', '/stub/test/frontend.stub', 'FrontendTest', null, false, false],
                'create'=>[false, null],
            ];
        }
        return $return;
    }
}
