<?php

namespace PackageHalcyon\Modular\Console\Make;

use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class SeedersMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;


    public function __toString() :string
    {
        return get_class();
    }


    public static function optionsSeeder($key = null)
    {
        // $this->_options->hasTableSeeder
        $options = [
            1 => 'Yes, with factories seeders ',
            2 => 'Yes, only empty',
            3 => 'No'
         ];

        if (!is_null($key)) {
            return $options[$key];
        }

        return $options;
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        if ($this->_options->hasTableSeeder === $this->optionsSeeder(3)) {
            return null;
        }

        $return = [];
        $filename = ucfirst($this->_args->getCamelCaseModuleInput);

        // Permission Table Seeder
        $return[]=[
            'generate'=>['database\\seeds\\Auth\\ModulePermission\\', '/stub/database/permissionSeeder.stub', $filename . 'PermissionTableSeeder', false, true],
            'create'=>[false, null],
        ];
        
        $isDefault = ($this->_options->hasTableSeeder === $this->optionsSeeder(1));
        $default = function ($value) use ($isDefault) {
            return FilterStub::replaceTableSeeder($value, $isDefault);
        };
        

        // Module Data Seeder
        $return[]=[
            'generate'=>['database\\seeds\\Module\\', '/stub/database/seeder.stub', $filename . 'TableSeeder', false, true],
            'create'=>[false, $default],
        ];
        
        return $return;
    }
}
