<?php

namespace PackageHalcyon\Modular\Console\Make;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class RepositoriesMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

   
    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }
    public function structures()
    {
        $modulename =  $this->_args->getCamelCaseModuleInput;

        return [
            'generate'=>['Repositories\\Backend\\' . ucfirst(studly_case($modulename)) . '\\', '/stub/repository.stub', 'Repository', true, false],
            'create'=>[false, null],
        ];
    }
}
