<?php

namespace PackageHalcyon\Modular\Console\Make;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class RoutesMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

 
    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }
    public function structures()
    {
        $return =[];
        $modulename =  $this->_args->getCamelCaseModuleInput;

        $return[]=[
            'generate'=>['routes\\backend\\', '/stub/routes/backend.stub', snake_case($modulename), false, true],
            'create'=>[false, null],
        ];

        if ($this->_options->hasFrontend) {
            $return[]=[
                'generate'=>['routes\\frontend\\', '/stub/routes/frontend.stub', snake_case($modulename), false, true],
                'create'=>[false, null],
            ];
        }
        return $return;
    }
}
