<?php

namespace PackageHalcyon\Modular\Console;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Maker\StubGeneratorTraits;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Status\StubStatusTrait;
use PackageHalcyon\Modular\Console\Filter\FilterStub as FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Traits\ConsoleUITrait;

class ModuleMakeCommand extends GeneratorCommand
{
    use StubGeneratorTraits, StubStatusTrait,ConsoleUITrait;
    protected $stubStatusFileName = 'modular.json';

    /**
     * Laravel version
     *
     * @var string
     */
    protected $version;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new module (folder structure)';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Module';

    /**
     * The current stub.
     *
     * @var string
     */
    protected $currentStub;
    private $_path;

    /**
     * Store all strings generated  files directory
     */
    private $_modulesFiles;

    private $dir = __DIR__;

    /**
     * Execute the console command >= L5.5.
     *
     * @return void
     */
    public function handle()
    {
        $this->version       = (int) str_replace('.', '', app()->version());
        $this->_modulesFiles = [];
        $this->setFilterStub(new FilterStub);

        $this->_options = new \stdClass;
        $seeder = new  \PackageHalcyon\Modular\Console\Make\SeedersMake;

        do {
            $this->moduleQuestions($seeder);
        } while (!(bool) ($this->ask('Confirm? (y/n)', false) == 'y'));
   
        $this->classes = [
            \PackageHalcyon\Modular\Console\Make\RoutesMake::class,
            \PackageHalcyon\Modular\Console\Make\ControllersMake::class,
            \PackageHalcyon\Modular\Console\Make\RepositoriesMake::class,
            \PackageHalcyon\Modular\Console\Make\ModelsMake::class,
            \PackageHalcyon\Modular\Console\Make\BreadcrumbsMake::class,
            \PackageHalcyon\Modular\Console\Make\MigrationsMake::class,
            $seeder,
            \PackageHalcyon\Modular\Console\Make\UnitTestsMake::class,
            \PackageHalcyon\Modular\Console\Make\ViewsMake::class,
        ];

        if ($this->_options->hasTableSeeder != $seeder->optionsSeeder(3)) {
            $this->classes[] = \PackageHalcyon\Modular\Console\Make\FactoriesMake::class;
        }
        
        $this->excecute($this->_options);
    }




    protected function moduleQuestions($seeder)
    {
        $intToString= function ($start, $end) {
            $return = [];
            foreach (range($start, $end) as $index) {
                $return[] = (string) $index;
            }
            return $return;
        };

        if ($this->option('basic')) {
            $this->_options->hasSoftDelete    = false;
            $this->_options->hasStatus        = false;
            $this->_options->hasFrontend      = true;
            $this->_options->hasActivity      = false;
            $this->_options->renderFieldCount = 0;
            $this->_options->hasSluggable     = false;
            $this->_options->hasTableSeeder   = true;
            $this->_options->hasTestCases     = false;
        } else {
            $this->_options->hasSoftDelete    = $this->muneOption('1/10. Has SoftDelete?', ['Yes','No'], true);
            $this->_options->hasStatus        = $this->muneOption('2/10. Include Status function? ', ['Yes','No'], true);
            $this->_options->hasFrontend      = $this->muneOption('3/10. Include Frontend Feature?', ['Yes','No'], true);
            $this->_options->hasActivity      = $this->muneOption('4/10. Include Activity Page?', ['Yes','No'], true);
            $this->_options->renderFieldCount = (int) $this->muneOption('5/10. Number of Fields?', array_merge(['default'], $intToString(1, 20)));
            if (($this->_options->renderFieldCount) > 0) {
                $types=  ['string', 'integer', 'boolean', 'text', 'datetime'];
                for ($i = 1; $i <= $this->_options->renderFieldCount; $i++) {
                    $filedName =snake_case($this->ask("Field $i Name?"));
                    $this->_options->fields[$i] = [
                        'name' => $filedName,
                        'type' => $types[$this->muneOption("Field $i.\"$filedName\" Type?", $types)],
                    ];
                }
            }
            $this->_options->hasSluggable = $this->muneOption('6/10. Has Sluggable?', ['Yes','No'], true);
            if ($this->_options->hasSluggable) {
                if (($this->_options->renderFieldCount) > 0) {
                    $this->_options->sluggableFieldName =  $this->_info()['fieldNames'][$this->muneOption('6.1/10. what column name will use?', $this->_info()['fieldNames'])];
                }
            }
            $this->_options->hasTableSeeder = $seeder::optionsSeeder()[$this->muneOption('8/10. Has Table Seeder?', $seeder::optionsSeeder())];
            $this->_options->hasTestCases   = $this->muneOption('9/10. Has Test Cases?', ['Yes','No'], true);
        }

        $info = $this->_info();

        $this->info('Summary on Module "' . $this->getCamelCaseModuleInput() . '"');
        $this->table(['key', 'value'], $info['info']);
        $this->table(['Fields', 'Data Types'], $info['fields']);
    }

    private function _info()
    {
        $fields     = [];
        $fieldNames = [];
        $info       = [];

        foreach ($this->_options as $key => $option) {
            if (!is_array($option)) {
                $info[] = [$key, is_bool($option) ? ($option ? 'true' : 'false') : $option];
            } elseif ($key == 'fields') {
                foreach ($option as $field) {
                    $fields[]     = [$field['name'], $field['type']];
                    $fieldNames[] = $field['name'];
                }
            }
        }
        return[
            'info' => $info,
            'fields' => $fields,
            'fieldNames' => $fieldNames,
        ];
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            ['name', InputArgument::REQUIRED, 'Module name.'],
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            ['no-migration', null, InputOption::VALUE_NONE, 'Do not create new migration files.'],
            ['no-translation', null, InputOption::VALUE_NONE, 'Do not create module translation filesystem.'],
            ['basic', null, InputOption::VALUE_NONE, 'Generate Basic Module.'],
        );
    }
}
