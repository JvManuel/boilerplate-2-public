<div class="col">
    <div class="table-responsive">
		<table class="table table-hover">
			<tbody>
				<tr>
					<th>Name</th>
					<td>{{ $model->metable->name }}</td>
				</tr>
				<tr>
					<th>Keywords</th>
					<td>{{ $model->metable->keywords }}</td>
				</tr>
				<tr>
					<th>Description</th>
					<td>{{ $model->metable->description }}</td>
				</tr>
				<tr>
					<th>Author</th>
					<td>{{ $model->metable->user->name }}</td>
				</tr>
			</tbody>
		</table>
		</div>
</div>