@if(isset($model) && $model->metable)
	@section('title', ($site_settings->key('site', 'title') ?? app_name()) . ' - ' . $model->metable->name )
	@section('meta_author', $model->metable->user->name)
	@section('meta_description', $model->metable->description ?? $site_settings->key('site', 'description'))
	@section('meta_keywords', $model->metable->keywords ?? str_slug($site_settings->key('site', 'title') ?? app_name()))

@endif