<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header" role="tab" id="headingOne">
                <h5 class="mb-0">
                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Meta Information
                    </a>
                </h5>
            </div>
            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label">Name</label>
                        <div class="col-md-10">
                            {!! Form::text('meta[name]', isset($model) && $model->metable ? $model->metable->name : old('meta.name'), 
                                ['class' => 'form-control ' . ($errors->has('meta.name') ? 'is-invalid' : '' ), 
                                'placeholder' => 'Enter name', 'autofocus' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label">Keywords</label>
                        <div class="col-md-10">
                            {!! Form::text('meta[keywords]', isset($model) && $model->metable ? $model->metable->keywords : old('meta.keywords'), 
                                ['class' => 'form-control ' . ($errors->has('meta.keywords') ? 'is-invalid' : '' ), 
                                'placeholder' => 'Enter keywords',  'data-role' => "tagsinput"]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label">Description</label>
                        <div class="col-md-10">
                            {!! Form::textarea('meta[description]', isset($model) && $model->metable ? $model->metable->description : old('meta.description'), 
                                ['class' => 'form-control ' . ($errors->has('meta.description') ? 'is-invalid' : '' ), 
                                'placeholder' => 'Enter description', 'rows' => 3]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/bootstrap-tagsinput.css') }}">
@endpush
@push('after-scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
@endpush
