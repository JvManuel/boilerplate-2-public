<?php 

namespace PackageHalcyon\Meta\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Meta extends Model
{
    use Sluggable, SluggableScopeHelpers;

    protected $fillable = [
        'name', 'slug', 'keywords', 'description', 'user_id', 'metable_id', 'metable_type'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function metable()
    {
        return $this->morphTo();
    }
    
    public function user()
    {
        // return $this->belongsTo(config('history.auth.user.model'));
        return $this->belongsTo(\App\Models\Auth\User::class);
    }
}
