<?php
namespace PackageHalcyon\Meta\Traits;

use PackageHalcyon\Meta\Models\Meta;

trait Metable
{
    public function metable()
    {
        return $this->morphOne(Meta::class, 'metable');
    }
}
