<?php

namespace PackageHalcyon\Meta;

use App\Models\Meta as Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Meta.
 */
class Meta
{
    /**
     * @param Request $request
     * @param Model  $content
     *
     * @return static
     */
    public function store($request, $content)
    {
        $request = (object) $request;
        $meta = $content->metable;
        $data = $this->generateData($request, $content, $meta);
        try {
            DB::beginTransaction();
            if ($meta) {
                $meta->update($data);
            } else {
                $meta = $content->metable()->create($data);
            }
            DB::commit();
            return $meta;
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }
    
    public function generateData($request, $model, $meta)
    {
        $data = $request->meta;
        $name = array_key_exists('name', $data) ? $data['name'] : null;
        // $name = property_exists($request, 'name') ? $request->name : null;
        if (! $name) {
            $name = method_exists($model, 'baseable') ? $model->base('meta_name') : ($meta ? $meta->name : $model->name);
        }
        $data['name'] = $name;
        $data['keywords'] =  array_key_exists('keywords', $data) ? $data['keywords'] : "" ;
        $data['description'] =  array_key_exists('description', $data) ? $data['description'] : "" ;
        $data['description'] = str_limit($data['description'], $limit = 150, $end = '');
        $data['user_id'] = array_key_exists('user_id', $data) && $data['user_id'] ? $data['user_id'] :  auth()->id();
        return $data;
    }
}
