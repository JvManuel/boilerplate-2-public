<?php
namespace PackageHalcyon\Meta;

use Illuminate\Support\ServiceProvider;
use PackageHalcyon\Meta\Meta;

class MetaServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('meta', function ($app) {
            return new Meta($app);
        });
        $this->app->alias('meta', \PackageHalcyon\Meta\Facades\Meta::class);
        // Migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'meta');
    }
}
