<?php

namespace PackageHalcyon\Meta\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Upload.
 */
class Meta extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'meta';
    }
}
