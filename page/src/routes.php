<?php
/**
 * All route names are prefixed with 'admin.page'.
 */
Route::group([
    'namespace'  => 'PackageHalcyon\Page\Controllers',
    'middleware' => 'web'
], function () {
    Route::group([
        'namespace'  => 'Backend',
        'middleware' => 'admin', 'as' => 'admin.', 'prefix' => 'admin/'
    ], function () {
        Route::post('page/table', 'PageTableController')->name('page.table');
        Route::get('page/activities', 'PageActivityController')->name('page.activity');
    
        Route::get('page/inactive', 'PageStatusController@inactive')->name('page.inactive');
        Route::patch('page/mark/{page}', 'PageStatusController@mark')->name('page.mark');

        Route::resource('page', 'PagesController');
    });


    Route::group([
        'namespace'  => 'Frontend', 'as' => 'frontend.'
    ], function () {
        // Route::get('page/{page}', 'PagesController@show')->where('page', '.+')->name('page.show');
    });
});
