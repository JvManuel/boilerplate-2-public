<?php

namespace PackageHalcyon\Page\Controllers\Backend;

use PackageHalcyon\Base\Controllers\StatusController as Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Page\Repositories\PageRepository as Repository;

use PackageHalcyon\Page\Models\Page as Model;

/**
 * Class PageStatusController.
 */
class PageStatusController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
        $this->route    = 'admin.page';
        $this->view    	= 'page::backend';
        $this->middleware('permission:page inactive', ['only' => ['inactive']]);
        $this->middleware('permission:page change status', ['only' => ['mark']]);
    }
}
