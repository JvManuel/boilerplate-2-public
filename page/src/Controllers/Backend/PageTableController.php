<?php

namespace PackageHalcyon\Page\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Page\Repositories\PageRepository as Repository;

use PackageHalcyon\Page\Models\Page as Model;
use DataTables;

/**
 * Class PageTableController.
 */
class PageTableController extends Controller
{
    /**
     * @param BlockRepository $repo
     */
    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
        $this->middleware('permission:page list,page activity,page delete', ['only' => ['__invoke']]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $user = auth()->user();
        return DataTables::of($this->repo->table($request))
            ->escapeColumns(['id'])
            ->editColumn('status', function ($model) use ($user) {
                return [
                    'type' => $model->status == "active" ? 'success' : 'danger',
                    'label' => ucfirst($model->status),
                    'value' => $model->status,
                    'link' => route('admin.page.mark', $model),
                    'can' => $user->can('page change status')
                ];
            })
            ->editColumn('updated_at', function ($model) {
                return $model->updated_at->format('d M, Y h:m A');
            })
            ->addColumn('actions', function ($model) {
                return $model->actions();
            })
            ->make(true);
    }
}
