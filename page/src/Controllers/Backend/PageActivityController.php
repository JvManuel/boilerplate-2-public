<?php

namespace PackageHalcyon\Page\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class PageActivityController.
 */
class PageActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:page activity', ['only' => ['__invoke']]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return view('page::backend.activity');
    }
}
