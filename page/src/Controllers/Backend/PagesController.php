<?php

namespace PackageHalcyon\Page\Controllers\Backend;

use PackageHalcyon\Base\Controllers\CRUDController as Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Page\Repositories\PageRepository as Repository;

use PackageHalcyon\Page\Models\Page as Model;

/**
 * Class PagesController.
 */
class PagesController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
        $this->config   = (object)[
            'view' => 'page::backend',
            'route' => 'admin.page',
            'hasSoftDelete' => false,
            'isSlugged' => true
        ];

        $this->middleware('permission:page list', ['only' => ['index']]);
        $this->middleware('permission:page show', ['only' => ['show']]);
        $this->middleware('permission:page create', ['only' => ['create', 'store']]);
        $this->middleware('permission:page update', ['only' => ['update', 'edit']]);
        $this->middleware('permission:page delete', ['only' => ['delete', 'destroy']]);
    }

    public function storeRules()
    {
        return [
            'title' => 'required|max:255|unique:pages',
            'description' => 'nullable|max:255',
            'content' => 'required',
            'image' => 'nullable|file|mimes:jpg,png,pdf,jpeg'
        ];
    }

    public function updateRules($model)
    {
        return [
            'title' => 'required|max:255|unique:pages,id,'. $model->id,
            'description' => 'nullable|max:255',
            'content' => (!$model->type ? 'required': 'nullable'),
            'image' => 'nullable|file|mimes:jpg,png,pdf,jpeg'
        ];
    }
}
