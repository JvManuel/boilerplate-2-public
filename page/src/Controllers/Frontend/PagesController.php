<?php

namespace PackageHalcyon\Page\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Page\Repositories\PageRepository as Repository;
use PackageHalcyon\Page\Models\Page as Model;

/**
 * Class PagesController.
 */
class PagesController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->model->whereSlug($slug)->where('status', 'active');
        $model = $model->firstOrFail();
        return view('page::frontend.' . ($model->template ?? 'default'), compact('model'));
    }
}
