<?php 

namespace PackageHalcyon\Page\Models;

use PackageHalcyon\Base\Traits\Baseable;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use PackageHalcyon\Meta\Traits\Metable;
use PackageHalcyon\History\Traits\Historable;
use PackageHalcyon\Menu\Traits\Menuable;

class Page extends Model
{
    use Sluggable, SluggableScopeHelpers, Metable, Historable, Baseable, Menuable;

    protected $fillable = [
        'title', 'slug', 'content', 'description', 'status', 'image', 'template', 'type', 'url'
    ];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Return the baseable title for this model.
     *
     * @return array
     */
    public function baseable()
    {
        return [
            'source' => 'title'
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function actions($frontend = false)
    {
        $user = auth()->user();
        $actions = [];

        // Frontend Links
        if ($frontend == true) {
            if ($this->type) {
                $actions['show'] = ['type' => 'show', 'link' => route($this->url)];
                return $actions;
            }
            $actions['show'] = ['type' => 'show', 'link' => route('frontend.page.show', $this)];
            return $actions;
        }


        // Backend Links
        if ($user->can('page show')) {
            $actions['show'] = ['type' => 'show',      'link' => route('admin.page.show', $this)];
        }
        if ($user->can('page update')) {
            $actions['edit'] = ['type' => 'edit',      'link' => route('admin.page.edit', $this)];
        }
        if ($user->can('page delete') && $this->type == null) {
            $actions['purge'] = ['type' => 'purge',     'link' => route('admin.page.destroy', $this), 'redirect' => route('admin.page.index')];
        }

        return $actions;
    }
}
