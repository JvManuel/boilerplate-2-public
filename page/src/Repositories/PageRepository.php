<?php

namespace PackageHalcyon\Page\Repositories;

use PackageHalcyon\Page\Models\Page as Model;
use Illuminate\Support\Facades\DB;
use Uploader;
use Meta;
use History;
use Menu;
use PackageHalcyon\Base\BaseRepository;
use \Cviebrock\EloquentSluggable\Services\SlugService;

/**
 * Class PageRepository.
 */
class PageRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Model::class;

    /**
     * @var Slider Model
     */
    protected $model;
    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model  = $model;
        $this->imageConfig = config('package.page.image.config');
    }

    public function table($request)
    {
        $query = $this->model->query()
            ->select([ 'id', 'title', 'status', 'slug', 'image', 'updated_at', 'type' ]);
        $query->where('status', $request->status);
        return $query;
    }

    /**
     * @param Request $request
     * @param Model  $model
     *
     * @return static
     */
    public function store($request, $model = null)
    {
        $data = $this->generateStub($request, $model);
        DB::beginTransaction();
        try {
            if ($model) {
                $model->update($data);
                History::updated($model, $model);
            } else {
                $model = $this->model->create($data);
                History::created($model);
            }
            $this->_handleImages($model, $request);
            Meta::store($request, $model);
            Menu::store($request, $model);
            DB::commit();
            return $model;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Create stub in storing the Model
     * @param Request $request
     * @param Model $model
     * @return array $data
     */
    public function generateStub($request, $model=null)
    {
        $data = $request->except(['_token', '_method', 'status', 'image']);
        $data['status'] = $request->status ? 'active' : 'inactive';
        $data['slug'] = SlugService::createSlug(Model::class, 'slug', $data['title'], ['unique' => $model]);
        return $data;
    }

    /**
    * Calls event before deleting
    * @param Model $model
    */
    protected function _deleting($model)
    {
        Uploader::destroy($model->image);
    }
}
