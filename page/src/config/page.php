<?php

return [
    'templates' => [
        'default' => 'Default', 'custom' => 'Custom'
    ],
    'image' => [

        'config' => [
            'path' => 'pages/',
            'width' => 300,
            'height' => 300,
            'name' => 'page_img'
        ]
    ]
];
