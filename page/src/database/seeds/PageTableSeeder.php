<?php

use Illuminate\Database\Seeder;
use PackageHalcyon\Page\Models\Page;

/**
 * Class UserTableSeeder.
 */
class PageTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
    
        $this->enableForeignKeys();
    }
}
