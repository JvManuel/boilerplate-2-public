<?php

use PackageHalcyon\Page\Models\Page;
use Illuminate\Database\Seeder;
use App\Models\Auth\User;
use App\Models\Auth\Role;
use App\Models\Auth\Permission;

/**
 * Class PagePermissionTableSeeder.
 */
class PagePermissionTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $page = Role::create(['name' => 'page manager']);
        
        // Page
        Permission::create(['name' => 'page list']);
        Permission::create(['name' => 'page inactive']);
        Permission::create(['name' => 'page activity']);

        Permission::create(['name' => 'page create']);
        Permission::create(['name' => 'page update']);
        Permission::create(['name' => 'page delete']);
        Permission::create(['name' => 'page show']);
        
        Permission::create(['name' => 'page change status']);

        $page->givePermissionTo([
            'page list', 'page inactive', 'page activity',
            'page create', 'page update', 'page delete', 'page show',
            'page change status'
        ]);
        $this->enableForeignKeys();
    }
}
