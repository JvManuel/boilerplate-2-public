<?php

Breadcrumbs::register('admin.page.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Page', route('admin.page.index'));
});

Breadcrumbs::register('admin.page.activity', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.page.index');
    $breadcrumbs->push('Activities', route('admin.page.activity'));
});

Breadcrumbs::register('admin.page.inactive', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.page.index');
    $breadcrumbs->push('Inactive List', route('admin.page.inactive'));
});

Breadcrumbs::register('admin.page.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.page.index');
    $breadcrumbs->push('Create', route('admin.page.create'));
});

Breadcrumbs::register('admin.page.show', function ($breadcrumbs, $model) {
    $breadcrumbs->parent('admin.page.index');
    $breadcrumbs->push('Show', route('admin.page.show', $model));
});

Breadcrumbs::register('admin.page.edit', function ($breadcrumbs, $model) {
    $breadcrumbs->parent('admin.page.show', $model);
    $breadcrumbs->push('Edit', route('admin.page.edit', $model));
});
