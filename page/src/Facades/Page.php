<?php

namespace PackageHalcyon\Page\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Page.
 */
class Page extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'page';
    }
}
