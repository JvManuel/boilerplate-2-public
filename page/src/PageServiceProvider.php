<?php
namespace PackageHalcyon\Page;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class PageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([__DIR__ . '/config/page.php' => config_path('package/page.php')]);
        $this->publishes([__DIR__ . '/database/seeds/PagePermissionTableSeeder.php' => database_path('seeds/Auth/ModulePermission/PagePermissionTableSeeder.php')]);
        $this->publishes([__DIR__ . '/database/seeds/PageTableSeeder.php' => database_path('seeds/Module/PageTableSeeder.php')]);
        $this->publishes([ __DIR__. '/resources/views/frontend' => resource_path('views/vendor/page/frontend')]);

        require 'breadcrumbs.php';
    }
    

    public function register()
    {
        $this->app->bind('page', function ($app) {
            return new Page($app);
        });
        $this->app->alias('page', \PackageHalcyon\Page\Facades\Page::class);
        
        // register our controller
        $this->app->make('PackageHalcyon\Page\Controllers\Backend\PagesController');
        $this->app->make('PackageHalcyon\Page\Controllers\Backend\PageStatusController');
        $this->app->make('PackageHalcyon\Page\Controllers\Backend\PageTableController');
        $this->app->make('PackageHalcyon\Page\Controllers\Frontend\PagesController');
        

        // // Migration
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        
        //       // Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'page');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }
}
