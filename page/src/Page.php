<?php

namespace PackageHalcyon\Page;

use PackageHalcyon\Page\Models\Page as Model;
use Illuminate\Support\Facades\DB;
use PackageHalcyon\History\Facades\History;
use PackageHalcyon\Meta\Facades\Meta;

/**
 * Class Page.
 */
class Page
{
    public function get($type)
    {
        return Model::where('type', $type)->firstOrFail();
    }


    public function create($data, $user)
    {
        $page = Model::create($data);
        $meta = ['meta' => ['user_id' => 1]];
        Meta::store($meta, $page);
        History::created($page, $user);
    }
}
