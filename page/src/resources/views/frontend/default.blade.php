@extends('frontend.layouts.app')
@include('meta::meta')
@section('content')
	<div class="content-container">
		<h2>{{ $model->name }}</h2>
		<hr/>
		<div class="row @can('page update') qlink @endcan">
   			@can('page update')
       			<a href="{{ $model->action($frontend = false, 'edit') }}" class="btn btn-sm btn-info qlink-button">
       				<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
       			</a>
	        @endcan
			<div class="col-sm-4 text-center">
				<a href="{{ $model->image }}" class="card-img-top fancy-box text-center">
					<img class="card-img-top img-preview" src="{{ $model->image }}" alt="{{ $model->name }}">
				</a>
			</div>	
			<div class="col-sm-8">
				<div class="form-group">
					<label>Description :</label> 
					<p>{{ $model->description }}</p>
				</div>
				<div class="form-group">
					<p>{!! $model->content !!}</p>
				</div>
			</div>		
			<hr>
			<div class="author card-body clearfix">
				<small class="text-muted"><b>Updated By : </b> {{ $model->metable->user->name }}</small> 
				<small class="text-muted pull-right"><b>Updated At : </b> {{ $model->updated_at->diffForHumans() }}</small> 
			</div>
		</div>
	</div>
@endsection
@include('base::partials.fancybox')
