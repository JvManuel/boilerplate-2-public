@extends('backend.layouts.app')

@section ('title', 'Page Management | Edit Page')

@section('breadcrumb-links')
    @include('page::backend.includes.breadcrumb-links')
@endsection

@section('content')
    {!! Form::model($model, ['url' => route('admin.page.update', $model), 'file' => 'mutlipart/enctype', 'files' => true, 'id' => 'content-form', 'class' => 'form-horizontal', 'method' => 'PATCH' ]) !!}
        <div class="card card-form">
           
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Page Management
                            <small class="text-muted">Edit Page</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                @include('page::backend.includes.fields')
                <hr/> @include('meta::form')
                <hr/> @include('menu::backend.partials.widget')
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <a href="{{ $model->action($frontend = false, 'show') }}" class="btn btn-danger btn-sm">Cancel</a>
                    </div><!--col-->

                    <div class="col text-right">
                        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-sm btn-submit']) }}
                    </div><!--row-->
                </div><!--row-->
            </div><!--card-footer-->
        </div>
    {!! Form::close() !!}
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { formSubmit($('#content-form')) })
    </script>
@endpush

