@extends('backend.layouts.app')

@section ('title', 'Page Management | Inactive Page')

@section('breadcrumb-links')
    @include('page::backend.includes.breadcrumb-links')
@endsection
@include('base::partials.datatable')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card  card-table">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title mb-0">
                                Page Management <small class="text-muted">Inactive Page</small>
                            </h4>
                        </div><!--col-->
                        <div class="col-sm-7">
                            <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                <a name="btn_refresh" class="btn btn-primary ml-1" data-toggle="tooltip" title="Refresh List">
                                    <i class="fa fa-refresh"></i>
                                </a>
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                    <hr/>
                    <div class="row">
                        <div class="table-responsive">
                            <table id="content-table" class="table table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Last Modified</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Last Modified</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            var table = $('#content-table').DataTable({
                processing: false,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.page.table") }}',
                    type: 'post',
                    data: { status: 'inactive' },
                    beforeSend: function(){
                        $('.card.card-table').prepend(overlay);
                    },
                    dataSrc: function(response){
                        setTimeout(function () { $('.card.card-table').find('.overlay').remove(); }, 500);
                        return response.data;
                    }
                },
                scrollY: '45vh',
                scrollCollapse: false,
                columns: [
                    {data: 'title',      },
                    {data: 'slug',      },
                    {
                        data: 'status',  
                        render: function(data){
                            return data.can == true ? renderChangeStatus(data) : renderLabel(data); 
                        },
                    },
                    {data: 'updated_at',},
                    {
                        data: 'actions', 
                        render: function(data){ return renderActions(data); },
                        searchable: false, 
                        sortable: false
                    }
                ],
                order: [[2, "desc"]],
                searchDelay: 500,
                fnInitComplete:function(){
                    $('.card.card-table').find('.overlay').remove();
                    $('[name=btn_refresh]').click(function(){ table.ajax.reload(); });
                    dataTableActions(table);
                },
            });
        });
    </script>
@endpush
