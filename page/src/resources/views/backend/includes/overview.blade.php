<div class="col">
    <div class="table-responsive">
        <table class="table">
            @if($model->image)
                <tr>
                    <th>Image</th>
                    <td><a class="fancy-box" href="{{ $model->image }}"><img src="{{ $model->image }}" class="img-preview rounded"/></a></td>
                </tr>
            @endif
            <tr>
                <th>Title</th>
                <td>{{ $model->title }}</td>
            </tr>
            <tr>
                <th>Slug</th>
                <td>{{ $model->slug }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td class="pre-line">{{ $model->description }}</td>
            </tr>
            <tr>
                <th>Content</th>
                <td>{!! $model->content !!}</td>
            </tr>

            <tr>
                <th>Status</th>
                <td>{!! $model->status == 'active' ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>' !!}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->