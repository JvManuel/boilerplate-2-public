<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if (Active::checkUriPattern('admin/page'))
                    Active Page
                @elseif (Active::checkUriPattern('admin/page/create'))
                    Create Page
                @elseif (Active::checkUriPattern('admin/page/inactive'))
                    Inactive List
                @elseif (Active::checkUriPattern('admin/page/activities'))
                    Activities
                @else 
                    Page
                @endif
            </a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                @can('page list') <a class="dropdown-item" href="{{ route('admin.page.index') }}">Active Page</a> @endcan
                @can('page create') <a class="dropdown-item" href="{{ route('admin.page.create') }}">Create Page</a> @endcan
                @can('page inactive') <a class="dropdown-item" href="{{ route('admin.page.inactive') }}">Inactive List</a> @endcan
                @can('page activity') <a class="dropdown-item" href="{{ route('admin.page.activity') }}">Activities</a> @endcan
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>