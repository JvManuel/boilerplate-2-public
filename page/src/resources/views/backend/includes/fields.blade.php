<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            <label class="col-md-2 form-control-label">Title</label>
            <div class="col-md-10">
                {!! Form::text('title', old('title'), ['class' => 'form-control ' . ($errors->has('title') ? 'is-invalid' : '' ), 
                    'placeholder' => 'Enter title', 'autofocus' => true]) !!}
            </div>
        </div>
        @if(! isset($model) || (isset($model) && ! $model->type))
            <div class="form-group row">
                <label class="col-md-2 form-control-label">Template</label>
                <div class="col-md-10">
                    {!! Form::select('template', config('package.page.templates'), old('template'), ['class' => 'form-control ' . ($errors->has('template') ? 'is-invalid' : '' ), 
                        'placeholder' => 'Enter template', 'autofocus' => true]) !!}
                </div>
            </div>
        @endif

        <div class="form-group row">
            <label class="col-md-2 form-control-label">Description</label>
            <div class="col-md-10">
                {!! Form::textarea('description', old('description'), ['class' => 'form-control ' . ($errors->has('description') ? 'is-invalid' : '' ), 
                    'placeholder' => 'Enter description', 'rows' => 3]) !!}
            </div>
        </div>
        @if(! isset($model) || (isset($model) && ! $model->type))
        <div class="form-group row">
            <label class="col-md-2 form-control-label">Content</label>
            <div class="col-md-10">
                {!! Form::textarea('content', old('content'), ['id' => 'ckeditor', 'class' => 'form-control' . ($errors->has('content') ? 'is-invalid' : '' ), 
                    'placeholder' => 'Enter content', 'rows' => 3]) !!}
            </div>
        </div>
        @endif
        @include('base::form.image', ['required' => false])
        <div class="form-group row">
            <label class="col-md-2 form-control-label">Status<br/><small>This content will not be visible on the system if turned off</small></label>
            <div class="col-md-10">
                <label class="switch switch-3d switch-primary">
                    {!! Form::checkbox('status', 1, (isset($model) && $model->status == 'active' || ! isset($model)), ['class' => 'switch-input']) !!}
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->
    </div>
</div>
@push('after-scripts')
    @include('includes.plugins.ckeditor')
@endpush