@extends('backend.layouts.app')

@section ('title', 'Page Management | Show Page')

@section('breadcrumb-links')
    @include('page::backend.includes.breadcrumb-links')
@endsection
@include('base::partials.fancybox')


@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Page Management
                    <small class="text-muted">Show Page</small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
                <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                    @can('content update')
                        <a href="{{ $model->action($frontend = false, 'edit') }}" class="btn btn-info ml-1" data-toggle="tooltip" title="Edit Page"><i class="fa fa-pencil"></i></a>
                    @endcan
                    @if(! $model->type)
                        @can('content delete')
                            <a href="{{ $model->action($frontend = false, 'delete') }}" data-redirect="{{ route('admin.page.index') }}" class="btn btn-danger ml-1" id="btn_purge" data-toggle="tooltip" title="Delete Page"><i class="fa fa-trash"></i></a>
                        @endcan
                    @endif
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ ! request()->query() ? 'active' : '' }}" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fa fa-user"></i> Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#meta" role="tab" aria-controls="meta" aria-expanded="true"><i class="fa fa-list"></i> Meta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->query('history') ? 'active' : '' }}" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-expanded="true"><i class="fa fa-list"></i> Activity</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane {{ ! request()->query() ? 'active' : '' }}" id="overview" role="tabpanel" aria-expanded="true">
                        @include('page::backend.includes.overview')
                    </div><!--tab-->
                    <div class="tab-pane" id="meta" role="tabpanel" aria-expanded="true">
                        @include('meta::preview')
                    </div>
                    <div class="tab-pane {{ request()->query('history') ? 'active' : '' }}" id="history" role="tabpanel" aria-expanded="true">
                        {!! History::renderEntity($model) !!}
                    </div>
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>Created At: </strong> {{ $model->updated_at->timezone(get_user_timezone()) }} ({{ $model->created_at->diffForHumans() }}),
                    <strong>Updated At:</strong> {{ $model->created_at->timezone(get_user_timezone()) }} ({{ $model->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { previewActions() })
    </script>
@endpush
@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/timeline.css') }}">
@endpush