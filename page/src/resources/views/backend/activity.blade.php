@extends('backend.layouts.app')

@section ('title', 'Page Management | Activities')

@section('breadcrumb-links')
    @include('page::backend.includes.breadcrumb-links')
@endsection
@section('content')
	<div class="row">
		<div class="col">
			<div class="card">
			    <div class="card-body">
                    {!! History::renderType(\PackageHalcyon\Page\Models\Page::class) !!}
			    </div>
			</div>
		</div>
	</div>
@endsection
@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/timeline.css') }}">
@endpush