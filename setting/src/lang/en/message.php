<?php
return [
    'group' => [
        'site' => [
            'name' => 'Site Information',
            'description' => "This section handles the site's information."
        ],
        'social' => [
            'name' => 'Social Information',
            'description' => "This section handles the site's social links such as the Facebook link etc.,"
        ],
    ]
];
