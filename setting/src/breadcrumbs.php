<?php
Breadcrumbs::register('admin.setting.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Setting', route('admin.setting.index'));
});


Breadcrumbs::register('admin.setting.show', function ($breadcrumbs, $group) {
    $breadcrumbs->parent('admin.setting.index');
    $breadcrumbs->push(__('setting::message.group.'. $group .'.name'), route('admin.setting.show', $group));
});
