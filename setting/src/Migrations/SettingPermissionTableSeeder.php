<?php

use PackageHalcyon\Setting\Models\Setting;
use Illuminate\Database\Seeder;
use App\Models\Auth\Role;
use App\Models\Auth\Permission;

/**
 * Class SettingPermissionTableSeeder.
 */
class SettingPermissionTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        
        Permission::create(['name' => 'setting']);

        $this->enableForeignKeys();
    }
}
