<?php

use PackageHalcyon\Setting\Models\Setting;
use Illuminate\Database\Seeder;

/**
 * Class SettingTableSeeder.
 */
class SettingTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        Setting::create(['key' => 'title', 'value' => 'Halcyon Boilerplate', 'group' => 'site', 'type' => 'text', 'order' => 1, 'options' => json_encode(['required' => 'true', 'max' => 100])]);
        Setting::create(['key' => 'description', 'value' => 'Halcyon Boilerplate', 'group' => 'site', 'type' => 'textarea', 'order' => 2, 'options' => json_encode(['required' => 'true'])]);
        Setting::create(['key' => 'logo', 'value' => null, 'group' => 'site', 'type' => 'image', 'order' => 3, 'options' => json_encode(['nullable' => 'true', 'file' => 'mimes:jpg,png,pdf,jpeg'])]);
        
        Setting::create(['key' => 'link-fb', 'value' => 'https://www.fb.com/', 'group' => 'social', 'type' => 'text', 'order' => 4, 'options' => json_encode(['required' => 'true'])]);
        Setting::create(['key' => 'link-instagram', 'value' => 'https://www.instagram.com/', 'group' => 'social', 'type' => 'text', 'order' => 5, 'options' => json_encode(['required' => 'true'])]);
        Setting::create(['key' => 'link-twiiter', 'value' => 'https://www.twiiter.com/', 'group' => 'social', 'type' => 'text', 'order' => 6, 'options' => json_encode(['required' => 'true'])]);

        $this->enableForeignKeys();
    }
}
