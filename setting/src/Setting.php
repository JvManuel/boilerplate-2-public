<?php

namespace PackageHalcyon\Setting;

use PackageHalcyon\Setting\Models\Setting as Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Setting.
 */
class Setting
{
    public $settings;

    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    public function group($group)
    {
        return $this->settings->where('group', $group);
    }

    public function key($group, $key)
    {
        return $this->settings->where('group', $group)->where('key', $key)->pluck('value')->first();
    }

    public function keys($group, $keys)
    {
        return $this->settings->where('group', $group)->whereIn('key', $keys)->pluck('key', 'value');
    }
}
