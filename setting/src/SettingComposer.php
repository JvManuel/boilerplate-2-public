<?php

namespace PackageHalcyon\Setting;

use Illuminate\View\View;
use PackageHalcyon\Setting\Models\Setting;

/**
 * Class SettingComposer.
 */
class SettingComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        // $view->with('site_settings', Setting::all()->groupBy('group'));
    }
}
