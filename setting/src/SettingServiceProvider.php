<?php

namespace PackageHalcyon\Setting;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use PackageHalcyon\Setting\Setting;
use PackageHalcyon\Setting\Models\Setting as Model;
use Illuminate\Support\Facades\Schema;

class SettingServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadTranslationsFrom(__DIR__ . '/lang', 'setting');
        $this->publishes([__DIR__ . '/config/setting.php' => config_path('package/setting.php')]);
        $this->publishes([__DIR__ . '/Migrations/SettingPermissionTableSeeder.php' => database_path('seeds/Auth/ModulePermission/SettingPermissionTableSeeder.php')]);
        $this->publishes([__DIR__ . '/Migrations/SettingTableSeeder.php' => database_path('seeds/Module/SettingTableSeeder.php')]);
        require 'breadcrumbs.php';

        /*
         * Global
         */
        // View::composer(
        // // This class binds the $logged_in_user variable to every view
        //     '*', SettingComposer::class
        // );
        View::composer(config('package.setting.views'), function ($view) {
            $settings = collect();
            $setting = app('setting');
            try {
                $settings =  Model::select(['id', 'key', 'value', 'group'])->get();
            } catch (\Exception $e) {
            }
            $setting->setSettings($settings);
            $site_settings = $setting;
            $view->with(compact('site_settings'));
        });
    }

    public function register()
    {
        //  Register Facades
        $this->app->bind('setting', function ($app) {
            return new Setting($app);
        });
        $this->app->alias('setting', \PackageHalcyon\Setting\Facades\Setting::class);
        $this->app->make('PackageHalcyon\Setting\Controllers\SettingsController');

        // Migration
        $this->loadMigrationsFrom(__DIR__ . '/Migrations/database');

        // Views
        $this->loadViewsFrom(__DIR__ . '/views', 'setting');

        $this->mergeConfigFrom(__DIR__ . '/config/setting.php', 'package.setting');
    }
}
