<?php
/**
 * All route names are prefixed with 'admin.content'.
 */
Route::group([
    'namespace'  => 'PackageHalcyon\Setting\Controllers',
    'middleware' => ['web', 'admin'], 'prefix' => 'admin', 'as' => 'admin.'
], function () {
    Route::get('setting', 'SettingsController@index')->name('setting.index');
    Route::get('setting/{group}', 'SettingsController@show')->name('setting.show');
    Route::patch('setting/{group}', 'SettingsController@update')->name('setting.update');
});
