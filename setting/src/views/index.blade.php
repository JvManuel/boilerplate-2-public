@extends('backend.layouts.app')

@section ('title', 'Settings')

@section('content')
    <div class="row">
        <div class="col">
        	<div class="row">
        		@foreach($groups as $g => $group)
		        	<div class="col-sm-6">
			            <div class="card">
			                <div class="card-header clearfix">
			                	{{ __('setting::message.group.' . $group . '.name') }}
			                	<a href="{{ route('admin.setting.show', $group) }}" class="btn btn-primary pull-right"><i class="fa fa-cog"></i> Configure</a>
			            	</div>
			                <div class="card-body">{{ __('setting::message.group.' . $group . '.description') }}</div>
			            </div>
		        	</div>
        		@endforeach
        	</div>
        </div>
    </div>
@endsection
