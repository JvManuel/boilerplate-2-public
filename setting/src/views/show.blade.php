@extends('backend.layouts.app')
@section ('title', 'Setting | ' . __('setting::message.group.' . $group .'.name'))


@section('content')
{!! Form::open(['method' => 'PATCH', 'url' => route('admin.setting.update', $group), 'file' => 'mutlipart/enctype', 'files' => true]) !!}
<div class="row">
	<div class="col relative">
		<div class="card card-form" style="min-height: 400px;">
		    <div class="card-body">
		        <div class="row">
		            <div class="col-sm-5">
		                <h4 class="card-title mb-0">
		                    Setting <small class="text-muted">{{ __('setting::message.group.' . $group .'.name') }}</small>
		                </h4>
		            </div>
		        </div>
		        <div class="row mt-4 mb-4">
		            @include('setting::includes.fields')
		        </div>
		    </div>
		    <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <a href="{{ route('admin.setting.index') }}" class="btn btn-danger btn-sm">Cancel</a>
                    </div>
                    <div class="col text-right">
                        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-sm btn-submit']) }}
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{!! Form::close() !!}
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('form').submit(function (e) {
                var formId = this.id,
                    form = this;
                event.preventDefault();
                $(this).find('.card-form').prepend(`
                   <div class="overlay">
                        <div class="loader-container text-center">
                            <div class="text-center"><i class="fa fa-spin fa-spinner fa-3x"></i></div>
                            <div class="text-center">Submitting request...</div>
                        </div>
                    </div>
                `)
                setTimeout( function () { form.submit(); }, 1000);
            })
        })
    </script>
@endpush