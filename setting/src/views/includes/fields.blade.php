@forelse($settings as $s => $setting)
    <div class="col-sm-6">
        <div class="form-group">
            <label class="text-capitalize">{{ $setting->key }}</label>
            @if($setting->type == 'textarea')
                {{ Form::textarea($setting->id.'[value]', $setting->value, ['class'=> 'form-control','rows' => 3]) }}
            @elseif($setting->type == 'ckeditor')
                {{ Form::textarea($setting->id.'[value]', $setting->value, ['class'=> 'form-control ckeditor','rows' => 3]) }}
            @elseif($setting->type == 'image')
                @php $setting->image = asset($setting->value); @endphp
                @include('base::form.image', [
                    'name' => $setting->id.'[value]', 
                    'image_remove_name' => $setting->id.'[image_remove]', 
                    'required' => $setting->rule('required')  , 
                    'model' => $setting, 
                    'no_label' => true
                ])
            @else
                {{ Form::text($setting->id.'[value]', $setting->value, ['class'=> 'form-control']) }}
            @endif
        </div>
    </div>
@empty
    <div class="col-sm-12 text-center form-group">
        <b class="h3 text-muted">There are no settings set. Please contact the developers if you want to add.</b>
    </div>
@endforelse

@push('after-scripts')
    @include('includes.plugins.ckeditor')
@endpush