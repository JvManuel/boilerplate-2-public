<?php

namespace PackageHalcyon\Setting\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Setting\Models\Setting as Model;
use PackageHalcyon\Setting\SettingRepository as Repository;

/**
 * Class SettingsController.
 */
class SettingsController extends Controller
{
    public function __construct(Model $model, Repository $repo)
    {
        $this->model = $model;
        $this->repo = $repo;
        $this->middleware('permission:setting', ['only' => ['index', 'show', 'update']]);
    }

    public function index()
    {
        $groups = config('package.setting.groups');
        return view('setting::index', compact('groups'));
    }

    public function show($group)
    {
        $settings = $this->model->where('group', $group)->orderBy('order')->get();
        return view('setting::show', compact('group', 'settings'));
    }

    public function update(Request $request, $group)
    {
        $this->validate($request, $this->repo->rules($group));
        $model = $this->repo->store($request, $group);
        return $this->response('update', $request, $group);
    }

    public function response($process, $request, $group)
    {
        $message    = 'You have successfully updated the ' . __('setting::message.group.'.$group.'.name') . ' settings.';
        return $request->ajax() ?
            response()->json(['message' => $message, 'link' => route('admin.setting.show', $group)]) :
            redirect(route('admin.setting.show', $group))
            ->withFlashSuccess($message);
    }
}
