<?php 

namespace PackageHalcyon\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Lloricode\LaravelImageable\Models\Traits\ImageableTrait;

class Setting extends Model
{
    use ImageableTrait;
    
    protected $fillable = [
        'user_id', 'group', 'type', 'key', 'value', 'options', 'order'
    ];

    public function getRulesAttribute()
    {
        return collect(json_decode($this->options));
    }

    public function user()
    {
        // return $this->belongsTo(config('history.auth.user.model'));
        return $this->belongsTo(\App\Models\Auth\User::class);
    }

    public function rule($key)
    {
        return array_key_exists($key, $this->rules) ? $this->rules[$key] : null;
    }
}
