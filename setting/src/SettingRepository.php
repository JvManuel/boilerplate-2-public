<?php

namespace PackageHalcyon\Setting;

use PackageHalcyon\Setting\Models\Setting as Model;
use Illuminate\Support\Facades\DB;
use Image;
use File;
use PackageHalcyon\Base\BaseRepository;

/**
 * Class SettingRepository.
 */
class SettingRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Model::class;

    /**
     * @var Slider Model
     */
    protected $model;
    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model  = $model;
        $this->imageConfig = [
            'path' => 'setting/',
            'width' => 150,
            'height' => 150,
            'name' => 'setting_img'
        ];
    }

    /**
     * @return string
     */
    public function model()
    {
        return Model::class;
    }

    /**
     * @param Request $request
     * @param Model  $model
     *
     * @return static
     */
    public function store($request, $group = null)
    {
        $data = $request->except(['_token', '_method', 'image_remove']);
        $ids = array_keys($data);
        DB::beginTransaction();
        try {
            $models = $this->model->whereIn('id', $ids)->get();
            foreach ($data as $s => $setting) {
                $model = $models->where('id', $s)->first();
                if ($model->type == 'image') {
                    $this->_handleImage($model, $setting);
                } else {
                    $model->update($setting);
                }
            }
            DB::commit();
            return $group;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    protected function _handleImage($model, $setting)
    {
        $data = (object) $setting;
        if (array_key_exists('image_remove', $setting) && array_key_exists('value', $setting)) {
            if ($model->value && $setting['image_remove'] == true) {
                if ($model->value) {
                    File::delete($model->value);
                }
                $model->update(['value' => null]);
            }
        }
        if (array_key_exists('value', $setting) && $data->value) {
            $file = $data->value;
            $filename = $model->id . '_' . time() . '.' . $file->getClientOriginalExtension();
            $path = 'images/setting/';
            if (! file_exists($path)) {
                File::makeDirectory(public_path($path), 0777, true, true);
            }
            $dimensions = getimagesize($file);
            $img = Image::make($file->getRealPath());
            $img->resize(150, 150, function ($c) {
                $c->aspectRatio();
            })->save($path.$filename);
            if ($model->value) {
                File::delete($model->value);
            }
            $model->update(['value' => $path.$filename]);
        }
    }

    public function rules($group)
    {
        $settings = Model::where('group', $group)->get();
        $rules = [];
        foreach ($settings as $s => $setting) {
            $rule = "";
            if (count(collect($setting->rules))) {
                foreach (collect($setting->rules) as $r => $value) {
                    if ($r == 'required') {
                        if ($value == 'true') {
                            $rule .= 'required|';
                        }
                    } elseif ($r == 'nullable') {
                        if ($value == 'true') {
                            $rule .= 'nullable|';
                        }
                    } else {
                        $rule .= $r .':'. $value . '|';
                    }
                }
                $rule =  rtrim($rule, '|');
            }
            if ($rule) {
                $rules[$setting->id . '.value'] = $rule;
            }
        }
        return $rules;
    }
}
