# Halcyon Boilerplate 2 Packages #

Core logic of Halcyon Boilerplate 2

### Changes Log ###
- v4.3.5: Fix make:taxonomy, Fix getting costume field name for sluggable.
- v4.3.4: Fix menu resource, add default config on page repository.
- v4.3.3: Apply dynamic table seeder options on make:module.
- v4.3.2: Fix on default fields. on make:module
- v4.3.1: Segregate function, then formatted code style
- v4.3.0: Added `Console` UI on `make:module`'s command, and add `composer format` to format code style (for developing).
- v4.2.10: Fix Menu on minifier.
- v4.2.9: Menu and Settings views bugs fixed.
- v4.2.8: Fixed missing semmi colon on js, and remove js comment that affect minifier. 
- v4.2.7: Fixed missing semmi colon on js that affect minifier. 