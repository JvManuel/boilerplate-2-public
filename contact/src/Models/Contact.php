<?php 

namespace PackageHalcyon\Contact\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

// use Cviebrock\EloquentSluggable\Sluggable;
// use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
// use PackageHalcyon\Meta\Traits\Metable;
use PackageHalcyon\History\Traits\Historable;
use PackageHalcyon\Permit\Traits\Permitable;

class Contact extends Model
{
    // use Sluggable, SluggableScopeHelpers, SoftDeletes, Metable, Historable, Permitable;
    use Historable, Permitable;

    protected $fillable = [
        'name', 'email', 'phone', 'subject', 'message',
    ];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    // public function sluggable()
    // {
    //     return [
    //         'slug' => [
    //             'source' => 'name'
    //         ]
    //     ];
    // }

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function actions()
    {
        $user = auth()->user();
        $actions = [];
        // if ($this->trashed()) {
        //     if ($user->can('contact restore')) {
        //         $actions[] = ['type' => 'restore',   'link' => route('admin.contact.restore' , $this), 'redirect' => route('admin.contact.index')];
        //     }
        //     if ($user->can('contact restore')) {
        //         $actions[] = ['type' => 'purge',     'link' => route('admin.contact.purge'   , $this), 'redirect' => route('admin.contact.index')];
        //     }

        //     return $actions;
        // }
        

        if ($user->can('contact show')) {
            $actions[] = ['type' => 'show',      'link' => route('admin.contact.show', $this)];
        }
        // if ($user->can('contact update')) {
        //     $actions[] = ['type' => 'edit',      'link' => route('admin.contact.edit'   , $this)];
        // }
        if ($user->can('contact delete')) {
            $actions[] = ['type' => 'delete',    'link' => route('admin.contact.destroy', $this) , 'redirect' => route('admin.contact.index')];
        }

        return $actions;
    }

    public static function boot()
    {
        parent::boot();
    }
}
