<?php
namespace PackageHalcyon\Contact;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadTranslationsFrom(__DIR__.'/lang', 'contact');
    }

    public function register()
    {

        // register our controller
        $this->app->make('PackageHalcyon\Contact\Controllers\Backend\ContactsController');
        $this->app->make('PackageHalcyon\Contact\Controllers\Backend\ContactTableController');
        $this->app->make('PackageHalcyon\Contact\Controllers\Frontend\ContactsController');
        

        // Migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        
        // Views
        $this->loadViewsFrom(__DIR__.'/views', 'contact');

        
        $this->publishes([
            __DIR__.'/views/frontend' => resource_path('views/vendor/contact/frontend'),
        ]);
    }
}
