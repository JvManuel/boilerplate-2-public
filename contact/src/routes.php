<?php
/**
 * All route names are prefixed with 'admin.content'.
 */
Route::group([
    'namespace'  => 'PackageHalcyon\Contact\Controllers',
], function () {
    Route::group([
    'namespace'  => 'Backend',
    'middleware' => ['web', 'admin'], 'as' => 'admin.', 'prefix' => 'admin/'
    ], function () {
        Route::post('contact/table', 'ContactTableController@index')->name('contact.table');
        Route::resource('contact', 'ContactsController', ['only' => ['show', 'index', 'destroy']]);
    });
    Route::group([
        'namespace'  => 'Frontend',
        'middleware' => ['web'], 'as' => 'frontend.'
    ], function () {
        Route::resource('contact', 'ContactsController', ['only' => ['store', 'index']]);
    });
});
