<?php

namespace PackageHalcyon\Contact\Repositories;

use PackageHalcyon\Contact\Models\Contact as Model;
use Illuminate\Support\Facades\DB;
use Uploader;
use Meta;
use PackageHalcyon\Base\BaseRepository;

/**
 * Class PageRepository.
 */
class ContactRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Model::class;

    /**
     * @var Slider Model
     */
    protected $model;
    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model  = $model;
    }

    /**
     * @return string
     */
    public function model()
    {
        return Model::class;
    }

    public function table($request)
    {
        $query = $this->model->query()
            ->select([ 'id', 'name', 'email', 'phone', 'subject', 'message', 'created_at' ]);
        return $query;
    }

    /**
     * @param Request $request
     * @param Model  $model
     *
     * @return static
     */
    public function store($request, $model = null)
    {
        $data = $this->generateStub($request, $model);
        
        DB::beginTransaction();
        try {
            $model = $this->model->create($data);
            // history()->log($model, ['action' => 'created', 'icon' => 'fa fa-plus', 'color' => 'bg-success']);
            DB::commit();
            return $model;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Create stub in storing Inquiries
     * @param Request $request
     * @param Model $model
     * @return array $data
     */
    public function generateStub($request, $model=null)
    {
        $data = $request->except(['_token', '_method']);
        return $data;
    }
}
