@component('mail::message')
Hi, {{ __('contact::strings.emails.contact_ack.email_body_title') }}

@component('mail::panel')
Name: {{ $request->name }} <br />
@endcomponent

@component('mail::panel')
Email: {{ $request->email }} <br />
@endcomponent

@component('mail::panel')
Contact No. : {{ $request->phone }} <br />
@endcomponent

@component('mail::panel')
Message: {{ $request->subject }} <br />
@endcomponent

@component('mail::panel')
Message: {{ $request->message }} <br />
@endcomponent

We will get back to you as soon as we can. 

Thank you, <br>
Support Team
@endcomponent