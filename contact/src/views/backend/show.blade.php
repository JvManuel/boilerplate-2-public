@extends('backend.layouts.app')

@section ('title', 'Contact Management | Show Page')

@section('breadcrumb-links')
    @include('contact::backend.includes.breadcrumb-links')
@endsection

@include('base::partials.fancybox')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Contact Management
                    <small class="text-muted">Show Contact</small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
                <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                    @can('contact delete')
                        <a href="{{ route('admin.contact.destroy', $model) }}" data-redirect="{{ route('admin.contact.index') }}" class="btn btn-danger ml-1" id="btn_delete" data-toggle="tooltip" title="Delete Content"><i class="fa fa-trash"></i></a>
                    @endcan
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fa fa-user"></i> Overview</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                       <div class="col">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Message ID</th>
                                        <td>{{ $model->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td>{{ $model->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $model->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td>{{ $model->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Subject</th>
                                        <td class="pre-line">{{ $model->subject}}</td>
                                    </tr>
                                    <tr>
                                        <th>Message</th>
                                        <td class="pre-line">{{ $model->message}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div><!--table-responsive-->
                    </div><!--tab-->
                    
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>Created At: </strong> {{ $model->updated_at->timezone(get_user_timezone()) }} ({{ $model->created_at->diffForHumans() }}),
                    <strong>Updated At:</strong> {{ $model->created_at->timezone(get_user_timezone()) }} ({{ $model->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { previewActions() })
    </script>
@endpush
@push('after-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/timeline.css') }}">
@endpush