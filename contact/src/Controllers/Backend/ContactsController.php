<?php

namespace PackageHalcyon\Contact\Controllers\Backend;

use PackageHalcyon\Base\Controllers\CRUDController as Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Contact\Repositories\ContactRepository as Repository;

use PackageHalcyon\Contact\Models\Contact as Model;

/**
 * Class ContactController.
 */
class ContactsController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
        $this->config   = (object)[
            'view' => 'contact::backend',
            'route' => 'admin.contact',
            'hasSoftDelete' => false
        ];

        $this->middleware('permission:contact list', ['only' => ['index']]);
        $this->middleware('permission:contact show', ['only' => ['show']]);
        $this->middleware('permission:contact create', ['only' => ['create', 'store']]);
        $this->middleware('permission:contact update', ['only' => ['update', 'edit']]);
        $this->middleware('permission:contact delete', ['only' => ['delete','destroy']]);
    }

    public function show($slug)
    {
        $model = $this->model->whereId($slug)->firstOrFail();
        return view($this->config->view . '.show', compact('model'));
    }

    public function destroy(Request $request, $slug)
    {
        $model = $this->model->where('id', $slug)->firstOrFail();
        $this->repo->destroy($model);
        return $this->response('delete', $request, $model);
    }
}
