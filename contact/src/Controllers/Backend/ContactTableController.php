<?php

namespace PackageHalcyon\Contact\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Contact\Repositories\ContactRepository as Repository;

use PackageHalcyon\Contact\Models\Contact as Model;
use DataTables;

/**
 * Class ContactTableController.
 */
class ContactTableController extends Controller
{
    /**
     * @var BlockRepository
     */
    protected $repo;

    /**
     * @param BlockRepository $repo
     */
    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
        $this->middleware('permission:contact list', ['only' => ['index']]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        return DataTables::of($this->repo->table($request))
            ->escapeColumns(['id'])
            ->editColumn('subject', function ($model) {
                return str_limit($model->subject, 50);
            })
            ->editColumn('message', function ($model) {
                return str_limit($model->message, 50);
            })
            ->editColumn('created_at', function ($model) {
                return $model->created_at->format('d M, Y h:m A');
            })
            ->addColumn('actions', function ($model) {
                return $model->actions();
            })
            ->make(true);
    }
}
