<?php

namespace PackageHalcyon\Contact\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageHalcyon\Contact\Repositories\ContactRepository as Repository;
use Illuminate\Support\Facades\Mail;
use PackageHalcyon\Contact\Mail\SendContact;
use PackageHalcyon\Contact\Mail\SendContactAck;

use PackageHalcyon\Contact\Models\Contact as Model;

/**
 * Class ContactController.
 */
class ContactsController extends Controller
{
    public function __construct(Repository $repo, Model $model)
    {
        $this->repo     = $repo;
        $this->model    = $model;
    }

    public function index()
    {
        return view('contact::frontend.index');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->storeRules(), [
            'g-recaptcha-response.required' => 'The reCAPTCHA field is required.',
        ]);
        $model = $this->repo->store($request);

        Mail::send(new SendContact($request));
        Mail::send(new SendContactAck($request));

        return redirect()->back()->withFlashSuccess(__('alerts.frontend.contact.sent'));
    }

    public function storeRules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'subject' => 'max:255',
            'message' => 'required|max:1000',
            'g-recaptcha-response'=>'required|captcha',
        ];
    }
}
