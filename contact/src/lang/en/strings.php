<?php

return [
    'emails' => [
        'contact_ack' => [
            'email_body_title' => 'Below are the details of your contact form submission:',
            'subject' => 'Hi, We got your message. Thanks!',
        ],
    ],
];
