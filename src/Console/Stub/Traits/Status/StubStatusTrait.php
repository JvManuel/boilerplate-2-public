<?php

namespace PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Status;

trait StubStatusTrait
{


    /**
     *
     * @param string $path
     * @return array
     */
    private function _readModularFile()
    {
        if (!file_exists($this->stubStatusFileName)) {
            $this->files->put($this->stubStatusFileName, json_encode([]));
        }

        $data = [];
        try {
            $data = json_decode($this->files->get($this->stubStatusFileName));
        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
        }
        return $data;
    }

    public function getDataModularFileDatas($moduleName = null)
    {
        $datas = $this->_readModularFile();

        if (is_null($moduleName)) {
            return $datas;
        } elseif (isset($datas->$moduleName)) {
            return $datas->$moduleName;
        }
        return null;
    }

    private function _writeFile($datas)
    {
        $json = json_encode($datas, JSON_PRETTY_PRINT);
        $this->files->put($this->stubStatusFileName, $json);
    }

    public function generatingRevertFile($options)
    {
        $oldData = $this->getDataModularFileDatas();

        $newData = [$this->getNameInput() => [
                'status' => 'active',
                'created_at' => now()->format('Y-m-d H:i:s'),
                'updated_at' => now()->format('Y-m-d H:i:s'),
                'deleted_at' => null,
                'types' => $options ?: [],
                'datas' => $this->_modulesFiles]
        ];

        $this->_writeFile(array_merge((array) $oldData, $newData));
    }

    public function udpateDeleteData($moduleName)
    {
        $oldData = $this->getDataModularFileDatas($moduleName);

        $updatedData = [$this->getNameInput() => [
                'status' => 'inactive',
                'created_at' => $oldData->created_at,
                'updated_at' => $oldData->updated_at,
                'deleted_at' => now()->format('Y-m-d H:i:s'),
                'types' => $oldData->types,
                'datas' => $oldData->datas]
        ];

        $this->_writeFile(array_merge((array) $this->getDataModularFileDatas(), $updatedData));
    }

    public function getCamelCaseModuleInput()
    {
        return camel_case($this->getNameInput());
    }

    public function getCurrentProjectDir()
    {
        return str_replace('storage', '', storage_path());
    }

    public function removeProjectDir($path)
    {
        return str_replace($this->getCurrentProjectDir(), '', $path);
    }

    public function addProjectDir($path)
    {
        return $this->getCurrentProjectDir() . $path;
    }
}
