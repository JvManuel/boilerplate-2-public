<?php

namespace PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Maker;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubFilterGEneratorInterface;
use Artisan;

trait StubGeneratorTraits
{
    private $_filterStub;
    private $_optionsGenerator;

    public function excecute($options = null)
    {
        $this->_optionsGenerator = $options;
        $args = [
                        'getCamelCaseModuleInput' => $this->getCamelCaseModuleInput(),
                        'getNameInput' => $this->getNameInput(),
                ];

        foreach ($this->classes as  $class) {//=> $extraArgs) {
            $className = "$class";
            $this->line("<fg=yellow>Excecuting $className ...</>");

            // $class = (new $class($args));
            $class = new $class;
            $class->setArgs($args, (array)$this->_optionsGenerator);

            $structure = $class->structures();
            if (is_null($structure)) {
                continue;
            }
            if (isset($structure[0])) {
                foreach ($structure as $strctr) {
                    $generate  = $strctr['generate'];
                    $create    = $strctr['create'];
                    $this->_generate($generate[0], $generate[1], $generate[2], $generate[3], $generate[4])
                                        ->_createFile($create[0], $create[1]);
                }
            } else {
                $generate  = $structure['generate'];
                $create    = $structure['create'];
                $this->_generate($generate[0], $generate[1], $generate[2], $generate[3], $generate[4])
                                ->_createFile($create[0], $create[1]);
            }
        
            $this->info("Done Excecuting $className ...");
        }

        $this->line("\n" . '<fg=yellow>Generating "' . $this->getNameInput() . '" ' . $this->type . ' revert files ...</>');
        $this->generatingRevertFile($this->_optionsGenerator);
        $this->line('<fg=green>Done Generating "' . $this->getNameInput() . '" ' . $this->type . ' revert files ...</>');

        $this->line("\n" . '<bg=green>' . $this->type . ' created successfully.</>');

        $this->line("\n" . '<fg=yellow>Executing:</> composer clear-all ...');
        shell_exec('composer clear-all');

        Artisan::call('inspire');
        $this->info("\n" . Artisan::output());
    }

    public function setFilterStub(StubFilterGEneratorInterface $filterStub)
    {
        $this->_filterStub = $filterStub;
    }

    private function _generate($path, $stub, $filename = null, $isGetPath = false, $ignoreLastFolder = false)
    {
        $qualifyClass = method_exists($this, 'qualifyClass') ? 'qualifyClass' : 'parseName';

        if (!$ignoreLastFolder) {
            $filename = ucfirst($this->getCamelCaseModuleInput()) . $filename;
        }

        $name = str_replace('App\\', '', $this->$qualifyClass($path . $filename));
        if ($this->files->exists($path = $this->_fileChecker($name, $isGetPath))) {
            $this->error($this->type . ' already exists!');
            exit();
        }

        $this->currentStub = $this->dir . $stub;
        $this->makeDirectory($path);
        $this->_path       = $path;
        $this->_name       = $name;

        return $this;
    }

    private function _createFile($removeApp = false, $customeFunction = null)
    {
        $path = $this->_path;
        $this->line('<fg=yellow>Generating:</> ' . $path);

        if ($removeApp) {
            $path = str_replace('app/', '', $path);
        }

        $filteredBySoftDelete = $this->_filterStub::generate($this->buildClass($this->_name), $this->_optionsGenerator);

        if (!is_null($customeFunction)) {
            $filteredBySoftDelete = $customeFunction($filteredBySoftDelete);
        }

        $this->files->put($path, $filteredBySoftDelete);

        $this->line('<fg=green>Generated:</>  ' . $path);
                
        $this->_modulesFiles[] = $this->removeProjectDir($path);
    }

    private function _fileChecker($name, $isGetPath)
    {
        $path = null;
        if ($isGetPath) {
            $path = $this->getPath($name);
        } else {
            $path = $this->laravel['path.base'] . '/' . str_replace('\\', '/', $name) . '.php';
        }

        return $path;
    }
        
    /**
     * Replace the name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    public function replaceName(&$stub, $name)
    {
        // lloric code
                // Small case
                $stub = str_replace('dummy titles', str_replace('-', ' ', str_slug(str_plural($name))), $stub); // llorics | lloric codes
                $stub = str_replace('dummy title', str_replace('-', ' ', str_slug($name)), $stub); // lloric | lloric code

                $stub = str_replace('dummytitles', str_replace('-', '', str_slug(str_plural($name))), $stub); // llorics | lloriccodes
                $stub = str_replace('dummytitle', str_replace('-', '', str_slug($name)), $stub); // lloric | lloriccode

                $stub = str_replace('dummy_titles', snake_case(str_plural($name)), $stub); // llorics | lloric_codes
                $stub = str_replace('dummy_title', snake_case($name), $stub); // lloric | lloric_code

                $stub = str_replace('dummy-titles', str_slug(str_plural($name)), $stub); // llorics | lloric-codes
                $stub = str_replace('dummy-title', str_slug($name), $stub); // lloric | lloric-code

                $stub = str_replace('dummy.titles', str_replace('-', '.', str_slug(str_plural($name))), $stub); // llorics | lloric.codes
                $stub = str_replace('dummy.title', str_replace('-', '.', str_slug($name)), $stub); // lloric | lloric.code

                $stub = str_replace('dummyTitles', camel_case(str_plural($name)), $stub); // llorics | lloricCodes
                $stub = str_replace('dummyTitle', camel_case($name), $stub); // lloric | lloricCode
                // Big Cases
                $stub = str_replace('Dummy Titles', ucwords(str_plural($name)), $stub); // Llorics | Lloric Codes
                $stub = str_replace('Dummy Title', ucwords(str_replace('-', ' ', str_slug($name))), $stub); // Lloric | Lloric Code

                $stub = str_replace('DummyTitles', ucfirst(studly_case(str_plural($name))), $stub); // Llorics | LloricCodes
                $stub = str_replace('DummyTitle', ucfirst(studly_case($name)), $stub); // Lloric | LloricCode
                return $this;
    }


    /**
     *  ------------------------------------------
     */
        


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Get the full namespace name for a given class.
     *
     * @param  string  $name
     * @return string
     */
    protected function getNamespace($name)
    {
        $name = str_replace('\\routes\\', '\\', $name);
        return trim(implode('\\', array_map('ucfirst', array_slice(explode('\\', studly_case($name)), 0, -1))), '\\');
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());
        return $this->replaceName($stub, $this->getNameInput())->replaceNamespace($stub, $name)->replaceClass($stub, $name);
    }



    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = class_basename($name);
        return str_replace('DummyClass', $class, $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->currentStub;
    }
}
