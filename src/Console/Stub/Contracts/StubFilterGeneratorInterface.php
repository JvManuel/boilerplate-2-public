<?php

namespace PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts;

interface StubFilterGeneratorInterface
{
    public static function generate($value, $options) :string;
}
