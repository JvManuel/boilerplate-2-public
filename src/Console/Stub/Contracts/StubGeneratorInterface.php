<?php

namespace PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts;

interface StubGeneratorInterface
{
    public function structures();
    public function setArgs(array $args, array $options);
    public function __toString() :string;
}
