<?php

namespace PackageHalcyon\BoilerPlatePackages2\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class HalcyonCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'halcyon {--ver : See Halcyon BoilerPlate 2 Packages current version.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Halcyon BoilerPlate 2 Packages Information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('ver')) {
            $this->line('Halcyon BoilerPlate 2 Packages <fg=green>' . str_replace('v', '', versionNameHalcyonBoilerPlatePackages()) . '</>');
        }
    }
}
