<?php

namespace PackageHalcyon\BoilerPlatePackages2\Console\Traits;

trait ConsoleUITrait
{
    protected function muneOption($question, $options, $yesOrNo = false)
    {
        $selected = $this->menu($question, $options)
        ->setForegroundColour('green')
        ->setBackgroundColour('black')
        ->setWidth(200)
        ->setPadding(10)
        ->setMargin(5)
        ->setExitButtonText("Abort") // remove exit button with ->disableDefaultItems()
        ->setUnselectedMarker('*')
        ->setSelectedMarker('->')
        // ->setTitleSeparator('*-')
        // ->addLineBreak('#', 1)
        // ->addStaticItem('AREA 2')
        ->open();

        if ($yesOrNo) {
            return (bool) $selected == 0?true:false;
        }

        return $selected;
    }
}
