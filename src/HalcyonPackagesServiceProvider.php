<?php

namespace PackageHalcyon\BoilerPlatePackages2;

use Illuminate\Support\ServiceProvider;

class HalcyonPackagesServiceProvider extends ServiceProvider
{
    protected $files;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMakeCommand();
    }

    /**
     * Register the "make:module" console command.
     *
     * @return Console\ModuleMakeCommand
     */
    protected function registerMakeCommand()
    {
        $this->commands('halcyon');

        $bind_method = method_exists($this->app, 'bindShared') ? 'bindShared' : 'singleton';

        $this->app->{$bind_method}('halcyon', function ($app) {
            return new \PackageHalcyon\BoilerPlatePackages2\Console\HalcyonCommand();
        });
    }
}
