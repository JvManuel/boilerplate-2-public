<?php 

namespace PackageHalcyon\Taxonomy\Models;

use PackageHalcyon\Base\Traits\Baseable;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use PackageHalcyon\History\Traits\Historable;
use PackageHalcyon\Menu\Traits\Menuable;

class Taxonomy extends Model
{
    use Sluggable, SluggableScopeHelpers, Historable, Baseable, Menuable;

    protected $fillable = [
        'name', 'type', 'depth', 'menu_id', 'parent_id', 'menu_depth'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Return the baseable title for this model.
     *
     * @return array
     */
    public function baseable()
    {
        return [
            'source' => 'name'
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function actions($frontend = false)
    {
        $user = auth()->user();
        $actions = [];

        // Backend Links
        return $actions;
    }
}
