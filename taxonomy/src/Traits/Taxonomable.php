<?php

namespace PackageHalcyon\Taxonomy\Traits;

use PackageHalcyon\Taxonomy\Facades\Taxonomy;

/**
 * Trait Taxonomable.
 */
trait Taxonomable
{
    public function parent()
    {
        return $this->belongsTo(get_class($this), 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(get_class($this), 'parent_id');
    }
    public static function boot()
    {
        static::deleted(function ($model) {
            $model->where('parent_id', $model->id)->update(['parent_id' => null]);
        });
        parent::boot();
    }
}
