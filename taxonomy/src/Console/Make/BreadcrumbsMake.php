<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class BreadcrumbsMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        $modulename = $this->_args->getCamelCaseModuleInput;

        $filename = str_replace(' ', '-', strtolower($this->_args->getNameInput));

        return [
                  'generate'=>  ['routes\\breadcrumbs\\backend\\', '/stub/breadcrumbs.stub', $filename, true, true],
                  'create'=>  [true,null]
                ];
    }
}
