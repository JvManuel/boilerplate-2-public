<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class RepositoriesMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        $modulename =  $this->_args->getCamelCaseModuleInput;

        return [
            [
                'generate'=>['Repositories\\Backend\\' . ucfirst(studly_case($modulename)) . '\\', '/stub/Repository/backend.stub', ucfirst(studly_case($modulename)) . 'ActivityController', true, true],
                'create'=>[false, null],
            ],
            [
                'generate'=>['Repositories\\Frontend\\' . ucfirst(studly_case($modulename)) . '\\',  '/stub/Repository/frontend.stub', ucfirst(studly_case($modulename)) . 'ActivityController', true, true],
                'create'=>[false, null],
            ],
        ];
    }
}
