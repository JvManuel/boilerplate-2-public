<?php
namespace PackageHalcyon\Taxonomy\Console\Make;

// use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class MigrationsMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        $filename = now()->format('Y_m_d_hms') . '_create_' . snake_case(str_plural($this->_args->getNameInput)) . '_table';

        // $fieldsString = ($this->_options->renderFieldCount === 0) ? $this->_defaultFieldsStringMigration() : $this->_customeFieldsStringMigration();
        // $fieldsString .= $this->_extraMigrationFields();

        return [
            'generate'=>['database\\migrations\\', '/stub/database/migration.stub', $filename, false, true],
            'create'=>[false, null
            //  function($value) use ($fieldsString)
            // {
            //     return FilterStub::generateMigrationFields($value, $fieldsString);
            // }
        ],
        ];
    }

    // private function _extraMigrationFields()
    // {
    //     $return = '';
    //     if ($this->_options->hasSluggable)
    //     {
    //         $return .= '$table->string(\'slug\');' . "\n";
    //     }
    //     if ($this->_options->hasStatus)
    //     {
    //         $return .= '$table->enum(\'status\', [\'active\', \'inactive\'])->default(\'active\');' . "\n";
    //     }
    //     if ($this->_options->hasSoftDelete)
    //     {
    //         $return .= '$table->softDeletes();' . "\n";
    //     }

    //     return $return;
    // }

    // private function _defaultFieldsStringMigration()
    // {
    //     return '$table->string(\'name\')->unique();
    //         $table->text(\'content\');
    //         $table->text(\'description\')->nullable();
    //         $table->text(\'image\')->nullable();';
    // }

    // private function _customeFieldsStringMigration()
    // {
    //     $fieldsString = '';
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $fieldsString .= "\$table->{$field['type']}('{$field['name']}');\n";
    //     }

    //     return $fieldsString;
    // }
}
