<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

// use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class SeedersMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        $return = [];
        $filename = ucfirst($this->_args->getCamelCaseModuleInput);

        // Permission Table Seeder
        $return[]=[
            'generate'=>['database\\seeds\\Auth\\ModulePermission\\', '/stub/database/permissionSeeder.stub', $filename . 'PermissionTableSeeder', false, true],
            'create'=>[false, null],
        ];

        // if ($this->_options->hasTableSeeder)
        // {
        // Module Data Seeder
        $return[]=[
                'generate'=>['database\\seeds\\Module\\', '/stub/database/seeder.stub', $filename . 'TableSeeder', false, true],
                'create'=>[false, null],
            ];
        // }
        return $return;
    }
}
