<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

// use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class ViewsMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        return $this->_init();
    }

    private function _init()
    {
        $return =[];
        $modulename =  $this->_args->getCamelCaseModuleInput;

        // Backend View
        $views = [
            'index' => 'index.blade',
            'create' => 'create.blade',
            'edit' => 'edit.blade',
            'show' => 'show.blade',
            'activity' => 'activity.blade'
        ];

        // if ($this->_options->hasStatus)
        // {
        // $views = array_merge($views, [
        //     'inactive' => 'inactive.blade',
        // ]);
        // }
        // if ($this->_options->hasSoftDelete)
        // {
        // $views = array_merge($views, [
        //     'deleted' => 'deleted.blade',
        // ]);
        // }

        foreach ($views as $v => $view) {
            // $customeFunction = null;
            // if (in_array($v, ['index', 'inactive', 'deleted']))
            // {
            //     $fieldsString    = ($this->_options->renderFieldCount === 0) ? $this->_defaultStringFieldView() : $this->_customeStringFieldView();
            //     $customeFunction = function($value) use($fieldsString)
            //     {
            //         return FilterStub::replaceView($value, $fieldsString);
            //     };
            // } elseif (in_array($v, ['show']))
            // {
            //     $fieldsString    = ($this->_options->renderFieldCount === 0) ? $this->_defaultStringFieldViewShow() : $this->_customeStringFieldViewShow();
            //     $customeFunction = function($value) use($fieldsString)
            //     {
            //         return FilterStub::replaceViewShow($value, $fieldsString);
            //     };
            // }
            // $this->_generate()
            //         ->_createFile();
            $return[]=[
                'generate'=>['resources\\views\\backend\\' .  $this->_args->getCamelCaseModuleInput . '\\', '/stub/views/backend/' . $v . '.stub', $view, false, true],
                'create'=>[false, null],
            ];
        }

        // Backend Includes
        $views = [
            'breadcrumb-links' => 'breadcrumb-links.blade',
            'fields' => 'fields.blade',
            'overview' => 'overview.blade'
        ];
        foreach ($views as $v => $view) {
            // $customeFunction = null;
            // if (in_array($v, ['fields']))
            // {
            //     $fieldsString    = ($this->_options->renderFieldCount === 0) ? $this->_defaultStringFieldViewEdit() : $this->_customeStringFieldViewEdit();
            //     $customeFunction = function($value) use($fieldsString)
            //     {
            //         return FilterStub::replaceViewEdit($value, $fieldsString);
            //     };
            // } elseif (in_array($v, ['overview']))
            // {
            //     $fieldsString    = ($this->_options->renderFieldCount === 0) ? $this->_defaultStringFieldViewShow() : $this->_customeStringFieldViewShow();
            //     $customeFunction = function($value) use($fieldsString)
            //     {
            //         return FilterStub::replaceViewShow($value, $fieldsString);
            //     };
            // }
            // $this->_generate()
            //         ->_createFile();

            $return[]=[
                'generate'=>['resources\\views\\backend\\' .  $this->_args->getCamelCaseModuleInput . '\\includes\\', '/stub/views/backend/includes/' . $v . '.stub', $view, false, true],
                'create'=>[false, null],
            ];
        }
        // if ($this->_options->hasFrontend)
        // {
        // Frontend
        $views = [
                'index' => 'index.blade',
                'show' => 'show.blade',
            ];
        foreach ($views as $v => $view) {
            // $this->_generate()
            //         ->_createFile();

            $return[]=[
                    'generate'=>['resources\\views\\frontend\\' .  $this->_args->getCamelCaseModuleInput . '\\', '/stub/views/frontend/' . $v . '.stub', $view, false, true],
                    'create'=>[false, null],
                ];
        }
        // }

        return $return;
    }

    // private function _defaultStringFieldViewEdit()
    // {

    //     return "
    //     <div class=\"form-group row\">
    //         <label class=\"col-md-2 form-control-label\">Name</label>
    //         <div class=\"col-md-10\">
    //             {!! Form::text('name', old('name'), ['class' => 'form-control ' . (\$errors->has('name') ? 'is-invalid' : '' ),
    //                 'placeholder' => 'Enter name', 'autofocus' => true]) !!}
    //         </div>
    //     </div>
    //     <div class=\"form-group row\">
    //         <label class=\"col-md-2 form-control-label\">Description</label>
    //         <div class=\"col-md-10\">
    //             {!! Form::textarea('description', old('description'), ['class' => 'form-control ' . (\$errors->has('description') ? 'is-invalid' : '' ),
    //                 'placeholder' => 'Enter description', 'rows' => 3]) !!}
    //         </div>
    //     </div>
    //     <div class=\"form-group row\">
    //         <label class=\"col-md-2 form-control-label\">Content</label>
    //         <div class=\"col-md-10\">
    //             {!! Form::textarea('content', old('content'), ['id' => 'ckeditor', 'class' => 'form-control' . (\$errors->has('content') ? 'is-invalid' : '' ),
    //                 'placeholder' => 'Enter content', 'rows' => 3]) !!}
    //         </div>
    //     </div>
    //     @include('base::form.image', ['required' => true])";
    // }

    // private function _customeStringFieldViewEdit()
    // {
    //     $return    = '';
    //     $dataTypes = [
    //         'string'
    //         =>
    //         "<div class=\"form-group row\">
    //             <label class=\"col-md-2 form-control-label\">:fieldName:</label>
    //             <div class=\"col-md-10\">
    //                 {!! Form::text(':field:', old(':field:'), ['class' => 'form-control ' . (\$errors->has(':field:') ? 'is-invalid' : '' ),
    //                     'placeholder' => 'Enter :field:', 'autofocus' => true]) !!}
    //             </div>
    //         </div>",
    //         'integer'
    //         =>
    //         "<div class=\"form-group row\">
    //             <label class=\"col-md-2 form-control-label\">:fieldName:</label>
    //             <div class=\"col-md-10\">
    //                 {!! Form::text(':field:', old(':field:'), ['class' => 'form-control ' . (\$errors->has(':field:') ? 'is-invalid' : '' ),
    //                     'placeholder' => 'Enter :field:', 'autofocus' => true]) !!}
    //             </div>
    //         </div>",
    //         'boolean'
    //         =>
    //         "<div class=\"form-group row\">
    //             <label class=\"col-md-2 form-control-label\">:fieldName:</label>
    //             <div class=\"col-md-10\">
    //                 {!! Form::text(':field:', old(':field:'), ['class' => 'form-control ' . (\$errors->has(':field:') ? 'is-invalid' : '' ),
    //                     'placeholder' => 'Enter :field:', 'autofocus' => true]) !!}
    //             </div>
    //         </div>",
    //         'text'
    //         =>
    //         "<div class=\"form-group row\">
    //         <label class=\"col-md-2 form-control-label\">:fieldName:</label>
    //         <div class=\"col-md-10\">
    //             {!! Form::textarea(':field:', old(':field:'), ['class' => 'form-control ' . (\$errors->has(':field:') ? 'is-invalid' : '' ),
    //                 'placeholder' => 'Enter :field:', 'rows' => 3]) !!}
    //         </div>
    //     </div>",
    //         'datetime'
    //         =>
    //         "<div class=\"form-group row\">
    //             <label class=\"col-md-2 form-control-label\">:fieldName:</label>
    //             <div class=\"col-md-10\">
    //                 {!! Form::text(':field:', old(':field:'), ['class' => 'form-control ' . (\$errors->has(':field:') ? 'is-invalid' : '' ),
    //                     'placeholder' => 'Enter :field:', 'autofocus' => true]) !!}
    //             </div>
    //         </div>",
    //     ];
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $name = ucwords(str_replace('_', ' ', $field['name']));
    //         $form = $dataTypes[$field['type']];

    //         $tmp    = str_replace(':fieldName:', $name, $form);
    //         $return .= str_replace(':field:', $field['name'], $tmp);
    //     }
    //     return $return;
    // }

    // private function _defaultStringFieldViewShow()
    // {

    //     return '<tr>
    //                 <th>Image</th>
    //                 <td>
    //                     <a class="fancy-box" href="{{ $model->image }}">
    //                         <img src="{{ $model->image }}" alt="no image" class="img-preview rounded"/>
    //                     </a>
    //                 </td>
    //             </tr>
    //             <tr>
    //                 <th>Name</th>
    //                 <td>{{ $model->name }}</td>
    //             </tr>
    //             <tr>
    //                 <th>Description</th>
    //                 <td class="pre-line">{{ $model->description }}</td>
    //             </tr>
    //             <tr>
    //                 <th>Content</th>
    //                 <td>{!! $model->content !!}</td>
    //             </tr>';
    // }

    // private function _customeStringFieldViewShow()
    // {
    //     $return    = '';
    //     $dataTypes = [
    //         'string' => '<th>',
    //         'integer' => '<th>',
    //         'boolean' => '<th>',
    //         'text' => '<th class="pre-line">',
    //         'datetime' => '<th>',
    //     ];
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $name    = ucwords(str_replace('_', ' ', $field['name']));
    //         $openTag = $dataTypes[$field['type']];

    //         $return .= "<tr>
    //                         <th>{$name}</th>
    //                 {$openTag}
    //                         {{ \$model->{$field['name']} }}
    //                         </th>
    //                    </tr>\n";
    //     }
    //     return $return;
    // }

    // private function _defaultStringFieldView()
    // {
    //     $return = [
    //         'top' => '<th></th>' . "\n" . '<th>Name</th>',
    //         'bottom' => '{
    //                     data: \'image\',
    //                     render: function(data){ return renderImage(data); },
    //                     searchable: false,
    //                     sortable: false
    //                 },
    //                 {data: \'name\',      },',
    //     ];

    //     return $return;
    // }

    // private function _customeStringFieldView()
    // {
    //     $return = [
    //         'top' => '',
    //         'bottom' => '',
    //     ];
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $name = ucwords(str_replace('_', ' ', $field['name']));

    //         $return['top']    .= "<th>{$name}</th>\n";
    //         $return['bottom'] .= "{data: '{$field['name']}', },\n";
    //     }
    //     return $return;
    // }
}
