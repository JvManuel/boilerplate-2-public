<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

// use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class ControllersMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        $return = [];
        $modulename =  $this->_args->getCamelCaseModuleInput;

        $controllers = [
            'crud' =>  ucfirst(str_plural($modulename)) . 'Controller',
            'table' =>  ucfirst(studly_case($modulename)) . 'TableController',
            'hierarchy' =>  ucfirst(studly_case($modulename)) . 'HierarchyController',
        ];

        // Add Status Controller
        // if ($this->_options->hasStatus)
        // {
        // $controllers = array_merge($controllers, ['status' => ucfirst(studly_case($modulename)) . 'StatusController']);
        // }

        // Add Activity Controller
        // if ($this->_options->hasActivity)
        // {
        $controllers = array_merge($controllers, ['activity' => ucfirst(studly_case($modulename)) . 'ActivityController']);

        // }

        // Add Soft Delete
        // if ($this->_options->hasSoftDelete)
        // {
        // $controllers = array_merge($controllers, ['softdelete' => ucfirst(studly_case($modulename)) . 'DeletedController']);
        // }

        foreach ($controllers as $c => $controller) {
            // $customeFunction = null;
            // if ($c == 'crud')
            // {
            //     $fieldsString = ($this->_options->renderFieldCount === 0) ? $this->_defaultFieldsStringController() :  $this->_customeFieldsStringController();
            //     $customeFunction = function($value) use($fieldsString)
            //     {
            //         return FilterStub::generateCrudControllerValidation($value, $fieldsString);
            //     };
            // }
            
            $return[]=[
                'generate'=>['Http\\Controllers\\Backend\\' . ucfirst(studly_case($modulename)) . '\\', '/stub/Controller/backend/' . $c . '.stub', $controller, true, true],
                'create'=>[false, null],
            ];
        }

        // if ($this->_options->hasFrontend)
        // {
        $controller =  ucfirst(str_plural($modulename)) . 'Controller';

        $return[]=[
                'generate'=>['Http\\Controllers\\Frontend\\' . ucfirst(studly_case($modulename)) . '\\', '/stub/Controller/frontend/crud.stub', $controller, true, true],
                'create'=>[false, null],
            ];
        // }

        return $return;
    }

    // private function _defaultFieldsStringController()
    // {
    //     return [
    //         'store'
    //         =>
    //         '\'name\' => "required|max:255|unique:$table",
    //         \'description\' => \'max:255\',
    //         \'content\' => \'required\',
    //         \'image\' => \'file|mimes:jpg,png,pdf,jpeg\',',
    //         'update'
    //         =>
    //         '\'name\' => "required|max:255|unique:$table,name,{$model->id}",
    //         \'description\' => \'max:255\',
    //         \'content\' => \'required\',
    //         \'image\' => \'file|mimes:jpg,png,pdf,jpeg\','
    //     ];
    // }

    // private function _customeFieldsStringController()
    // {
    //     $return = [
    //         'store' => '',
    //         'update' => '',
    //     ];
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $return['store']  .= "'{$field['name']}' => 'required',\n";
    //         $return['update'] .= "'{$field['name']}' => 'required',\n";
    //     }
    //     return $return;
    // }
}
