<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

// use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class ModelsMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }
    public function structures()
    {
        // $modulename =  $this->_args->getCamelCaseModuleInput;
        // $filename   = ucfirst(studly_case($modulename));

        // $fieldsString = ($this->_options->renderFieldCount === 0) ? $this->_defaultFieldsStringModel() : $this->_customeFieldsStringModel();
        // $fieldsString .= $this->_extraStringfieldsModel();

        return [
            'generate'=>['Models\\', '/stub/model.stub', false, true, false, false],
            'create'=>[false,null
            //  function($value) use($fieldsString)
            // {
            //     return FilterStub::replaceFillable($value, $fieldsString);
            // }
        ],
        ];
    }

    // private function _extraStringfieldsModel()
    // {
    //     $return = '';
    //     if ($this->_options->hasStatus)
    //     {
    //         $return .= "'status', ";
    //     }
    //     return $return;
    // }

    // private function _defaultFieldsStringModel()
    // {
    //     return "'name', 'content', 'description', 'image', ";
    // }

    // private function _customeFieldsStringModel()
    // {
    //     $fieldsString = '';
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $fieldsString .= "'{$field['name']}', ";
    //     }
    //     return $fieldsString;
    // }
}
