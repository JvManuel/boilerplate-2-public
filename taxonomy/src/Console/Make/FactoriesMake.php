<?php

namespace PackageHalcyon\Taxonomy\Console\Make;

// use PackageHalcyon\Modular\Console\Filter\FilterStub;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Contracts\StubGeneratorInterface;

class FactoriesMake implements StubGeneratorInterface
{
    private $_arg;
    private $_options;

    public function __toString() :string
    {
        return get_class();
    }

    public function setArgs(array $args, array $options)
    {
        $this->_args =(object) $args;
        $this->_options =(object) $options;
    }

    public function structures()
    {
        $filename = ucfirst($this->_args->getCamelCaseModuleInput);

        // $fieldsString = ($this->_options->renderFieldCount === 0) ? $this->_defaultFieldsStringFactory() : $this->_customeFieldsStringFactory();

        return [
            'generate'=>['database\\factories\\Module\\', '/stub/database/factory.stub', $filename . 'Factory', false, true],
            'create'=>[false,null
            //  function($value) use ($fieldsString)
            // {
            //     return FilterStub::generatefactoryFields($value, $fieldsString);
            // }
        ],
        ];
    }

    // private function _defaultFieldsStringFactory()
    // {
    //     $return = '\'name\' => $faker->name,
    //     \'content\' => $faker->sentence(),
    //     \'description\' => $faker->sentence(50),';

    //     if ($this->_options->hasStatus)
    //     {
    //         $return .= '\'status\' => \'active\',' . "\n";
    //     }
    //     return $return;
    // }

    // private function _customeFieldsStringFactory()
    // {
    //     $fieldsString = '';
    //     $dataTypes    = [
    //         'string' => '$faker->sentence(10)',
    //         'integer' => '$faker->numberBetween(1, 20)',
    //         'boolean' => '$faker->boolean',
    //         'text' => '$faker->sentence(50)',
    //         'datetime' => '$faker->dateTime()',
    //     ];
    //     foreach ($this->_options->fields as $field)
    //     {
    //         $seeder       = $dataTypes[$field['type']];
    //         $fieldsString .= "'{$field['name']}' => $seeder,\n";
    //     }
    //     return $fieldsString;
    // }
}
