<?php

namespace App\Http\Controllers\Backend\DummyTitle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\DummyTitle\DummyTitleRepository as Repository;

use App\Models\DummyTitle as Model;

/**
 * Class DummyTitleTableController.
 */
class DummyTitleTableController extends Controller
{
    /**
     * @var Repository
     */
    protected $repo;

    /**
     * @param Repository $repo
     */
    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
        $this->middleware('permission:dummy title list,dummy title activity,dummy title delete', ['only' => ['__invoke']]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $user = auth()->user();
        return DataTables::of($this->repo->table($request))
            ->escapeColumns(['id'])
            ->editColumn('updated_at', function($model){
                return $model->updated_at->format('d M, Y h:m A');
            })
            ->addColumn('actions', function ($model) {
                return $model->actions();
            })
            ->make(true);
    }
}
