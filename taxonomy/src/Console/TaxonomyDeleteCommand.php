<?php

namespace PackageHalcyon\Taxonomy\Console;

use Illuminate\Console\GeneratorCommand;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Status\StubStatusTrait;

class TaxonomyDeleteCommand extends GeneratorCommand
{
    use StubStatusTrait;

    protected $stubStatusFileName = 'taxonomy.json';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'taxonomy:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete taxonomies generated.';
    private $_moduleName;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->_moduleName = $this->getNameInput();
        $this->_checkFile();
    }

    private function _checkFile()
    {
        $this->line("checking module '{$this->_getCamelCaseModuleInput()}' ...");

        $datas = $this->getDataModularFileDatas($this->_moduleName);

        if (is_null($datas)) {
            $this->error("module '{$this->_moduleName}' not exist\n");
            return;
        } elseif ($datas->status == 'inactive') {
            $this->error("module '{$this->_moduleName}' already deleted at {$datas->deleted_at}\n");
            return;
        }


        $this->_deletingFiles($datas->datas);

        $this->line("\n" . '<bg=green>Module "' . $this->_moduleName . '" deleted successfully.</>');
    }

    private function _deletingFiles($datas)
    {
        foreach ($datas as $file) {
            $file = $this->addProjectDir($file);
            $this->line('<fg=yellow>Deleting:</>      ' . $file);
            $this->files->delete($file);
            $this->line('<fg=green>Done Deleting:</> ' . $file);
        }

        $this->udpateDeleteData($this->_moduleName);
    }

    private function _getCamelCaseModuleInput()
    {
        return camel_case($this->_moduleName);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->currentStub;
    }
}
