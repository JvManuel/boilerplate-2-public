<?php

namespace PackageHalcyon\Taxonomy\Console;

use Illuminate\Console\GeneratorCommand;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Status\StubStatusTrait;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TaxonomyStatusCommand extends GeneratorCommand
{
    use StubStatusTrait;
    protected $stubStatusFileName = 'taxonomy.json';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'taxonomy:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'See Taxonomies status.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->getNameInput() != '') {
            $module = $this->getDataModularFileDatas($this->getNameInput());

            if (is_null($module)) {
                $this->error("module '{$this->getNameInput()}' not exist\n");
                return;
            }
            if ($this->option('type')) {//dd($module->types);
                $this->_tableTypes($module->types);
            } else {
                $this->_tableFiles($module->datas);
            }
        } else {
            $this->_tableAll($this->getDataModularFileDatas());
        }
    }

    private function _tableTypes($module)
    {
        $datas = [];
        $inc   = 1;

        $fields = null;
        foreach ($module as $type => $input) {
            if ($type == 'fields') {
                $fields = $input;
                continue;
            }
            $datas[] = [
                $inc++,
                $type,
                is_bool($input) ? ($input ? 'true' : 'false') : $input,
            ];
        }
        $this->table([
            '#',
            'Type', 'Input'
                ], $datas);

        $datas = [];
        $inc   = 1;

        if (!is_null($fields)) {
            foreach ($fields as $field) {
                $datas[] = [
                    $inc++, $field->name, $field->type
                ];
            }
        }
        $this->table([
            '#',
            'Name', 'Data Type'
                ], $datas);
    }

    private function _tableFiles($module)
    {
        $datas = [];
        $inc   = 1;

        foreach ($module as $file) {
            $datas[] = [
                $inc++,
                $file,
            ];
        }

        $this->table([
            '#',
            'File',
                ], $datas);
    }

    private function _tableAll($modules)
    {
        $header = [
            'Module Name',
            'Status',
            'File Count',
            'created at',
            'updated at',
            'deleted at',
        ];
        $datas  = [];

        foreach ($modules as $module => $value) {
            $countFile = count($value->datas);
            $datas[]   = [
                $module,
                $value->status,
                $countFile,
                $value->created_at,
                $value->updated_at,
                (is_null($value->deleted_at)) ? 'null' : $value->deleted_at,
            ];
        }

        $this->table($header, $datas);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->currentStub;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            ['name', InputArgument::OPTIONAL, 'Module name.'],
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            ['type', null, InputOption::VALUE_NONE, 'See Type inputs'],
        );
    }
}
