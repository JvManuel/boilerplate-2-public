<?php

namespace PackageHalcyon\Taxonomy\Console;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Maker\StubGeneratorTraits;
use PackageHalcyon\BoilerPlatePackages2\Console\Stub\Traits\Status\StubStatusTrait;
use PackageHalcyon\Taxonomy\Console\Filter\FilterStub as FilterStub;

class TaxonomyMakeCommand extends GeneratorCommand
{
    use StubGeneratorTraits, StubStatusTrait;
    protected $stubStatusFileName = 'taxonomy.json';

    /**
     * Laravel version
     *
     * @var string
     */
    protected $version;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:taxonomy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new taxonomy (folder structure)';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Taxonomy';

    /**
     * The current stub.
     *
     * @var string
     */
    protected $currentStub;
    private $_path;

    /**
     * Store all strings generated  files directory
     */
    private $_modulesFiles;

    private $dir = __DIR__;

    /**
     * Execute the console command >= L5.5.
     *
     * @return void
     */
    public function handle()
    {
        $this->version       = (int) str_replace('.', '', app()->version());
        $this->_modulesFiles = [];
        $this->setFilterStub(new FilterStub);
        
   
        $this->classes = [
            \PackageHalcyon\Taxonomy\Console\Make\RoutesMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\ControllersMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\RepositoriesMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\ModelsMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\BreadcrumbsMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\MigrationsMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\SeedersMake::class,
            \PackageHalcyon\Taxonomy\Console\Make\FactoriesMake::class,
            // 'PackageHalcyon\Taxonomy\Console\Make\UnitTestsMake', // TODO: soon
            \PackageHalcyon\Taxonomy\Console\Make\ViewsMake::class,
        ];

        $this->excecute();
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            ['name', InputArgument::REQUIRED, 'Taxonomy name.'],
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
