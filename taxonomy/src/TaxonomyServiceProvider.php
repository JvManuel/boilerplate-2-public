<?php

namespace PackageHalcyon\Taxonomy;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use PackageHalcyon\Taxonomy\Taxonomy;
use Illuminate\Support\Facades\Schema;

class TaxonomyServiceProvider extends ServiceProvider
{
    protected $files;

    public function boot()
    {
    }

    public function register()
    {
        //  Register Facades
        $this->app->bind('taxonomy', function ($app) {
            return new Taxonomy($app);
        });
        $this->app->alias('taxonomy', \PackageHalcyon\Taxonomy\Facades\Taxonomy::class);

        $this->loadViewsFrom(__DIR__ . '/views', 'taxonomy');
        
        // // Migration
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->_regiterGenerator();
    }

    private function _regiterGenerator()
    {
        $this->files = new Filesystem;
        $this->commands('taxonomy.make');
        $this->commands('taxonomy.delete');
        $this->commands('taxonomy.status');

        $bind_method = method_exists($this->app, 'bindShared') ? 'bindShared' : 'singleton';
        $this->app->{$bind_method}('taxonomy.make', function ($app) {
            return new Console\TaxonomyMakeCommand($this->files);
        });
        $this->app->{$bind_method}('taxonomy.delete', function ($app) {
            return new Console\TaxonomyDeleteCommand($this->files);
        });
        $this->app->{$bind_method}('taxonomy.status', function ($app) {
            return new Console\TaxonomyStatusCommand($this->files);
        });
    }
}
