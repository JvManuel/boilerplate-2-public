@extends('backend.layouts.app')

@section ('title', $model->name . ' Management | '. $model->name .' Configurations')

@section('breadcrumb-links')
    @include('backend.'. $model->slug .'.includes.breadcrumb-links')
@endsection
@section('content')
	<div class="row">
		<div class="col">
		    {!! Form::model($model, ['url' => route('admin.'.$model->slug.'.setting.update'), 'file' => 'mutlipart/enctype', 'files' => true, 'id' => 'content-form', 'class' => 'form-horizontal', 'method' => 'PATCH' ]) !!}
		        <div class="card card-form">
					<div class="card-body">
	                    <h4 class="card-title mb-0">
	                        {{ $model->name }} Management <small class="text-muted">{{ $model->name }} Settings</small>
	                    </h4>
	                    <hr/>
	                    <div class="form-group">
	                    	<label>Hierarchy Maximum Depth</label>
	                    	{!! Form::number('depth', old('depth'), ['class' => 'form-control']) !!}
	                    	<small>The maximum depth of a {{ strtolower($model->name) }} tree. </small>
	                    </div>
	                </div>
	                <div class="card-footer text-right">
	                    {{ Form::submit('Submit', ['class' => 'btn btn-success btn-sm btn-submit']) }}
	                	<div class="clearfix"></div>
	                </div>
                </div>
            {!! Form::close() !!}
		</div>
	</div>
@endsection

@push('after-scripts')
    <script type="text/javascript">
        $(document).ready(function() { formSubmit($('#content-form')) })
    </script>
@endpush


