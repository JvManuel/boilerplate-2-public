<div class="card" id="hierarchy_form">
    <div class="card-header">Category Hierarchy</div>
    <div class="card-body">
        <div id="hierarchy" class="dd"></div>
    </div>
    <div class="card-footer">
        <a class="btn btn-success pull-right" name="btn_hierarchy_update"><i class="fa fa-save"></i> Update Changes</a>
        <div class="clearfix"></div>
    </div>
</div>