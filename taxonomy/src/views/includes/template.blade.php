<script type="text/x-custom-template" id="tpl_list_group">
    <ol class="dd-list"></ol>
</script>

<script type="text/x-custom-template" id="tpl_list_item">
    <li class="dd-item dd3-item"  data-id="__id__" >
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
            <span class="text pull-left">__name__</span>
            <span class="text pull-right">__actions__</span>
        </div>
        <div class="clearfix"></div>
    </li>
</script>