<li class="media">
	<div class="media-body">
		{{ $node->name }} 
		@if(count($node->children))
			<ul>
				@foreach($node->children as $c => $child)
					@include('taxonomy::backend.hierarchy.includes.node', ['node' => $child])
				@endforeach
			</ul>
		@endif
	</div>
</li>