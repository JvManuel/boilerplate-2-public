@extends('backend.layouts.app')

@section ('title', 'Categories Management | Categories Hierarchy')

@section('breadcrumb-links')
    @include('backend.category.includes.breadcrumb-links')
@endsection

@push('after-styles')
	@include('backend.category.hierarchy.assets.css')
@endpush
@section('content')
	<div class="row col">
		<div class="col-sm-12">
			@include('backend.category.hierarchy.includes.hierarchy')
		</div>
	</div>
@endsection
@push('after-scripts')

		@include('backend.category.hierarchy.includes.template')
		@include('backend.category.hierarchy.assets.js')
		@include('backend.category.hierarchy.assets.hierarchy')
		
	    <script type="text/javascript">
            $(document).ready(function () {
                var hierarchyUI = new HierarchyUI.constuctor();
                    hierarchyUI.init({
                        nodeLink  : '{{ route('admin.category.hierarchy.table') }}',
                        nodeStore : '{{ route('admin.category.hierarchy.update') }}',
                        depth: 5
                    });
            })
	    </script>
@endpush