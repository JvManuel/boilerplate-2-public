<?php

namespace PackageHalcyon\Taxonomy\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Taxonomy
 .
 */
class Taxonomy extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'taxonomy';
    }
}
