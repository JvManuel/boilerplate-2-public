<?php

namespace PackageHalcyon\Taxonomy;

use Illuminate\Support\Facades\DB;

/**
 * Class Taxonomy.
 */
class Taxonomy
{
    protected $model;
    protected $nodes;
    protected $depth;

    /**
     * Returns the hierarchy structure of a taxonomy
     * @return array
     */
    public function hierarchy($class, $parentID = null) : array
    {
        $this->model = app($class);
        if (! $this->nodes) {
            $this->nodes = $this->model->orderBy('order', 'ASC')->get();
        }

        $nodes = $this->nodes->where('parent_id', $parentID);
        $result = [];

        foreach ($nodes as $n => $node) {
            $node->children = $this->hierarchy($class, $node->id);
            $node->actions = $node->actions();
            $result[] = $node;

            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                $this->nodes = null;
                $this->model = null;
            }
        }
        return $result;
    }

    /**
     * Returns the hierarchy structure of a taxonomy in an array
     * @return array
     */
    public function hierarchiesInForm($classes, &$result = [])
    {
        $lists = [];
        if (count($classes)) {
            foreach ($classes as $c => $class) {
                $lists[$c] = $this->hierarchyInForm($class);
            }
        }
        return $lists;
    }


    /**
     * Returns the hierarchy structure of a taxonomy in an array
     * @return array
     */
    public function hierarchyInForm($class, &$result = [], $label = "", $parentID = null, $depth = null)
    {
        $this->model = app($class);
        if (! $this->nodes) {
            $this->nodes = $this->model->orderBy('order', 'ASC')->get();
        }
        $nodes = $this->nodes->where('parent_id', $parentID);
        foreach ($nodes as $n => $node) {
            $result[$node->id] = $label . $node->name;
            if ($depth) {
                if (! $this->depth) {
                    $this->depth = 0;
                } elseif ($this->depth >= $depth) {
                    $this->depth = null;
                    break;
                } else {
                    $this->depth++;
                }
            }
            $this->hierarchyInForm($class, $result, $label . '-', $node->id);
            
            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                $this->nodes = null;
                $this->model = null;
                $this->depth = null;
            }
        }
        return $result;
    }


    /**
     * Returns the hierarchy structure of a taxonomy in an array
     * @return array
     */
    public function hierarchyToArray($class, &$result, $parentID = null)
    {
        $this->model = app($class);
        if (! $this->nodes) {
            $this->nodes = $this->model->orderBy('order', 'ASC')->get();
        }
        $nodes = $this->nodes->where('parent_id', $parentID);
        foreach ($nodes as $n => $node) {
            $result[] = $node;
            $this->hierarchyToArray($class, $result, $node->id);
            /**
             * Reset related nodes when done
             */
            if ($node->id == $nodes->last()->id && $parentID === null) {
                $this->nodes = null;
                $this->model = null;
            }
        }
        return $result;
    }


    /**
     * Returns the hierarchy route
     * @param Taxonomy $model
     * @param Appended Collection &$list
     * @return Collection $list
     */
    public function getRoots($model, &$list, $include_model = true)
    {
        if (! $list instanceof Illuminate\Database\Eloquent\Collection) {
            $list = collect($list);
        }
        if ($include_model) {
            $list->prepend($model);
        }
        if ($model->parent_id != null) {
            $this->getRoots($model->parent, $list);
        }
        return $list;
    }

    /**
     * Returns the hierarchy route
     * @param Taxonomy $model
     * @param Appended String &$route
     * @return String $route
     */
    public function getRouteRoots($model, &$route, $include_model = true) : String
    {
        $slug = $include_model ? $model->{$model->getRouteKeyName()} : "";
        $route = $slug . ($route != "" ? '/' : '') . $route ;
        if ($model->parent_id != null) {
            $this->getRouteRoots($model->parent, $route);
        }
        return $route;
    }

    /**
     * Update
     *
     **/
    /**
     * Update Hierarchy
     * @param Object $request
     * @return mixed
     */
    public function updateHierarchy($request, $class)
    {
        $this->model = app($class);
        DB::beginTransaction();
        try {
            // Delete
            $nodes = $request->deleted;
            if ($request->deleted && !is_array($nodes)) {
                $nodes = json_decode($nodes ?? []);
            }
            $this->deleteHierarchyNode($nodes);
            // Structure
            $hierarchy = $request->hierarchy;
            if (!is_array($hierarchy)) {
                $hierarchy = json_decode($hierarchy, true);
            }

            $list = $this->model->orderBy('order', 'ASC')->get();
            $nodes = collect();
            foreach ($hierarchy as $order => $node) {
                $nodes = $nodes->push($this->updateHierarchyNode($list, $node, $order));
            }

            DB::commit();
            return $this->hierarchy($class);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Update Node by Hierarchy
     * @param Collection $list
     * @param Model $node
     * @param int $order
     * @param int $parentID
     * @return mixed
     */
    protected function updateHierarchyNode($list, $node, $order, $parentID=null)
    {
        DB::beginTransaction();
        try {
            $data = ['order' => $order, 'parent_id' => $parentID];
            if (array_key_exists('id', $node) && $node['id']) {
                $result = $list->where('id', $node['id'])->first();
                $result->update($data);
            }

            $children = array_get($node, 'children', []);
            if (count($children)) {
                foreach ($children as $key => $child) {
                    $this->updateHierarchyNode($list, $child, $key, $node['id']);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Delete items by id
     * @param EloquentBase|int|array|null $id
     * @return mixed
     */
    protected function deleteHierarchyNode($id = null)
    {
        $nodes = null;
        if (is_array($id)) {
            $nodes = $this->model->whereIn('id', $id);
        } elseif ($id instanceof Model) {
            $nodes = $id;
        } else {
            $nodes = $this->model->where('id', '=', $id);
        }
        try {
            $nodes->delete();
            return true;
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
