<?php

namespace PackageHalcyon\History;

use Illuminate\Support\ServiceProvider;

class HistoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //	     Translation
        $this->loadTranslationsFrom(__DIR__ . '/lang', 'history');
    }

    public function register()
    {
        //  Register Facades
        $this->app->bind('history', function ($app) {
            return new History($app);
        });
        $this->app->alias('history', \PackageHalcyon\History\Facades\History::class);

        // Migration
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        // Views
        $this->loadViewsFrom(__DIR__ . '/views', 'history');


        // Translation
        // $this->publishes([
        //     __DIR__.'/lang' => resource_path('lang/vendor/history'),
        // ]);
    }
}
