<?php

return [
    'actions' => [
        'created' 	=> ':User created :Name',
        'updated' 	=> ':User updated :Name',
        'deleted' 	=> ':User deleted :Name',
        'restored' 	=> ':User restored :Name',
        'purged' 	=> ':User permanently deleted :Name',
        'changed-status' => ':User changed the status of the :Name to :status',
    ],
    'none' => 'There are no recent activities found.'
];
