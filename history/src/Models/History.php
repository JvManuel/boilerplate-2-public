<?php 

namespace PackageHalcyon\History\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = [
       'user_id', 'historable_id', 'historable_type', 'action', 'display', 'icon', 'color', 'options'
    ];

    public function historable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        // return $this->belongsTo(config('history.auth.user.model'));
        return $this->belongsTo(\App\Models\Auth\User::class);
    }

    public function option($key)
    {
        $options = json_decode($this->options);
        return array_key_exists($key, $options) ? $options->$key : null;
    }
}
