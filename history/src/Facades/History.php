<?php

namespace PackageHalcyon\History\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Upload.
 */
class History extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'history';
    }
}
