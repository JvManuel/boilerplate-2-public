<?php

namespace PackageHalcyon\History;

use PackageHalcyon\History\Models\History as Model;
use Illuminate\Support\Facades\DB;

/**
 * Class History.
 */
class History
{
    public function log($model, $data)
    {
        $data['user_id']         = array_key_exists('user_id', $data) ?? auth()->id();
        $data['historable_id']   = $model->id;
        $data['historable_type'] = get_class($model);

        Model::create($data);
    }

    /**
     * History Log Create Template
     * @param Model $model
     * @param array  $data
     */
    public function created($model, $user = null, $data = [])
    {
        $user            = $user ?? auth()->user();
        $data['user_id'] = $user->id;
        $data['icon']    = 'fa fa-plus';
        $data['color']   = 'bg-success';
        $data['display'] = $this->_display($user, $model, 'created');

        $this->log($model, $data);
    }

    private function _display($user, $model, $type, $nolink = false)
    {
        $route = str_replace('_', '-', str_singular($model->getTable()));
        if ($nolink) {
            $modelLink = '<b>'.  $model->base('history_name')  .'</b>';
        } else {
            $modelLink = '<a href="' . route("admin.$route.show", $model) . '">' . $model->base('history_name') . '</a>';
        }
        return '<b><a href="' . route('admin.auth.user.show', $user) . '">' . $user->name . '</a></b>'
                . ' ' . $type . ' ' . $modelLink .'</b>.';
    }

    /**
     * History Log Update Template
     * @param Model $model
     * @param Model $oldModel
     * @param array  $data
     */
    public function updated($model, $oldModel, $user = null, $data = [], $nolink = false)
    {
        $user            = $user ?? auth()->user();
        $data['user_id'] = $user->id;
        $data['icon']    = 'fa fa-pencil';
        $data['color']   = 'bg-primary';
        $data['render']  = 'json';
        $data['display'] = $this->_display($user, $model, 'updated', $nolink);

        $this->log($model, $data);
    }

    /**
     * History Log Delete Template
     * @param Model $model
     * @param array  $data
     */
    public function deleted($model, $user = null, $data = [])
    {
        $user            = $user ?? auth()->user();
        $data['user_id'] = $user->id;
        $data['icon']    = 'fa fa-trash';
        $data['color']   = 'bg-danger';
        $data['display'] = $this->_display($user, $model, 'deleted');

        $this->log($model, $data);
    }

    /**
     * History Log Restore Template
     * @param Model $model
     * @param array  $data
     */
    public function restored($model, $data = [])
    {
        $user            = $user ?? auth()->user();
        $data['user_id'] = $user->id;
        $data['icon']    = 'fa fa-refresh';
        $data['color']   = 'bg-info';
        $data['display'] = $this->_display($user, $model, 'restored');

        $this->log($model, $data);
    }

    /**
     * History Log Purge Template
     * @param Model $model
     * @param array  $data
     */
    public function purged($model, $data = [])
    {
        $user            = $user ?? auth()->user();
        $data['user_id'] = $user->id;
        $data['icon']    = 'fa fa-trash';
        $data['color']   = 'bg-danger';
        $data['display'] = $this->_display($user, $model, 'permanently deleted');

        $this->log($model, $data);
    }

    // Render
    /**
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function render($limit = null, $paginate = true, $pagination = 10)
    {
        $history = Model::with(['user',
                    'historable' => function ($query) use ($type) {
                        if (method_exists(app($type), 'bootSoftDeletes')) {
                            $query->withTrashed();
                        }
                    }])->latest();
        $history = $this->buildPagination($history, $limit, $paginate, $pagination);
        if (!$history->count()) {
            return trans('history.backend.none');
        }

        return $this->buildList($history, $paginate, $pagination);
    }

    /**
     * @param $type
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function renderType($type, $limit = null, $paginate = true, $pagination = 10)
    {
        $history = Model::with(['user',
                    'historable' => function ($query) use ($type) {
                        if (method_exists(app($type), 'bootSoftDeletes')) {
                            $query->withTrashed();
                        }
                    }
                ])->where('historable_type', $type)->latest();
        // $history = $this->buildPagination($history, $limit, $paginate, $pagination);
        if (!$history->count()) {
            return trans('history::message.none');
        }

        return $this->buildList($history, $paginate, $pagination);
    }

    /**
     * @param $type
     * @param $entity_id
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function renderEntity($historable, $limit = null, $paginate = true, $pagination = 10)
    {
        $type    = get_class($historable);
        $history = Model::with(['user',
                    'historable' => function ($query) use ($type) {
                        if (method_exists(app($type), 'bootSoftDeletes')) {
                            $query->withTrashed();
                        }
                    }
                ])->where('historable_type', get_class($historable))->where('historable_id', $historable->id)->latest();
        if (!$history->count()) {
            return trans('history::message.none');
        }
        return $this->buildList($history, $paginate, $pagination);
    }

    /**
     * @param $type
     * @param $entity_id
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function renderEntityUser($historable, $limit = null, $paginate = true, $pagination = 10)
    {
        $type    = get_class($historable);
        $history = Model::with(['user',
                            'historable' => function ($query) use ($type) {
                                if (method_exists(app($type), 'bootSoftDeletes')) {
                                    $query->withTrashed();
                                }
                            }
                        ])->whereHas('user', function ($q) {
                            $q->where('id', auth()->id());
                        })
                        ->where('historable_type', get_class($historable))->where('historable_id', $historable->id)->latest();
        // $history = $this->buildPagination($history, $limit, $paginate, $pagination);
        if (!$history->count()) {
            return trans('history::message.none');
        }
        return $this->buildList($history, $paginate, $pagination);
    }

    /**
     * @param $type
     * @param $entity_id
     * @param null $limit
     * @param bool $paginate
     * @param int  $pagination
     *
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function renderUser($historable, $limit = null, $paginate = true, $pagination = 10)
    {
        $history = Model::with(['user'])->whereHas('user', function ($q) {
            $q->where('id', auth()->id());
        })->latest();
        // $history = $this->buildPagination($history, $limit, $paginate, $pagination);
        if (!$history->count()) {
            return trans('history::message.none');
        }
        return $this->buildList($history, $paginate, $pagination);
    }

    /**
     * @param $history
     * @param bool $paginate
     *
     * @return string
     */
    public function buildList($histories, $paginate = true, $pagination = 10)
    {
        return view('history::list', ['histories' => $histories->paginate($pagination, ['*'], 'history'), 'paginate' => $paginate])->render();
    }
}
