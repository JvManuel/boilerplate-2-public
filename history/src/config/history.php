<?php

return [
    'auth' => [
        'user' => [
            'model' => \App\Models\Auth\User::class,
            'table' => 'users'
        ]
    ]
];
