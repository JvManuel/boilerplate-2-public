<?php
namespace PackageHalcyon\History\Traits;

use PackageHalcyon\History\Models\History as Model;

trait Historable
{
    public function historable()
    {
        return $this->morphMany(Model::class, 'historable');
    }

    public function author()
    {
        return $this->historable()->where('action', 'create')->firstOrFail()->user();
    }

    // public static function boot()
    // {
    // 	parent::boot();
    // 	static::created(function ($model) {  history()->log($model, 'created'); });
    // 	// static::saved(	function ($model) {  history()->log($model, 'updated'); });
    // 	// static::updated(function ($model) {  history()->log($model, 'updated'); });
    // 	// static::deleted(function ($model) {  history()->log($model, 'deleted'); });
    // 	static::restored(function ($model) { history()->log($model, 'restored'); });
    // }
}
