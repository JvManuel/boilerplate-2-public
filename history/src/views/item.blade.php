<article class="timeline-entry">
	<div class="timeline-entry-inner">
		<time class="timeline-time">
			@if(auth()->guest())
				<span>{{ $history->updated_at->format('h:i A') }}</span> 
				<span>{{ $history->created_at->diffForHumans() }}</span>
			@else
				<span>{{ $history->updated_at->timezone(auth()->user()->timezone)->format('h:i A') }}</span> 
				<span>{{ $history->created_at->timezone(auth()->user()->timezone)->diffForHumans() }}</span>
			@endif
		</time>
		<div class="timeline-icon {{ $history->color }}">
			<i class="{{ $history->icon }}"></i>
		</div>
		
		<div class="timeline-label">
			{{-- <h2><a href="#">{{ $history->user->name }}</a> <span></span></h2> --}}
			{{-- <p>{!! __('history::message.actions.' . $history->action, ['Name' => $history->historable ? $history->historable->name : $history->option('name'), 
				'User' => '<b><a href="'. route('admin.auth.user.show', $history->user) .'">'.$history->user->name.'</a> <span></span></b>',
				'status' => ($history->icon == 'fa fa-ban' ? '<span class="text-danger"> inactive</span>' : '<span class="text-success"> active</span>')
			]) !!}</p> --}}
			<p>{!! $history->display !!}</p>
		</div>
	</div>
	
</article>
