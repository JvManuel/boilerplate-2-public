<div class="timeline-centered">

	@each('history::item', $histories, 'history')
</div>
<div class="clearfix">
	<div class="pull-right"> {{ $histories->links() }} </div>
</div>
