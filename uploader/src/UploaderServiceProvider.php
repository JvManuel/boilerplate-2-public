<?php
namespace PackageHalcyon\Uploader;

use Illuminate\Support\ServiceProvider;
use PackageHalcyon\Uploader\Uploader;

class UploaderServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    public function register()
    {
        $this->app->bind('uploader', function ($app) {
            return new Uploader($app);
        });
        $this->app->alias('uploader', \PackageHalcyon\Uploader\Facades\Uploader::class);
        // register our controller
        $this->app->make('PackageHalcyon\Uploader\Controllers\UploaderController');
    }
}
