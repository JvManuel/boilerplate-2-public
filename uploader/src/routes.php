<?php
/**
 * All route names are prefixed with 'admin.content'.
 */
Route::group([
    'namespace'  => 'PackageHalcyon\Uploader\Controllers',
], function () {
    Route::get('file/download/{uploader}', 'UploaderController@download')->name('uploader.download');
    Route::get('file/{uploader}', 'UploaderController@show')->name('uploader.show');
});
