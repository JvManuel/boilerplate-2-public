<?php

namespace PackageHalcyon\Uploader\Controllers;

use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\Controller;
use Crypt;
use Storage;
use Image;

/**
 * Class UploaderController.
 */
class UploaderController extends Controller
{
    /**
     * @param $lang
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($file)
    {
        try {
            $path = decrypt($file);
            $file = Storage::get($path);
            $type = Storage::mimeType($path);
            return response($file)->header('Content-Type', $type);
        } catch (DecryptException $e) {
            abort(404);
        }
    }


    /**
     * @param $lang
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download($file)
    {
        try {
            $file = decrypt($file);
            return response()->download(storage_path('app/'.$file));
        } catch (DecryptException $e) {
            abort(404);
        }
    }
}
