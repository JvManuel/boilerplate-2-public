<?php

namespace PackageHalcyon\Uploader;

use Crypt;
use Image;
use File;
use Storage;

/**
 * Class Upload.
 */
class Uploader
{
    public function storeMultiple($files, $config)
    {
        if (is_array($config)) {
            $config = (object) $config;
        }
        if (!is_array($files)) {
            $files  = [$files];
        }

        if (!file_exists(storage_path() . '/app/' . $config->path)) {
            Storage::makeDirectory($config->path);
        }
        $filenames = [];
        try {
            foreach ($files as $f => $file) {
                $filenames[] = $this->store($file, $config);
            }
            return $filenames;
        } catch (\Exception $e) {
            $this->exceptions($e->getMessage());
        }
    }

    public function store($file, $config, $view = 'uploader.show')
    {
        if (is_array($config)) {
            $config = (object) $config;
        }
        $type  = $file->getMimeType();
        $name  = $config->name .'_' . uniqid() . '.' . $file->getClientOriginalExtension();
        if (str_contains($type, ['image']) && $type != 'image/vnd.dwg') {
            $dimensions = getimagesize($file);
            $width   = $config->width  < $dimensions[0] ? $config->width  : $dimensions[0];
            $height  = $config->height < $dimensions[1] ? $config->height : $dimensions[1];
            $img = Image::make($file->getRealPath());
            $img = Image::make($file->getRealPath());
            $img->resize($width, $height, function ($c) {
                $c->aspectRatio();
            });
            $img->stream();
            Storage::put($config->path . '/' . $name, (string) $img->encode());
        } else {
            $file->storeAs($config->path, $name);
        }

        return $this->get($config->path . '/' . $name, $view);
    }

    public function get($paths, $route = 'uploader.show')
    {
        if (!$paths) {
            return null;
        }
        if (!is_array($paths)) {
            return route($route, Crypt::encrypt($paths));
        }
        $files = [];
        if (count($paths)) {
            foreach ($paths as $p => $path) {
                $files[] = route($route, Crypt::encrypt($path));
            }
        }
        return $files;
    }

    /**
     * Check if File Path exists
     * @param  string $path [description]
     * @return bool
     */
    public function checkIfPathExists($path = "") : bool
    {
        $basepath = url('/') . '/file/';
        if (strpos($path, $basepath) !== false) {
            $filepath = str_replace($basepath, '', $path);
            try {
                return Storage::exists(decrypt($filepath));
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function destroy($files)
    {
        if (! $files) {
            return true;
        }
        try {
            if (!is_array($files)) {
                $files = [$files];
            }
            
            if (count($files)) {
                foreach ($files as $f => $path) {
                    if ($path) {
                        $uploader_path = url('/') . '/file/';
                        $fullpath = decrypt(str_replace($uploader_path, '', $path));
                        if (Storage::exists($fullpath)) {
                            Storage::delete($fullpath);
                        }

                        // Delete Directory if there are no files existing
                        $directory = str_replace(explode('/', $fullpath)[count(explode('/', $fullpath)) - 1], '', $fullpath);
                        if (! count(Storage::allFiles($directory))) {
                            Storage::deleteDirectory($directory);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            // if(!file_exists($full_path)){ File::delete($full_path); }
            $this->exceptions($e->getMessage());
        }
    }

    /**
     * @return GeneralException
     */
    public function exceptions($label)
    {
        throw new \Exception($label);
    }
}
