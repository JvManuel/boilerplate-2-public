<?php

if (! function_exists('uploader')) {
    /**
     * uploader (lol) the uploader:: facade as a simple function.
     */
    function uploader()
    {
        return app('uploader');
    }
}
