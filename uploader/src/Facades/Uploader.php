<?php

namespace PackageHalcyon\Uploader\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Upload.
 */
class Uploader extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'uploader';
    }
}
